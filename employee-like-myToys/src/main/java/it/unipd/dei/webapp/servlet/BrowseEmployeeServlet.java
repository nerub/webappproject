package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Searches employees by their salary.
 * 
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class BrowseEmployeeServlet extends AbstractDatabaseServlet {

	/**
	 * Searches employees by their salary.
	 * 
	 * @param req
	 *            the HTTP request from the client.
	 * @param res
	 *            the HTTP response from the server.
	 * 
	 * @throws ServletException
	 *             if any error occurs while executing the servlet.
	 * @throws IOException
	 *             if any error occurs in the client/server communication.
	 */

	 //OLD JAVA OBJECT APPROACH
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {


        // model
        List<Employee> el = null;
        Message m = null;


        try {

            // creates a new object for accessing the database and searching the questions
            el = new BrowseEmployeeDatabase(getDataSource().getConnection())
                    .browseEmployee();

            m = new Message("questions successfully searched.");

            res.setStatus(HttpServletResponse.SC_OK);

        } catch (NumberFormatException ex) {
            m = new Message("Cannot search for questions.",
                    "E100", ex.getMessage());
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());        
        } catch (SQLException ex) {
            m = new Message("Cannot search for questions: unexpected error while accessing the database.",
                    "E200", ex.getMessage());
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());        
        }

    //    new ResourceList(el).toJSON(res.getOutputStream());


        // stores the question list and the message as a request attribute
        req.setAttribute("table-results", el);
        req.setAttribute("message", m);

        // forwards the control to the search-question-result JSP
        req.getRequestDispatcher("/jsp/index.jsp").forward(req, res);
        

        // set the MIME media type of the response
		res.setContentType("text/html; charset=utf-8");

		// get a stream to write the response
		PrintWriter out = res.getWriter();

		// write the HTML page
		out.printf("<!DOCTYPE html>%n");
		
		out.printf("<html lang=\"en\">%n");
		out.printf("<head>%n");
		out.printf("<meta charset=\"utf-8\">%n");
		out.printf("<title>Browse Employee</title>%n");
		out.printf("</head>%n");

		out.printf("<body>%n");
		out.printf("<h1>Browse Employee</h1>%n");
		out.printf("<hr/>%n");

		if(m.isError()) {
			out.printf("<ul>%n");
			out.printf("<li>error code: %s</li>%n", m.getErrorCode());
			out.printf("<li>message: %s</li>%n", m.getMessage());
			out.printf("<li>details: %s</li>%n", m.getErrorDetails());
			out.printf("</ul>%n");
		} else {
			out.printf("<p>%s</p>%n", m.getMessage());
			
			out.printf("<table>%n");
			out.printf("<tr>%n");
			out.printf("<td>Badge</td><td>Surname</td><td>Age</td><td>Salary</td>%n");
			out.printf("</tr>%n");
			
			for(Employee e: el) {
				out.printf("<tr>%n");
				out.printf("<td>%s</td><td>%s</td><td>%s</td><td>%s</td>%n", 
					e.getBadge(), e.getSurname(), e.getAge(), e.getSalary());
				out.printf("</tr>%n");
			}
			out.printf("</table>%n");
		}

		out.printf("</body>%n");
		
		out.printf("</html>%n");

		// flush the output stream buffer
		out.flush();

		// close the output stream
		out.close(); 
		

    }

}
