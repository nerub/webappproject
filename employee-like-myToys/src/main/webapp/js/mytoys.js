// Self-invoking function that fill the questions table and the categories section by querying the database
(function() {

    //QUESTION QUERY
    var httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHttpRequest instance');
        return false;
    }
	var url = "http://dbstud.dei.unipd.it:5432/webapp1718/browse-employee";
    httpRequest.onreadystatechange = writeEmployeeResults;
	httpRequest.open('POST', url);
    httpRequest.send();

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    //functions

    /**
     * Function to print questions
     */
    function writeEmployeeResults()
    {
        if (httpRequest.readyState === XMLHttpRequest.DONE)
        {
            // get the div where to write results
            var div = document.getElementById('table-results');

            // clean it up
            div.className = '';
            while (div.firstChild)
            {
                div.removeChild(div.firstChild);
            }

            // generic DOM elements
            var e;
            var ee;

            // if we get a 200 status write result table parsing it from JSON
            if (httpRequest.status == 200)
            {
                // a generic row and column of the table
                var tr;
                var td;

                // the JSON list of questions
                var jsonData = JSON.parse(httpRequest.responseText);
                jsonData = jsonData['resource-list'];

                var employee;

                //creation of the table
                e = document.createElement('table');
                e.className = 'table';
                e.id='table';
                div.appendChild(e);

                //creation of the head table
                ee = document.createElement('thead');
                ee.className = 'thead-light';
                e.appendChild(ee);

                tr = document.createElement('tr');
                ee.appendChild(tr);

                td = document.createElement('th');
                td.appendChild(document.createTextNode('Badge'));
                tr.appendChild(td);

                td = document.createElement('th');
                td.appendChild(document.createTextNode('Surname'));
                tr.appendChild(td);

                td = document.createElement('th');
                td.appendChild(document.createTextNode('Age'));
                tr.appendChild(td);

                td = document.createElement('th');
                td.appendChild(document.createTextNode('Salary'));
                tr.appendChild(td);


                //creation of the dynamic table body
                ee = document.createElement('tbody');
                e.appendChild(ee);

                for (var i = 0; i < jsonData.length; i++)
                {
                    employee = jsonData[i].employee;

                    tr = document.createElement('tr');
                    ee.appendChild(tr);

                    td = document.createElement('td');
                    td.appendChild(document.createTextNode(i+1));
                    tr.appendChild(td);

                    a.appendChild(document.createTextNode(employee['badge']));
                    td.appendChild(a);
                    tr.appendChild(td);

                    td = document.createElement('td');
                    td.appendChild(document.createTextNode(employee['surname']));
                    tr.appendChild(td);

                    td = document.createElement('td');
                    td.appendChild(document.createTextNode(employee['age']));
                    tr.appendChild(td);

                    td = document.createElement('td');
                    td.appendChild(document.createTextNode(employee['salary']));
                    tr.appendChild(td);

                }
            }

            else
            {
                div.className = 'alert alert-danger';

                e = document.createElement('p');
                e.appendChild(document.createTextNode('Unexpected error'));
                div.appendChild(e);
            }
        }
    }  
})();
