/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.*;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Manages the REST API for the {@link Comment} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class CommentRestResource extends RestResource
{

    /**
     * Creates a new REST resource for managing {@code Comment} resources.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public CommentRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con)
	{
        super(req, res, con);
    }


    /**
     * Creates a new comment into the database.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void insertComment() throws IOException
	{
        Comment e = null;
        Message m = null;

        try
		{
            final Comment comment = Comment.fromJSON(req.getInputStream());

            // creates a new object for accessing the database and stores the comment
            e = new InsertCommentDatabase(con, comment).InsertComment();

            if (e != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                e.toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot create the comment: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
		
		catch (Throwable t)
		{
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505"))
			{
                m = new Message("Cannot create the comment: it already exists.", "E5A2", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            }
			
			else
			{
                m = new Message("Cannot create the comment: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Deletes an comment from the database.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void deleteComment() throws IOException
	{
        Comment e  = null;
        Message m = null;

        try
		{
            // parse the URI path to extract the badge
            String path = req.getRequestURI();
			String attributeName = "comment";
            path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());

            final int badge = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and deletes the comment
            e = new DeleteCommentDatabase(con, badge).DeleteComment();

            if(e != null)
			{
                res.setStatus(HttpServletResponse.SC_OK);
                e.toJSON(res.getOutputStream());
            }
			
			else
			{
                m = new Message(String.format("Comment %d not found.", badge), "E5A3", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        }
		
		catch (Throwable t)
		{
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503"))
			{
                m = new Message("Cannot delete the comment: other resources depend on it.", "E5A4", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            }
			
			else
			{
                m = new Message("Cannot delete the comment: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Creates comments by their questionId.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void selectCommentJoinHasByQuestionId()  throws IOException
	{
        List<Comment> el  = null;
        Message m = null;

        try
		{
            // parse the URI path to extract the questionId
            String path = req.getRequestURI();
			String attributeName = "questionId";
            path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());

            final int questionId = Integer.parseInt(path.substring(1));


            // creates a new object for accessing the database and create the comments
            el = new SelectCommentJoinHasByQuestionIdDatabase(con, questionId).SelectComment();

            if(el != null)
			{
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(el).toJSON(res.getOutputStream());
            }
			
			else
			{
                // it should not happen
                m = new Message("Cannot create comment: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
		
		catch (Throwable t)
		{
            m = new Message("Cannot create comment: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}