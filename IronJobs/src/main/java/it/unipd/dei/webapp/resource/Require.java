package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Require} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Require
{
	/**
	 * the competence name
 	 */
	private final String name;
	/**
	 * the interview id
	 */
	private final int interviewId;

	/**
	 * Creates a new requirement
	 *
	 * the competence name
	 * @param name
	 * the interview id
	 * @param interviewId
	 */
	public Require(final String name, final int interviewId)
	{
		this.name = name;
		this.interviewId = interviewId;
	}
	
	// Get methods

	/**
	 * the competence Name
	 * @return competence name
	 */
	public final String getName()
	{
		return name;
	}

	/**
	 * the interview id
 	 * @return interview id
	 */
	public final int getInterviewId()
	{
		return interviewId;
	}
	
	///////////////
	
	// Set methods
	
	///////////////
}