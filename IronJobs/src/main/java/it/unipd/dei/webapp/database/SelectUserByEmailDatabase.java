package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
* Searches for the details of a user in the database.
*
* @version 1.00
* @since 1.00
*/

public final class SelectUserByEmailDatabase
{
  /**
  * SQL statement that does the insertion.
  */
  private static final String STATEMENT = "SELECT username, birthDate, points, email, "
  + "avatar, details, degree, pswd, currentJob, type, registrationTime FROM IronJobs.UserAccount"
  + " WHERE email = ?";

  /**
  * ID of the question.
  */
	private final Connection con;

  /**
  * Email of the user.
  */
	private final String email;

  /**
  * Creates a new object for searching the user.
  * @param con The connection to the database.
  * @param email The email of the user for which to search for the details.
  */
	public SelectUserByEmailDatabase(final Connection con, final String email)
	{
		this.con = con;
		this.email = email;
	}

  /**
  * Searches the user attributes.
  * @return A list of the user with all the attributes.
  * @throws SQLException if the search goes wrong.
  */
	public List<User> SelectUserByEmail() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<User> users = new ArrayList<User>();

		try
		{
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1,email);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
					users.add(new User(rs.getString("username"),
										rs.getString("birthDate"),
										rs.getInt("points"),
										rs.getString("email"),
										rs.getInt("avatar"),
										rs.getString("details"),
										rs.getString("degree"),
										rs.getString("pswd"),
										rs.getString("currentJob"),
										rs.getInt("type"),
										rs.getString("registrationTime")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return users;
	}
}
