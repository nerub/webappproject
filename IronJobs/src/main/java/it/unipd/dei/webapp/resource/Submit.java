package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Submit} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Submit
{
	/**
	 * the submit time
	 */
	private final String submitTime;
	/**
	 * the vatin
	 */
	private final String vatIn;
	/**
	 * the question id
	 */
	private final int questionId;

	/**
	 * Creates a new submit
	 * submittime
	 * @param submitTime
	 * the company vatin
	 * @param vatIn
	 * the questionId
	 * @param questionId
	 */
	public Submit(final String submitTime, final String vatIn, final int questionId)
	{
		this.submitTime = submitTime;
		this.vatIn = vatIn;
		this.questionId = questionId;
	}
	
	// Get methods

	/**
	 * the submit time
	 * @return submittime
	 */
	public String getSubmitTime()
	{
		return submitTime;
	}

	/**
	 * the vatin
	 * @return vatin
	 */
	public String getVatIn()
	{
		return vatIn;
	}

	/**
	 * the question id
	 * @return questionId
	 */
	public int getQuestionId()
	{
		return questionId;
	}
	
	///////////////
	
	// Set methods
	
	///////////////
}