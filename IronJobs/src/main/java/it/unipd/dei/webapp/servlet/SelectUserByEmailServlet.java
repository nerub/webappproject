
package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SelectUserByEmailDatabase;
import it.unipd.dei.webapp.resource.User;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Searches users by their email.
 *
 * @author Eooardo Furlan (edoardo.furlan.1@studenti.dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class SelectUserByEmailServlet extends AbstractDatabaseServlet {

    /**
     * Searches users by their email.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        //request parameter
        String email=null;

        // model
        List<User> el = null;
        Message m = null;

        // retrieves the request parameter
        email = req.getParameter("email");

        try {

            // creates a new object for accessing the database and searching the users
            el = new SelectUserByEmailDatabase(getDataSource().getConnection(),email)
                    .SelectUserByEmail();

            m = new Message("users successfully searched.");

        } catch (NumberFormatException ex) {
            m = new Message("Cannot search for users. Invalid input parameters: email must be integer.",
                    "E100", ex.getMessage());
        } catch (SQLException ex) {
            m = new Message("Cannot search for users: unexpected error while accessing the database.",
                    "E200", ex.getMessage());
        }

        // stores the user list and the message as a request attribute
        req.setAttribute("userlist", el);
        req.setAttribute("message", m);

        // forwards the control to the search-user-result JSP
        req.getRequestDispatcher("/jsp/select-user-by-email-result.jsp").forward(req, res);

    }

}
