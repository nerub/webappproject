package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Possess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Insert a new possess into the database.
 *
 * @version 1.00
 * @since 1.00
 */
public final class InsertPossessDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "INSERT INTO IronJobs.Possess (email, name, rating) VALUES (?, ?, ?) RETURNING *";

  /**
  * Connection to the database.
  */
	private final Connection con;

  /**
  * Object possess which will be inserted into the database.
  */
	private final Possess possess;

  /**
  * Creates a new object for inserting.
  * @param con The connection to the database.
  * @param possess The new possess object to be inserted.
  */
	public InsertPossessDatabase(final Connection con, final Possess possess)
	{
		this.con = con;
		this.possess = possess;
	}

  /**
  * Inserts the new possess object into the database.
  * @return The possess inserted.
  * @throws SQLException if the insertion goes wrong.
  */
	public Possess InsertPossess() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Possess e = null;

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

			pstmt.setString(1, possess.getEmail());
			pstmt.setString(2, possess.getName());
			pstmt.setInt(3, possess.getRating());

			rs = pstmt.executeQuery();

			if (rs.next()) {
				e = new Possess(rs.getString("email"),
						rs.getString("name"),
						rs.getInt("rating"));
			}
		}

		finally
		{
			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}
		return e;
	}
}
