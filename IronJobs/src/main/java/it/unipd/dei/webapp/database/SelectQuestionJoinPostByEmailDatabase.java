package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Question;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
* Searches for all the questions by a user.
*
* @version 1.00
* @since 1.00
*/

public final class SelectQuestionJoinPostByEmailDatabase
{
  /**
  * SQL statement that does the insertion.
  */
  private static final String STATEMENT = "SELECT IronJobs.Question.questionId, difficulty, isPrivate, upvote, downvote, views, body, title "
  +" FROM IronJobs.Post INNER JOIN IronJobs.Question"
  +" ON IronJobs.Post.questionId = IronJobs.Question.questionId "
  +" WHERE IronJobs.Post.email = ?";

  /**
  * Connection to the database.
  */
  private final Connection con;

  /**
  * Email of the user.
  */
  private final String email;

  /**
  * Creates a new object for searching the questions.
  * @param con The connection to the database.
  * @param email The email of the user to search for questions.
  */
	public SelectQuestionJoinPostByEmailDatabase(final Connection con, final String email)
	{
        this.con = con;
        this.email = email;
	}

  /**
  * Searches the questions.
  * @return A list of the questions with all of the attributes.
  * @throws SQLException if the search goes wrong.
  */
	public List<Question> SelectQuestionJoinPostByEmail() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Question> questions = new ArrayList<Question>();

		try
		{
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setString(1, email);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				questions.add(new Question(rs.getInt("questionId"),
											rs.getInt("difficulty"),
											rs.getBoolean("isPrivate"),
											rs.getInt("upvote"),
											rs.getInt("downvote"),
											rs.getInt("views"),
											rs.getString("body"),
											rs.getString("title")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return questions;
	}
}
