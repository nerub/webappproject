package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Question;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Searches for the questions by the questionid.
 *
 * @version 1.00
 * @since 1.00
 */
public final class SelectQuestionByQuestionIdDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "SELECT questionId, difficulty, isPrivate, upvote, downvote, views, body, title "+
											"FROM IronJobs.Question "+
											"WHERE IronJobs.Question.questionId = ?";

  /**
  * Connection to the database.
  */
  private final Connection con;

  /**
  * Id of the question.
  */
  private final int questionId;

  /**
  * Creates a new object for searching the question.
  * @param con The connection to the database.
  * @param questionId The id of the question to search for.
  */
	public SelectQuestionByQuestionIdDatabase(final Connection con, final int questionId)
	{
        this.con = con;
        this.questionId = questionId;
	}

  /**
  * Searches the question.
  * @return A list of the questions with all of the attributes.
  * @throws SQLException if the search goes wrong.
  */
	public List<Question> SelectQuestion() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Question> questions = new ArrayList<Question>();

		try
		{
			pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, questionId);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				questions.add(new Question(rs.getInt("questionId"),
											rs.getInt("difficulty"),
											rs.getBoolean("isPrivate"),
											rs.getInt("upvote"),
											rs.getInt("downvote"),
											rs.getInt("views"),
											rs.getString("body"),
											rs.getString("title")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return questions;
	}
}
