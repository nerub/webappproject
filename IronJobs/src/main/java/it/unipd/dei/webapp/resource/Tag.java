package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Tag} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Tag
{
	/**
	 * the Tag name
	 */
	private final String name;

	/**
	 * Creates a new Tag
 	 * @param name
	 */
	public Tag (final String name)
	{
		this.name = name;
	}
	
	// Get methods

	/**
	 * the tag name
	 * @return tag name
	 */
	public String getName()
	{
		return name;
	}
	
	///////////////
	
	// Set methods
	
	///////////////
}