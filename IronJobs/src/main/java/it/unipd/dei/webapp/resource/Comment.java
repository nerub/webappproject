package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.*;

import java.io.*;


/**
 * Represents a {@link Comment} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Comment extends Resource
{
	/**
	 * The id
	 */
	private final int commentId;
	/**
	 * The text
	 */
	private final String commentText;
	/**
	 * The number of upvotes
	 */
	private int upvote;
	/**
	 * The number of downvotes
	 */
	private int downvote;
	/**
	 * The writetime
	 */
	private final String writeTime;

	/**
	 * Creates a comment.
	 *
	 * @param commentId
	 *            the id.
	 * @param commentText
	 *            the text.
	 * @param upvote
	 *            number of upvote.
	 * @param downvote
	 *            number of downvote.
	 * @param writeTime
	 *            writetime.
	 */
	public Comment(final int commentId,final String commentText, int upvote, int downvote, final String writeTime)
	{
		this.commentId = commentId;
		this.commentText = commentText;
		this.upvote = upvote;
		this.downvote = downvote;
		this.writeTime = writeTime;
	}

	// Get methods

	/**
	 * Returns the id.
	 *
	 * @return the id.
	 */
	public final int getCommentId() { return commentId; }

	/**
	 * Returns the text.
	 *
	 * @return the text.
	 */
	public final String getCommentText() { return commentText; }

	/**
	 * Returns the number of upvote.
	 *
	 * @return the number of upvote.
	 */
	public int getUpvote()
	{
		return upvote;
	}

	/**
	 * Returns the number of downvote.
	 *
	 * @return the number of downvote.
	 */
	public int getDownvote()
	{
		return downvote;
	}

	/**
	 * Returns the writetime.
	 *
	 * @return the writetime.
	 */
	public final String getWriteTime()
	{
		return writeTime;
	}

	///////////////

	// Set methods

	/**
	 * Set the upvote
	 *
	 * @param upvote
	 */
	public void setUpvote(int upvote)
	{
		this.upvote = upvote;
	}

	/**
	 * Set the downvote
	 *
	 * @param downvote
	 */
	public void setDownvote(int downvote)
	{
		this.downvote = downvote;
	}

	///////////////

	// JSON part

	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("comment");

		jg.writeStartObject();

		jg.writeNumberField("commentId", commentId);

		jg.writeStringField("commentText", commentText);

		jg.writeNumberField("upvote", upvote);

		jg.writeNumberField("downvote", downvote);

		jg.writeStringField("writeTime", writeTime);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	/**
	 * Creates a {@code Comment} from its JSON representation.
	 *
	 * @param in the input stream containing the JSON document.
	 *
	 * @return the {@code Comment} created from the JSON representation.
	 *
	 * @throws IOException if something goes wrong while parsing.
	 */
	public static Comment fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		int jCommentId = -1;
		String jCommentText = null;
		int jUpVote = -1;
		int jDownVote = -1;
		String jWriteTime = null;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "comment".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no comment object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "commentId":
						jp.nextToken();
						jCommentId = jp.getIntValue();
						break;
					case "commentText":
						jp.nextToken();
						jCommentText = jp.getText();
						break;
					case "upvote":
						jp.nextToken();
						jUpVote = jp.getIntValue();
						break;
					case "downvote":
						jp.nextToken();
						jDownVote = jp.getIntValue();
						break;
					case "writeTime":
						jp.nextToken();
						jWriteTime = jp.getText();
						break;
				}
			}
		}

		return new Comment(jCommentId, jCommentText, jUpVote, jDownVote, jWriteTime);
	}
}
