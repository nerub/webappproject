(function()
{
// Load dog list
	var select = document.getElementById('categoryValue'); //variabile cercata, id del form su html
	var httpRequest;
	httpRequest = new XMLHttpRequest();

	if (!httpRequest)
	{
		alert('Giving up :( Cannot create an XMLHttpRequest instance');
		return false;
	}

	url = "https://dog.ceo/api/breeds/list/all"; //url dato dal prof
	httpRequest.onreadystatechange = loadDogs; //funzione sotto
	httpRequest.open('GET', url);
	httpRequest.send();

	//////////////////////////////////////////////////////////////////
	// loadDogs crea la lista che verrà messa nella form di ricerca //
	//////////////////////////////////////////////////////////////////
	function loadDogs()
	{
		if (httpRequest.readyState === XMLHttpRequest.DONE)
		{
			var jsonData = JSON.parse(httpRequest.responseText);

			// if we get a 500 status write the error message parsing it from JSON
			if (httpRequest.status == 500)
			{	
				select.innerHTML = "<option value='ServerError'>Server Error</option>";
			}
			
			else if (httpRequest.status == 200)
			{
				var jsonData = JSON.parse(httpRequest.responseText); 
				dogs = jsonData['message'];	//lista dei cani richiesti
				var selectList = "";			
				for (var key in dogs)	//per tutti i cani richiesti e trovati
				{
					//Inserisco nella lista le razze di cani cani negli option value della form
					selectList += "<option value='" + key + "'>" + key + "</option>";
				}
			
				select.innerHTML = selectList;	//trasformo in html
			}				
		}
		// if we get a 200 status write result table parsing it from JSON
		
		

		else
		{
			select.innerHTML = "<option value='ServerError'>Server Error</option>";
		}
	}	
	/////////////////////////////
	//FINE FUNZIONE LOADDOGS() //
	/////////////////////////////


})();

/* Store dogs details */
function insertDog() {
	//Url dato dal prof dove verranno mandati i dati
	var dogurl = "https://dog.ceo/api/breeds/"; //esempio

	//Creo l'oggetto prendendolo dagli input dell'utente
	var dog = [{name : document.getElementById('name').value,
				age: document.getElementById('age').value,
				category: document.getElementById('category').value,
				registrationTime: new Date().toUTCString()
	}];
	
	var httpRequest;
	httpRequest = new XMLHttpRequest();
	httpRequest.open('POST', 'somewhere', true);
	httpRequest.onload = function () {
	    // do something to response
	    //Call a function when the state changes.
    	if(http.status == 500) {
        	//Printa errore
        }
        else if(http.status == 200){
        	// OK, printa oggetto
        }
	};
	httpRequest.send(dog);
}

	//ALTRA POSSIBILITA'
/*
	var httpRequest;
	httpRequest = new XMLHttpRequest();

	if (!httpRequest)
	{
		alert('Giving up :( Cannot create an XMLHttpRequest instance');
		return false;
	}

	httpRequest.open('POST', url, true);
	httpRequest.send();

}
*/

/*
	$.ajax({
		method: "POST",
		url: dogurl,
		data: JSON.stringify({dog : dog}),
		contentType: 'application/json',
		error: function( status ) {
			/* Errore 
			Faccio quello che mi chiede in caso di errore 
		  alert( "Status: " + status.responseText);
		  console.log(status.responseText);
		}
	}).done(function(){

		/* Andata a buon fine 
		Faccio quello che mi chiede 

		 // Now we reload the page to show the new comment
		 //location.replace("page2.html");
		});
}*/
