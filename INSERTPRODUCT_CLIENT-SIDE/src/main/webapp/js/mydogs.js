(function()
{
// Load dog list
	var select = document.getElementById('categoryValue'); //variabile cercata, id del form su html
	var httpRequest;
	httpRequest = new XMLHttpRequest();

	if (!httpRequest)
	{
		alert('Giving up :( Cannot create an XMLHttpRequest instance');
		return false;
	}

	url = "https://dog.ceo/api/breeds/list/all"; //url dato dal prof
	httpRequest.onreadystatechange = loadDogs; //funzione sotto
	httpRequest.open('GET', url);
	httpRequest.send();

	//////////////////////////////////////////////////////////////////
	// loadDogs crea la lista che verrà messa nella form di ricerca //
	//////////////////////////////////////////////////////////////////
	function loadDogs()
	{
		if (httpRequest.readyState === XMLHttpRequest.DONE)
		{
			var jsonData = JSON.parse(httpRequest.responseText);

			// if we get a 500 status write the error message parsing it from JSON
			if (httpRequest.status == 500)
			{	
				select.innerHTML = "<option value='ServerError'>Server Error</option>";
			}
			
			else if (httpRequest.status == 200)
			{
				var jsonData = JSON.parse(httpRequest.responseText); 
				dogs = jsonData['message'];	//lista dei cani richiesti
				var selectList = "";			
				for (var key in dogs)	//per tutti i cani richiesti e trovati
				{
					//Inserisco nella lista le razze di cani cani negli option value della form
					selectList += "<option value='" + key + "'>" + key + "</option>";
				}
			
				select.innerHTML = selectList;	//trasformo in html
			}				
		}
		// if we get a 200 status write result table parsing it from JSON
		
		

		else
		{
			select.innerHTML = "<option value='ServerError'>Server Error</option>";
		}
	}	
	/////////////////////////////
	//FINE FUNZIONE LOADDOGS() //
	/////////////////////////////

	//Ho i nomi, ora carico le immagini
	// Search dog images
	var httpRequest2 = new XMLHttpRequest();
	var imagesBox = document.getElementById("images");	
	document.getElementById('searchByCategory').addEventListener('submit', function(event)
	{	
		if (!httpRequest2)
		{
			alert('Giving up :( Cannot create an XMLHttpRequest instance');
			return false;
		}

		var selectedValue = select.options[select.selectedIndex].value;	//Prendo il valore della categoria selezionata
		var url2 = "https://dog.ceo/api/breed/"+ selectedValue +"/images"; //dai l'url dell'immagine voluta
		httpRequest2.onreadystatechange = loadImages;
		httpRequest2.open('GET', url2);
		httpRequest2.send();
		
		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();
	});
	
	////////////////////////////////////////////////////////////
	// loadImages carica le prime 12 immagini della categoria //
	////////////////////////////////////////////////////////////
	function loadImages()
	{
		if (httpRequest2.readyState === XMLHttpRequest.DONE)
		{
			var jsonData2 = JSON.parse(httpRequest2.responseText);
			var status = jsonData2['status'];		
			// if we get a 500 status write the error message parsing it from JSON
			if (httpRequest2.status == 500)
			{	
				imagesBox.innerHTML = "Server error. Impossible to load images. Status:" + status;
			}
			
			else if (httpRequest2.status == 200)
			{
				dogsImages = jsonData2['message'];		
				var imagesCreator = "";
				
				// Show only the first 12 images of the search
				var maxValue = dogsImages.length;
				if (maxValue > 12)
				{
					maxValue = 12;
				}				
						
				for (var i = 0; i < maxValue; i++)
				{
					imagesCreator += " <img src='"+ dogsImages[i] +"' alt='image' height='150' width='150'>";
				}
				
				imagesBox.innerHTML = imagesCreator;
			}				
		}
		
		else
		{	
			imagesBox.innerHTML = "Server error. Impossible to load images. Status:" + status;
		}
	}
})();
