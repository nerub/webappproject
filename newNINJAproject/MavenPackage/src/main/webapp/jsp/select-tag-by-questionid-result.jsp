<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
   <body>
      <c:import url="/jsp/include/show-message.jsp"/>

      <c:if test='${not empty taglist && !message.error}'>
         <div class="container addMargin table-responsive">
            <table class="table-bordered tagsTable">
               <tbody>
                  <tr>
                  <c:forEach var="tag" items="${taglist}">
                        <td align="center"><c:out value="${tag.name}"/></td>
                     </c:forEach>
                  </tr>
               </tbody>
            </table>
         </div>
      </c:if>
   </body>
</html>
