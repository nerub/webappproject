<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
   <meta charset="utf-8">
   <title>Search Tags By QuestionID</title>
</head>

<body>
<h1>Search Employee Form</h1>

<form method="POST" action="<c:url value="/select-tag-by-question"/>">
   <label for="questionId">QuestionId:</label>
   <input name="questionId" type="text"/><br/><br/>

   <button type="submit">Submit</button><br/>
   <button type="reset">Reset the form</button>
</form>
</body>
</html>
