<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"></link>
		<link href="../css/insertnamehere.css" rel="stylesheet" type="text/css"></link>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="../js/jquery-3.3.1.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/insertnamehere.js"></script>
		<title>Iron Jobs - Profile</title>
	</head>
	
	<body>
		<!-- Navigation Bar -->
		<div class="topnav">
			<a href="<c:url value="/html/index.html"/>"> <img class="topnav-logo" src="<c:url value="/resource/SVG/logo.png"/>" alt="logo_nav"></a>
			<% 
				String username = request.getParameter("username");
				if (username == null || username.isEmpty())
				{
			%>
			<a href="http://localhost:8080/IronJobs-1.0-SNAPSHOT/jsp/questions.jsp" class="burbank">QUESTIONS</a>

			<%
				}

				else
				{
			%>
				<a href="http://localhost:8080/IronJobs-1.0-SNAPSHOT/jsp/questions.jsp?username=<%= username %>" class="burbank">QUESTIONS</a>
			<%	
				}
			%>
			
			<a href="<c:url value= "/html/interviews.html"/>" class="burbank">INTERVIEWS</a>

		  	<% 
				if (username == null || username.isEmpty())
				{
			%>
			<!-- Login Modal -->
			<div class="topnav-right">
				<!-- Trigger the modal with a button -->
				<button type="button" class="login_button" data-toggle="modal" data-target="#myModal">Login</button>
				<!-- Modal -->
				<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title">Login</h3>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								<form class="form-group loginForm" method="POST"><br>
									<input class="form-control-plaintext" name="userName" placeholder=" Username" type="text" required><br>
									<input class="form-control-plaintext" name="password" placeholder=" Password" type="password" required><br>
									<button class="btn mybtn-primary"><b>Login</b></button><br><br>
									<p> Not a member? <button class="btn mybtn-primary" onclick="window.location.href='../html/registration.html';"><b>Register</b></button></p>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<%
				}

				else
				{
			%>
				<div class="topnav-right">
					<button type="button" class="login_button" onclick="window.location.href='account.jsp?username=<%= username %>';"><%= username %></button>
				</div>
			<%	
				}
			%>

		</div>
		<!-- ************** -->
		
		<div class="container">
			<br><h1>Account</h1><br><hr>
		</div>
		<div>
			<br>
			<div align="middle"><img src="../resource/workinprogress.png" alt="workinprogress"></div>
		</div>
	</body>
</html>