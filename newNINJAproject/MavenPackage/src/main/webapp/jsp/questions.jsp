<!--
 Copyright 2018 University of Padua, Italy

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Edoardo Borsato
		 Edoardo Furlan
		 Erik Bessegato
 Version: 1.0
 Since: 1.0
-->

<!-- Quando sono passato da html a jsp ho introdotto il content type response header e
    il taglib core -->

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Questions page">
	<meta name="author" content="Edoardo Borsato">
	<meta name="author" content="Edoardo Furlan">
	<meta name="author" content="Erik Bessegato">

	<title>Iron Jobs - Questions</title>

	<!-- bootstrap css -->
	<link href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css"></link>
	<!-- custom css -->
	<link href="<c:url value="/css/insertnamehere.css"/>" rel="stylesheet" type="text/css"></link>
	<!-- Scripts js -->
	<script src="../js/jquery-3.3.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<!-- NB: Custom JS at the end -->

</head>

<body>
      <!-- Navigation Bar -->
	    <div class="topnav">
			<a href="<c:url value="/html/index.html"/>"> <img class="topnav-logo" src="<c:url value="/resource/SVG/logo.png"/>" alt="logo_nav"></a>
			<% 
				String username = request.getParameter("username");
				if (username == null || username.isEmpty())
				{
			%>
			<a href="http://localhost:8080/IronJobs-1.0-SNAPSHOT/jsp/questions.jsp" class="burbank">QUESTIONS</a>

			<%
				}

				else
				{
			%>
				<a href="http://localhost:8080/IronJobs-1.0-SNAPSHOT/jsp/questions.jsp?username=<%= username %>" class="burbank">QUESTIONS</a>
			<%	
				}
			%>
			
			<a href="<c:url value= "/html/interviews.html"/>" class="burbank">INTERVIEWS</a>
				
			<% 
				if (username == null || username.isEmpty())
				{
			%>
				<!-- Login Modal -->
				<div class="topnav-right">
	            <!-- Trigger the modal with a button -->
		            <button type="button" class="login_button" data-toggle="modal" data-target="#myModal">Login</button>
		            <!-- Modal -->
		            <div class="modal fade" id="myModal" role="dialog">
		               <div class="modal-dialog">
		                  <!-- Modal content-->
		                  <div class="modal-content">
		                     <div class="modal-header">
		                        <h3 class="modal-title">Login</h3>
		                        <button type="button" class="close" data-dismiss="modal">&times;</button>
		                     </div>
		                     <div class="modal-body">
		                        <form class="form-group loginForm" method="POST"><br>
		                           <input class="form-control-plaintext" name="userName" placeholder=" Username" type="text" required><br>
		                           <input class="form-control-plaintext" name="password" placeholder=" Password" type="password" required><br>
		                           <button class="btn mybtn-primary"><b>Login</b></button><br><br>
		                           <p> Not a member? <button class="btn mybtn-primary" onclick="window.location.href='../html/registration.html';"><b>Register</b></button></p>
		                        </form>
		                     </div>
		                     <div class="modal-footer">
		                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                     </div>
		                  </div>
		               </div>
		            </div>
	        	</div>

			<%
				}

				else
				{
			%>
				<div class="topnav-right">
					<button type="button" class="login_button" onclick="window.location.href='account.jsp?username=<%= username %>';"><%= username %></button>
				</div>
			<%	
				}
			%>

		</div>
	<!-- ************** -->

	<div class="container">
		<br><h1>Questions</h1><br><hr>

		<div class="container">
			<!-- Use row format to allineate -->
			<div class="row">
				<!-- Filter Question Section -->
				<div class="col-md-6">
					<br>
					<!--<form id='form-search'>-->
						<div class="form-group inlineDivContainer">
							<input class="form-control-plaintext inlineObject searchInput" name="txtSearchQuestion" id="textSearch"/>
							<br>
							<br>
							<button class="btn inlineObject" name="btnSearchQuestion" id="btnSearch">
								<img height="20px" src="<c:url value="/resource/lens.png"/>" />
							</button>
						</div>
					<!--</form>-->
					<br>
				</div>
				<!-- ************** -->
			</div>
		</div>
		<!-- Question Table Section, done as in the exam solution -->
<!-- Here we prepare the space for the question table, and for the categories list -->
		<div class="row text-center">
				<div class="col-md-9">
					<div id="table-results"></div>
				</div>

				<div class="categoriesContainer">
					<div class="col-md-3">
						<h2>Categories</h2><br>
					</div>
					<div class="col-md-3">
						<div id="list-results"></div>
						<br>
						<div>
							<button type="button" class="btn btn-default" id = "button-search-by-tag">Search</button>
						</div>
						
					</div>
				</div>
		</div>
	</div>
	<br>
      
    <!-- Custom JS -->
    <script src="<c:url value="/js/questions.js"/>"></script>
    <script src="<c:url value="/js/insertnamehere.js"/>"></script>
</body>
</html>