/* Load the content of the page */
function loadQuestion()
{
   // Checks if the user il logged in
   var username = sessionStorage.getItem('username');

   if(!(username == undefined))
   {
      var div = document.getElementById("topnav-right");
      while (div.firstChild)
      {
         div.removeChild(div.firstChild);
      }
      var div = document.createElement('div');
      var button = document.createElement('button');
      button.className = 'login_button';
      button.type = 'button';
      button.setAttribute("onclick", "window.location.href='jsp/account.jsp?username=" + username + "'");
      button.append(document.createTextNode(username));

      div.append(button);
      $('#topnav-right').append(div);

   }

   // Query the question and display the title and text
   var httpRequest = new XMLHttpRequest();
   if (!httpRequest)
   {
      alert('Giving up :( Cannot create an XMLHttpRequest instance');
      return false;
   }

   var questionid = sessionStorage.getItem('questionId');

   if (questionid == undefined)
   {
      alert("An unexpected error occured: cannot find questionid");
      return;
   }
   var u = window.location.href;

   if (u.search("question.jsp") !== -1) // first call, load the tags
   {
      // let's print the tags of the question
      $("#search-tags")[0].value = questionid;
      $("#post-tags").submit();
   }

   var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/question/" + questionid;
   httpRequest.onreadystatechange = writeQuestionDetails;
   httpRequest.open('GET', url);
   httpRequest.send();

   function writeQuestionDetails()
   {
      // Get complete
      if (httpRequest.readyState === XMLHttpRequest.DONE)
      {

         // DOM elements
         var e;
         var ee;

         // 500 status is an error
         if (httpRequest.status == 500)
         {
            // Gets the div that contains the question body
            var div = document.getElementById("nav-question");

            // clean the div
            div.className = '';
            while (div.firstChild)
            {
               div.removeChild(div.firstChild);
            }

            // Let's incorporate the error message and then print it
            var jsonData = JSON.parse(httpRequest.responseText);
            jsonData = jsonData['message'];

            e = document.createElement('ul');
            div.appendChild(e);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Message: ' + jsonData['message']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Error code: ' + jsonData['error-code']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Details: ' + jsonData['error-details']));
            e.appendChild(ee);

         }
         else if (httpRequest.status == 200) // Success
         {

            var title = document.getElementById("questionTitle");
            var text = document.getElementById("questionText");

            var jsonData = JSON.parse(httpRequest.responseText);

            jsonData = jsonData['resource-list'];

            var question = jsonData[0].question;

            title.innerHTML = 'Question: ' + question['title'];
            text.innerHTML = question['body'];

            // Now let's display username and avatar of the user who posted the question
            var getEmail = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/post/questionId/" + question['questionId'];

            $.getJSON(getEmail, function(data){

               var email = data['resource-list'][0].post.email;
               var getUser = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/user/" + email;

               $.getJSON(getUser, function(data){
                  var user = data['resource-list'][0].user;
                  $('#username').append(user.username);
                  var p = document.createElement('p');
                  p.append(document.createTextNode(' ' + (question.upvote - question.downvote) + ' '));
                  p.id = 'questionVotes';
                  $('#username').append(p);

                  $('#questionVotes').prepend("<i class='fas fa-arrow-circle-up'></i>");
                  $('#questionVotes').append("<i class='fas fa-arrow-circle-down'></i>");

                  var avatarSrc = "<img title='Avatar of the user' src='resource/avatar/" + getAvatar(user.avatar) + "'>";
                  $("#avatar").append(avatarSrc);
               });
            });

         }
         else
         {
            // Gets the div that contains the question body
            var div = document.getElementById("nav-question");

            // clean the div
            div.className = '';
            while (div.firstChild)
            {
               div.removeChild(div.firstChild);
            }

            div.className = 'alert alert-danger';

            var e = document.createElement('p');
            e.appendChild(document.createTextNode('Unexpected error...'));
            div.appendChild(e);
         }

      }
   }
}

function loadSolutions()
{
   // loads all the solutions to the question
   var httpRequest = new XMLHttpRequest();
   if (!httpRequest)
   {
      alert('Giving up :( Cannot create an XMLHttpRequest instance');
      return false;
   }
   var questionid = sessionStorage.getItem('questionId');
   var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/solution/questionId/" + questionid;
   httpRequest.onreadystatechange = writeSolutionsList;
   httpRequest.open('GET', url);
   httpRequest.send();


   function writeSolutionsList()
   {

      // Get complete
      if (httpRequest.readyState === XMLHttpRequest.DONE)
      {
         // DOM elements
         var e;
         var ee;

         // Gets the div that contains the question body
         var div = document.getElementById("solutionsOnQuestionList");

         // clean the div
         div.className = '';
         while (div.firstChild)
         {
            div.removeChild(div.firstChild);
         }

         // 500 status is an error
         if (httpRequest.status == 500)
         {

            // Let's incorporate the error message and then print it
            var jsonData = JSON.parse(httpRequest.responseText);
            jsonData = jsonData['message'];

            e = document.createElement('ul');
            div.appendChild(e);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Message: ' + jsonData['message']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Error code: ' + jsonData['error-code']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Details: ' + jsonData['error-details']));
            e.appendChild(ee);

         }
         else if (httpRequest.status == 200) // Success
         {

            var jsonData = JSON.parse(httpRequest.responseText);

            jsonData = jsonData['resource-list'];

            // Check if there exist at least one solution
            if(jsonData.length < 1)
            {
               var a = document.createElement('h3');
               a.appendChild(document.createTextNode('No solution posted yet! Post your solution to help others!'));
               div.appendChild(a);
               return;
            }

            var solution;

            // Table first row
            var tableDiv = document.createElement('div');
            tableDiv.className = 'col-md-11';
            div.appendChild(tableDiv);

            e = document.createElement('table');
            e.className = 'table table-striped';
            tableDiv.appendChild(e);


            ee = document.createElement('thead');
            e.appendChild(ee);

            var tr = document.createElement('tr');
            ee.appendChild(tr);

            th = document.createElement('th');
            th.className = 'user';
            h4 = document.createElement('h4')
            h4.appendChild(document.createTextNode('User'));
            th.appendChild(h4);
            tr.appendChild(th);

            var th = document.createElement('th');
            th.className = 'solutionText';
            var h4 = document.createElement('h4')
            h4.appendChild(document.createTextNode('Solution'));
            th.appendChild(h4);
            tr.appendChild(th);

            th = document.createElement('th');
            th.className = 'votes';
            var h4 = document.createElement('h4')
            h4.appendChild(document.createTextNode('Votes'));
            th.appendChild(h4);
            tr.appendChild(th);

            var body = document.createElement('tbody');
            e.appendChild(body);

            for (var i = 0; i < jsonData.length; i++)
            {
               solution = jsonData[i].solution;

               tr = document.createElement('tr');
               body.appendChild(tr);

               td = document.createElement('td');
               var id = "username" + i;
               td.id = id;
               tr.appendChild(td);
               $('#'+id).addClass('username');  // add a new class to the node

               var td = document.createElement('td');
               td.id = 'text' + i;

               var a = document.createElement('a');

               // save the solutionid if clicked
               a.id = "link"+solution['solutionId'];

               a.onclick =  function(){
                  var solid = this.id;
                  solid = solid.substring(solid.lastIndexOf('link') + 4);
                  setSolutionID(solid);
               };

               var uri = window.location.href; // actual page
               var page = 'select-tag-by-question';
               var index = uri.lastIndexOf(page);
               uri = uri.substring(index + page.length);
               uri = uri + 'jsp/solution.jsp';
               a.href = uri;
               a.appendChild(document.createTextNode(solution['solutionText']));
               td.appendChild(a);
               tr.appendChild(td);

               if (solution['isBest'])
               {
                  var star = "<i class='fas fa-star fa-sm' title='Best Solution'></i>";
                  $('#text' + i).prepend(star);
               }


               td = document.createElement('td');
               var votes = parseInt(solution['upvote']) - parseInt(solution['downvote']);
               td.id = 'voteSolution' + i;
               td.appendChild(document.createTextNode(' ' + votes + ' '));
               tr.appendChild(td);

               // Let's write also the username and display the avatar of the user
               var solution = jsonData[i].solution;
               var getUser = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/user/" + solution['email'];

               $.getJSON(getUser, displayUsernameAndAvatar(i));

               var id2 = '#voteSolution' + i;
               $(id2).prepend("<i class='fas fa-arrow-circle-up'></i>");
               $(id2).append("<i class='fas fa-arrow-circle-down'></i>");

            }

         }
         else
         {
            div.className = 'alert alert-danger';

            e = document.createElement('p');
            e.appendChild(document.createTextNode('Unexpected error...'));
            div.appendChild(e);
         }
      }
   }
}

/* Loads the comments of the questions and displays them */
function loadComments()
{
   // loads all the comments of a question
   var httpRequest = new XMLHttpRequest();
   if (!httpRequest)
   {
      alert('Giving up :( Cannot create an XMLHttpRequest instance');
      return false;
   }
   var questionid = sessionStorage.getItem('questionId');
   var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/comment/questionId/" + questionid;
   httpRequest.onreadystatechange = writeComments;
   httpRequest.open('GET', url);
   httpRequest.send();

   function writeComments()
   {
      // Get complete
      if (httpRequest.readyState === XMLHttpRequest.DONE)
      {

         // DOM elements
         var e;
         var ee;

         var div = document.getElementById("commentOnQuestionList");

         // clean the div
         div.className = '';
         while (div.firstChild)
         {
            div.removeChild(div.firstChild);
         }

         // 500 status is an error
         if (httpRequest.status == 500)
         {
            // Let's incorporate the error message and then print it
            var jsonData = JSON.parse(httpRequest.responseText);
            jsonData = jsonData['message'];

            e = document.createElement('ul');
            div.appendChild(e);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Message: ' + jsonData['message']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Error code: ' + jsonData['error-code']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Details: ' + jsonData['error-details']));
            e.appendChild(ee);
         }
         else if (httpRequest.status == 200) // Success
         {
            var jsonData = JSON.parse(httpRequest.responseText);

            jsonData = jsonData['resource-list'];

            // Check if there exist at least one comment
            if(jsonData.length < 1)
            {
               var a = document.createElement('h3');
               a.appendChild(document.createTextNode('No comment written yet! Post a new one!'));
               div.appendChild(a);
               return;
            }

            // List all comments
            var comment;

            // Table first row
            var tableDiv = document.createElement('div');
            tableDiv.className = 'col-md-11';
            div.appendChild(tableDiv);

            e = document.createElement('table');
            e.className = 'table table-striped';
            tableDiv.appendChild(e);

            ee = document.createElement('thead');
            e.appendChild(ee);

            var tr = document.createElement('tr');
            ee.appendChild(tr);

            var th = document.createElement('th');
            var h4 = document.createElement('h4');
            th.className = 'user';
            h4.appendChild(document.createTextNode('User'));
            th.appendChild(h4);
            tr.appendChild(th);

            th = document.createElement('th');
            h4 = document.createElement('h4');
            th.className = 'content';
            h4.appendChild(document.createTextNode('Comment'));
            th.appendChild(h4);
            tr.appendChild(th);

            th = document.createElement('th');
            h4 = document.createElement('h4');
            th.className = 'votes';
            h4.appendChild(document.createTextNode('Votes'));
            th.appendChild(h4);
            tr.appendChild(th);

            th = document.createElement('th');
            h4 = document.createElement('h4');
            th.className = 'writeTime';
            h4.appendChild(document.createTextNode('Posted on'));
            th.appendChild(h4);
            tr.appendChild(th);

            var body = document.createElement('tbody');
            e.appendChild(body);


            for (var i = 0; i < jsonData.length; i++)
            {
               comment = jsonData[i].comment;

               tr = document.createElement('tr');
               body.appendChild(tr);

               var td = document.createElement('td');
               td.id = "usernameComment" + i;
               tr.appendChild(td);

               td = document.createElement('td');
               td.appendChild(document.createTextNode(comment['commentText']));
               tr.appendChild(td);

               td = document.createElement('td');
               var votes = parseInt(comment['upvote']) - parseInt(comment['downvote']);
               td.appendChild(document.createTextNode(' ' + votes + ' '));
               td.id = 'votes' + i;
               tr.appendChild(td);

               var id2 = '#votes' + i;
               $(id2).prepend("<i class='fas fa-arrow-circle-up'></i>");
               $(id2).append("<i class='fas fa-arrow-circle-down'></i>");

               td = document.createElement('td');
               td.appendChild(document.createTextNode(comment['writeTime']));
               tr.appendChild(td);

               // Let's write also the username and display the avatar of the user
               var solution = jsonData[i].solution;
               var getUser = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/user/" + comment['email'];

               $.getJSON(getUser, displayUsernameAndAvatarComments(i));
            }
         }

         else
         {
            alert("Unexpected error: " + httpRequest.status);
         }
      }
   }
}

/* Saves the solutionid of the solution clicked */
function setSolutionID(solutionid)
{
   sessionStorage.setItem("solutionid", solutionid);
}

/*
* These two functions display the username and the avatar of the users in the comments
* and on the solutions list
*/
function displayUsernameAndAvatar(item) {
   return function(data) {
      var id = "#username" + item;
      var avatarSrc = "<img title='Avatar of the user' src='resource/avatar/" + getAvatar(data['resource-list'][0].user.avatar) + "'>";
      $(id).append(avatarSrc);
      p = document.createElement('p');
      p.className = 'username'
      p.append(document.createTextNode(data['resource-list'][0].user.username));
      $(id).append(p);
   };
}

function displayUsernameAndAvatarComments(index){
   return function(data)
   {
      var id = "#usernameComment" + index;
      var avatarSrc = "<img title='Avatar of the user' src='resource/avatar/" + getAvatar(data['resource-list'][0].user.avatar) + "'>";
      $(id).append(avatarSrc);
      p = document.createElement('p');
      p.append(document.createTextNode(data['resource-list'][0].user.username));
      $(id).append(p);
   }
}

/* Function that saves the new solution posted by the user */
function submitNewSolution()
{
   var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/solution";
   var questionId = sessionStorage.getItem('questionId');
   // console.log('Inside the submit function');
   var text = $('#user_solution').val();
   // console.log("Read text: " + text);
   var email = sessionStorage.getItem('email');
   if (email == undefined)
   {
      alert("You need to login to be able to post a new solution!");
      return;
   }
   var solution = [{solutionId:100,
   upvote: 0,
   downvote: 0,
   isBest: false,
   solutionText: text,
   isPremium: false,
   submitTime: new Date().toUTCString(),
   email: email,
   questionId: parseInt(questionId)}];

   $.ajax({
      method: "POST",
      url: url,
      data: JSON.stringify({solution: solution}),
      contentType: 'application/json',
      error: function( status ) {
         alert( "Status: " + status.responseText);
         console.log(status.responseText);
      },
   }).done(function(){
      // Now we load the solutions list
      $("#nav-solution-tab").trigger("click");
   });
}

function passEmailUsername1()
{
	var tmpUser = sessionStorage.getItem("username");
	if (tmpUser != null)
		window.location.href = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/jsp/questions.jsp?username=" + tmpUser;
	else
		window.location.href = "/jsp/questions.jsp";
}
