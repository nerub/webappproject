package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Question;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
* Searches for the questions in the database.
*
* @version 1.00
* @since 1.00
*/

public final class SelectQuestionDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "SELECT questionId, difficulty, isPrivate, upvote, downvote, views, body, title "+
											"FROM IronJobs.Question ORDER BY upvote-downvote DESC";

  /**
  * Connection to the database.
  */
	private final Connection con;

  /**
  * Creates a new object for searching the questions.
  * @param con The connection to the database.
  */
	public SelectQuestionDatabase(final Connection con)
	{
		this.con = con;
	}

  /**
  * Searches the questions.
  * @return A list of all the questions with all of the attributes.
  * @throws SQLException if the search goes wrong.
  */
	public List<Question> SelectQuestion() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Question> questions = new ArrayList<Question>();

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				questions.add(new Question(rs.getInt("questionId"),
											rs.getInt("difficulty"),
											rs.getBoolean("isPrivate"),
											rs.getInt("upvote"),
											rs.getInt("downvote"),
											rs.getInt("views"),
											rs.getString("body"),
											rs.getString("title")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return questions;
	}
}
