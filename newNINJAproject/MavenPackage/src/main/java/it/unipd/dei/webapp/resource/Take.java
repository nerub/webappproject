package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Take} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Take
{
	/**
	 * the take time
	 */
	private final String takeTime;
	/**
	 * the user mail
	 */
	private final String email;
	/**
	 * the interview id
	 */
	private final int interviewId;

	/**
	 * Creates a new take entry.
	 *
	 * @param takeTime the timestamp of the take.
	 *
	 * @param email the email of the user.
	 *
	 * @param interviewId the id of the interview the user has taken.
	 */
	public Take(final String takeTime, final String email, final int interviewId)
	{
		this.takeTime = takeTime;
		this.email = email;
		this.interviewId = interviewId;
	}

	// Get methods

	/**
	 * Returns the taketime.
	 * @return taketime the timestamp of the take.
	 */
	public final String getTakeTime()
	{
		return takeTime;
	}

	/**
	 * Returns the email of the user.
	 * @return email of the user that took the interview.
	 */
	public final String getEmail()
	{
		return email;
	}

	/**
	 * returns the interviewId.
	 * @return the id of the interview the user has taken.
	 */
	public final int getInterviewId()
	{
		return interviewId;
	}

	///////////////

	// Set methods

	///////////////
}
