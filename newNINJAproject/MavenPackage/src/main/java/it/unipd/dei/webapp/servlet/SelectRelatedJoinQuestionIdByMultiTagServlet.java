//THIS SERVLET IS USED TO GATHER THE QUESTIONIDS OF THE QUESTIONS RELATED TO THE TAG.
//IT IS NOT NECESSARY TO SELECT ALL THE QUESTIONS DATA, BECAUSE IT IS ALREADY DONE BEFORE
//THESE QUERY, SO WE ONLY NEED THE ID TO FILTER THE RESULTS.
package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SelectRelatedJoinQuestionIdByMultiTagDatabase;
import it.unipd.dei.webapp.resource.Question;
import it.unipd.dei.webapp.resource.Related;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.ResourceList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Searches questionIds by their tag.
 *
 * @version 1.00
 * @since 1.00
 */
public final class SelectRelatedJoinQuestionIdByMultiTagServlet extends AbstractDatabaseServlet
{
    /**
     * Searches questionIds by their tag.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        //request parameter
        String[] tagNames = null;
        String names = null;

        // model
        List<Question> questionsList = null;
        Message m = null;
       
        StringBuffer jb = new StringBuffer();
        String line = null;
        
        try
        {
            BufferedReader reader = req.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        }

        catch (Exception e) { }

        names = jb.toString();

        tagNames = names.split(",");

        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");

        try
        {
            // creates a new object for accessing the database and searching the questionIds
            questionsList = new SelectRelatedJoinQuestionIdByMultiTagDatabase(getDataSource().getConnection(), tagNames)
                    .SelectRelated();

            m = new Message("questionIds successfully searched.");

            res.setStatus(HttpServletResponse.SC_OK);

        }

        catch (NumberFormatException ex)
        {
            m = new Message("Cannot search for questionIds. Invalid input parameters",
                    "E100", ex.getMessage());
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        catch (SQLException ex)
        {
            m = new Message("Cannot search for questionIds: unexpected error while accessing the database.",
                    "E200", ex.getMessage());
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        new ResourceList(questionsList).toJSON(res.getOutputStream());
    }
}