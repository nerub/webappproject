package it.unipd.dei.webapp.resource;

/**
 * Represents a {@link Company} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Company
{
	/**
	 * partita iva europea
	 */
	private final String vatIn;
	/**
	 * the name
	 */
	private final String name;
	/**
	 * the generality
	 */
	private final String generality;
	/**
	 *  the venue
	 */
	private final String venue;
	/**
	 *  the registation time
	 */
	private final String registrationTime;

	/**
	 * Creates a Company
	 *
	 * @param vatIn
	 * the company id
	 * @param name
	 * the company name
	 * @param generality
	 * the generalities
	 * @param venue
	 * the venue
	 * @param registrationTime
	 * the registration time
	 */
	public Company(final String vatIn, final String name, final String generality, final String venue, final String registrationTime)
	{
		this.vatIn = vatIn;
		this.name = name;
		this.generality = generality;
		this.venue = venue;
		this.registrationTime = registrationTime;
	}
	
	// Get methods

	/**
	 * return the VatIn
	 *
	 * @return VatIn
	 */
	public final String getVatIn()
	{
		return vatIn;
	}
	/**
	 * return the Name
	 *
	 * @return Name
	 */
	public final String getName()
	{
		return name;
	}
	/**
	 * return the Generality
	 *
	 * @return Generality
	 */
	public final String getGenerality()
	{
		return generality;
	}
	/**
	 * return the Venue
	 *
	 * @return Venue
	 */
	public final String getVenue()
	{
		return venue;
	}
	/**
	 * return the RegistrationTime
	 *
	 * @return RegistrationTime
	 */
	public final String getRegistrationTime()
	{
		return registrationTime;
	}
	
	///////////////
	
	// Set methods
	
	///////////////
}