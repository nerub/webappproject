package it.unipd.dei.webapp.resource;

/**
 * Represents a {@link Interview} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Interview
{
	/**
	 * the interviewId
	 */
	private final int interviewId;
	/**
	 * the expiring date
	 */
	private final String expiringDate;
	/**
	 * the description
	 */
	private final String description;
	/**
	 *  the currentTime
	 */
	private final String createTime;
	/**
	 *  the vatin
	 */
	private final String vatIn;

	/**
	 * Creates an Interview
	 *
	 * @param interviewId The interviewId.
	 *
	 * @param expiringDate The expiring date.
	 *
	 * @param description The description of the interview.
	 *
	 * @param createTime The cration time of the interview.
	 *
	 * @param vatIn The vatin of the company that creates the interview.
	 */
	public Interview(final int interviewId, final String expiringDate, final String description, final String createTime, final String vatIn)
	{
		this.interviewId = interviewId;
		this.expiringDate = expiringDate;
		this.description = description;
		this.createTime = createTime;
		this.vatIn = vatIn;
	}

	// Get methods

	/**
	 * Returns the ID of the interview.
	 * @return interviewId of the interview.
	 */
	public final int getInterviewId()
	{
		return interviewId;
	}

	/**
	 * Returns the expiring date of the interview.
	 * @return expiring date.
	 */
	public final String getExpiringDate()
	{
		return expiringDate;
	}

	/**
	 * Returns the description of the interview.
	 * @return description of the interview.
	 */
	public final String getDescription()
	{
		return description;
	}

	/**
	 * Returns the creationtime of the interview.
	 * @return creation time.
	 */
	public final String getCreateTime()
	{
		return createTime;
	}

	/**
	 * Returns the vatin of the company that created the interview.
	 * @return vatIn the ID of the company.
	 */
	public final String getVatIn()
	{
		return vatIn;
	}

	///////////////

	// Set methods

	///////////////
}
