package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Insert a new user into the database.
 *
 * @version 1.00
 * @since 1.00
 */
public final class InsertUserDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "INSERT INTO IronJobs.UserAccount (username, birthDate, points, email, avatar, details, degree, pswd, currentJob, type, registrationTime) " +
											"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING *";

  /**
  * Connection to the database.
  */
	private final Connection con;

  /**
  * Object user which will be inserted into the database.
  */
	private final User user;

  /**
  * Creates a new object for inserting the user.
  * @param con The connection to the database.
  * @param user The new user to be inserted.
  */
	public InsertUserDatabase(final Connection con, final User user)
	{
		this.con = con;
		this.user = user;
	}

  /**
  * Inserts the new user into the database.
  * @return The user inserted.
  * @throws SQLException if the insertion goes wrong.
  */
	public User InsertUser() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		User e = null;

		try
		{
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, user.getUsername());
			pstmt.setString(2, user.getBirthDate());
			pstmt.setInt(3, user.getPoints());
			pstmt.setString(4, user.getEmail());
			pstmt.setInt(5, user.getAvatar());
			pstmt.setString(6, user.getDetails());
			pstmt.setString(7, user.getDegree());
			pstmt.setString(8, user.getPswd());
			pstmt.setString(9, user.getCurrentJob());
			pstmt.setInt(10, user.getType());
			pstmt.setString(11, user.getRegistrationTime());

			rs = pstmt.executeQuery();

			if (rs.next()) {
				e = new User(rs.getString("username"),
								rs.getString("birthDate"),
								rs.getInt("points"),
								rs.getString("email"),
								rs.getInt("avatar"),
								rs.getString("details"),
								rs.getString("degree"),
								rs.getString("pswd"),
								rs.getString("currentJob"),
								rs.getInt("type"),
								rs.getString("registrationTime")
								);
			}
		}

		finally
		{
			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}
	return e;

	}
}
