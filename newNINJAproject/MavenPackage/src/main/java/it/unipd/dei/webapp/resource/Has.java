package it.unipd.dei.webapp.resource;

/**
 * Represents an {@link Has} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Has
{
	/**
	 * the comment id
	 */
	private final int commentId;

	/**
	 * the questionId
	 */
	private final int questionId;

	/**
	 * Creates an Has
	 *
	 * @param commentId The ID of the comment.
	 *
	 * @param questionId The ID of the question to which the comment id referred.
	 */
	public Has(final int commentId, final int questionId)
	{
		this.commentId = commentId;
		this.questionId = questionId;
	}

	// Get methods

	/**
	 * Gets the ID of the comment.
	 * @return commentId The comment ID.
	 */
	public final int getCommentId()
	{
		return commentId;
	}

	/**
	 * Gets the question ID to which the comment is referred.
	 * @return questionID The question ID.
	 */
	public final int getQuestionId()
	{
		return questionId;
	}

	///////////////

	// Set methods

	///////////////
}
