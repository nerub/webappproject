

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SelectQuestionJoinPostByEmailDatabase;
import it.unipd.dei.webapp.resource.Question;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Searches questions by their Email.
 *
 * @author Eooardo Furlan (edoardo.furlan.1@studenti.dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class SelectQuestionJoinPostByEmailServlet extends AbstractDatabaseServlet {

    /**
     * Searches questions by their Email.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        //request parameter
        String email=null;

        // model
        List<Question> el = null;
        Message m = null;

        // retrieves the request parameter
        email = req.getParameter("email");

        try {

            // creates a new object for accessing the database and searching the questions
            el = new SelectQuestionJoinPostByEmailDatabase(getDataSource().getConnection(),email)
                    .SelectQuestionJoinPostByEmail();

            m = new Message("questions successfully searched.");

        } catch (NumberFormatException ex) {
            m = new Message("Cannot search for questions. Invalid input parameters: Email must be integer.",
                    "E100", ex.getMessage());
        } catch (SQLException ex) {
            m = new Message("Cannot search for questions: unexpected error while accessing the database.",
                    "E200", ex.getMessage());
        }

        // stores the question list and the message as a request attribute
        req.setAttribute("questionlist", el);
        req.setAttribute("message", m);

        // forwards the control to the search-question-result JSP
        req.getRequestDispatcher("/jsp/select-question-join-post-by-email-result.jsp").forward(req, res);

    }

}
