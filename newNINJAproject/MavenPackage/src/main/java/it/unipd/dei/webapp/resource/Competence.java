package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.*;

import java.io.*;

/**
 * Represents a {@link Competence} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Competence extends Resource
{
	/**
	 * the competence name
	 */
	private final String name;

	/**
	 * Creates a Competence
	 *
	 * @param name Name of the competence.
	 */
	public Competence(final String name)
	{
		this.name = name;
	}

	// Get methods

	/**
	 * returns the name
	 *
	 * @return name Name of the competence.
	 */
	public final String getName()
	{
		return name;
	}

	///////////////

	// Set methods

	///////////////

	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("competence");

		jg.writeStartObject();

		jg.writeStringField("name", name);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	/**
	 * Creates a {@code Competence} from its JSON representation.
	 *
	 * @param in the input stream containing the JSON document.
	 *
	 * @return the {@code Competence} created from the JSON representation.
	 *
	 * @throws IOException if something goes wrong while parsing.
	 */
	public static Competence fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		String jName = null;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "competence".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no competence object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "name":
						jp.nextToken();
						jName = jp.getText();
						break;
				}
			}
		}

		return new Competence(jName);
	}
}
