package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Solution;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
* Searches for all the solutions to a specific question.
*
* @version 1.00
* @since 1.00
*/

public final class SelectSolutionByQuestionIdDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "SELECT solutionId,upvote,downvote,isBest,solutiontext,isPremium,submitTime,email,questionId " +
	"FROM IronJobs.Solution WHERE IronJobs.Solution.questionId = ? ORDER BY upvote - downvote DESC";

  /**
  * Connection to the database.
  */
	private final Connection con;

  /**
  * ID of the question.
  */
	private final int questionId;

  /**
  * Creates a new object for searching the solutions.
  * @param con The connection to the database.
  * @param questionId The id of the question for which to search the solutions.
  */
	public SelectSolutionByQuestionIdDatabase(final Connection con, final int questionId)
	{
		this.con = con;
		this.questionId = questionId;
	}

  /**
  * Searches the solutions.
  * @return A list of the solutions to the question.
  * @throws SQLException if the search goes wrong.
  */
	public List<Solution> SelectSolution() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Solution> solutions = new ArrayList<Solution>();

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setInt(1, questionId);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				solutions.add(new Solution(rs.getInt("solutionId"),
											rs.getInt("upvote"),
											rs.getInt("downvote"),
											rs.getBoolean("isBest"),
											rs.getString("solutionText"),
											rs.getBoolean("isPremium"),
											rs.getString("submitTime"),
											rs.getString("email"),
											rs.getInt("questionId")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return solutions;
	}
}
