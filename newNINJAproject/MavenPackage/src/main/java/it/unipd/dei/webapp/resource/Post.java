package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.*;

import java.io.*;
import java.util.*;

/**
 * Represents a {@link Post} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Post extends Resource
{
	/**
	 * the email
	 */
	private final String email;
	/**
	 * the questionId
	 */
	private final int questionId;
	/**
	 * the postTime
	 */
	private final String postTime;

	/**
	 * Creates a new post entry.
	 *
	 * @param email of the user who created the post.
	 *
	 * @param questionId the questionId of the question created.
	 *
	 * @param postTime The timestamp of the Post.
	 */
	public Post(final String email, final int questionId, String postTime)
	{
		this.email = email;
		this.questionId = questionId;
		this.postTime = postTime;
	}

	// Get methods

	/**
	 * Returns the mail of the user.
	 * @return email of the user.
	 */
	public final String getEmail()
	{
		return email;
	}

	/**
	 * Returns the questionId.
	 * @return ID of the question.
	 */
	public final int getQuestionId()
	{
		return questionId;
	}

	/**
	 * Returns the postTime.
	 * @return postTime of the Post.
	 */
	public final String getPostTime()
	{
		return postTime;
	}

	///////////////

	// Set methods

	///////////////
   @Override
   public final void toJSON(final OutputStream out) throws IOException {
      final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

      jg.writeStartObject();

      jg.writeFieldName("post");

      jg.writeStartObject();

      jg.writeNumberField("questionId", questionId);

      jg.writeStringField("email", email);

      jg.writeStringField("postTime", postTime);

      jg.writeEndObject();

      jg.writeEndObject();

      jg.flush();
   }

   /**
    * Creates a {@code Post} from its JSON representation.
    *
    * @param in the input stream containing the JSON document.
    *
    * @return the {@code Post} created from the JSON representation.
    *
    * @throws IOException if something goes wrong while parsing.
    */
   public static Post fromJSON(final InputStream in) throws IOException {

      // the fields read from JSON
      String jEmail = null;
      int jQuestionId = -1;
      String jPostTime = null;

      final JsonParser jp = JSON_FACTORY.createParser(in);

      // while we are not on the start of an element or the element is not
      // a token element, advance to the next element (if any)
      while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "respondTo".equals(jp.getCurrentName()) == false) {

         // there are no more events
         if (jp.nextToken() == null) {
            throw new IOException("Unable to parse JSON: no respondTo object found.");
         }
      }

      while (jp.nextToken() != JsonToken.END_OBJECT) {

         if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

            switch (jp.getCurrentName()) {
               case "questionId":
                  jp.nextToken();
                  jQuestionId = jp.getIntValue();
                  break;
               case "email":
                  jp.nextToken();
                  jEmail = jp.getText();
                  break;
               case "postTime":
                  jp.nextToken();
                  jPostTime = jp.getText();
                  break;
            }
         }
      }

      return new Post(jEmail, jQuestionId, jPostTime);
   }

}
