package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Comment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Searches for the comments of a solution.
 *
 * @version 1.00
 * @since 1.00
 */


public final class SelectCommentIDBySolutionID
{
   /**
   * SQL statement that does the insertion.
   */
   private static final String STATEMENT = "SELECT IronJobs.Comment.commentId, upvote, downvote, commentText, writeTime, email"
   + " FROM IronJobs.Comment INNER JOIN IronJobs.respondTo"
   + " ON IronJobs.Comment.commentId = IronJobs.respondTo.commentId"
   + " WHERE IronJobs.respondTo.solutionid = ? ORDER BY upvote-downvote DESC";

   /**
   * Connection to the database.
   */
   private final Connection con;

   /**
   * Id of the solution.
   */
   private final int solutionId;

   /**
   * Creates a new object for searching the respondTo entry.
   * @param con The connection to the database.
   * @param solutionId The id of the solution to search for comments.
   */
    public SelectCommentIDBySolutionID(final Connection con, final int solutionId)
    {
         this.con = con;
         this.solutionId = solutionId;
    }

    /**
    * Searches the comments of the solution.
    * @return A list of the comments with all the attributes.
    * @throws SQLException if the search goes wrong.
    */
     public List<Comment> getComments() throws SQLException
     {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        // the results of the search
        final List<Comment> comments = new ArrayList<Comment>();

        try
        {
             pstmt = con.prepareStatement(STATEMENT);
             pstmt.setInt(1, solutionId);

           rs = pstmt.executeQuery();

           while (rs.next())
           {
             comments.add(new Comment(rs.getInt("commentId"),
                                   rs.getString("commentText"),
                                   rs.getInt("upvote"),
                                   rs.getInt("downvote"),
                                   rs.getString("writeTime"),
                                   rs.getString("email")));
           }
        }

        finally
        {
           if (rs != null)
           {
             rs.close();
           }

           if (pstmt != null)
           {
             pstmt.close();
           }

           con.close();
        }

        return comments;
     }
}
