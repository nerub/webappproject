package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Solution;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
* Searches for all the solutions provided by a user.
*
* @version 1.00
* @since 1.00
*/

public final class SelectSolutionByEmailDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "SELECT solutionId,upvote,downvote,isBest,solutiontext,isPremium,submitTime,email,questionId "+
											"FROM IronJobs.Solution WHERE IronJobs.Solution.email = ? ORDER BY upvote-downvote DESC";

  /**
  * Connection to the database.
  */
	private final Connection con;

  /**
  * Email of the user.
  */
	private final String email;

  /**
  * Creates a new object for searching the solutions.
  * @param con The connection to the database.
  * @param email The email of the user for which to search the solutions.
  */
	public SelectSolutionByEmailDatabase(final Connection con, final String email)
	{
		this.con = con;
		this.email = email;
	}

  /**
  * Searches the solutions.
  * @return A list of the solutions provided by the user.
  * @throws SQLException if the search goes wrong.
  */
	public List<Solution> SelectSolutionByEmail() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Solution> solutions = new ArrayList<Solution>();

		try
		{
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				solutions.add(new Solution(	rs.getInt("solutionId"),
											rs.getInt("upvote"),
											rs.getInt("downvote"),
											rs.getBoolean("isBest"),
											rs.getString("solutionText"),
											rs.getBoolean("isPremium"),
											rs.getString("submitTime"),
											rs.getString("email"),
											rs.getInt("questionId")));

			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return solutions;
	}
}
