
package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SelectTagDatabase;
import it.unipd.dei.webapp.resource.ResourceList;
import it.unipd.dei.webapp.resource.Tag;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Searches all the tags .
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class SelectTagServlet extends AbstractDatabaseServlet {

    /**
     * Searches all the tags .
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        // model
        List<Tag> el = null;
        Message m = null;

        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");

        try {

            // creates a new object for accessing the database and searching the tags
            el = new SelectTagDatabase(getDataSource().getConnection())
                    .SelectTag();

            m = new Message("Tags successfully searched.");

            res.setStatus(HttpServletResponse.SC_OK);

        } catch (NumberFormatException ex) {
            m = new Message("Cannot search for tags.",
                    "E100", ex.getMessage());
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        } catch (SQLException ex) {
            m = new Message("Cannot search for tags: unexpected error while accessing the database.",
                    "E200", ex.getMessage());
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        new ResourceList(el).toJSON(res.getOutputStream());
    }
    

    /* OLD JAVA OBJECT APPROACH
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {


        // model
        List<Tag> el = null;
        Message m = null;

        try {

            // creates a new object for accessing the database and searching the tags
            el = new SelectTagDatabase(getDataSource().getConnection())
                    .SelectTag();

            m = new Message("Tags successfully searched.");

        } catch (NumberFormatException ex) {
            m = new Message("Cannot search for tags.",
                    "E100", ex.getMessage());
        } catch (SQLException ex) {
            m = new Message("Cannot search for tags: unexpected error while accessing the database.",
                    "E200", ex.getMessage());
        }

        // stores the tag list and the message as a request attribute
        req.setAttribute("taglist", el);
        req.setAttribute("message", m);

        // forwards the control to the search-tag-result JSP
        req.getRequestDispatcher("/jsp/select-tag-result.jsp").forward(req, res);

    }
    */

}
