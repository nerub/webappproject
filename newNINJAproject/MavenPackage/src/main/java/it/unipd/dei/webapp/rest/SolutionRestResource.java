/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.*;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Manages the REST API for the {@link Solution} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class SolutionRestResource extends RestResource
{
    /**
     * Creates a new REST resource for managing {@code Solution} resources.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public SolutionRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con)
	{
        super(req, res, con);
    }

    /**
     * Creates a new solution into the database.
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void insertSolution() throws IOException
	{

        Solution e  = null;
        Message m = null;

        try
		{
            final Solution solution =  Solution.fromJSON(req.getInputStream());

            // creates a new object for accessing the database and stores the solution
            e = new InsertSolutionDatabase(con, solution).InsertSolution();

            if(e != null)
			{
                res.setStatus(HttpServletResponse.SC_CREATED);
                e.toJSON(res.getOutputStream());
            }
			
			else
			{
                // it should not happen
                m = new Message("Cannot create the solution: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
		
		catch (Throwable t)
		{
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505"))
			{
                m = new Message("Cannot create the solution: it already exists.", "E5A2", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            }
			
			else
			{
                m = new Message("Cannot create the solution: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Searches solutions by their questionId.
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void selectSolutionByQuestionId()  throws IOException
	{
        List<Solution> el  = null;
        Message m = null;

        try
		{
            // parse the URI path to extract the questionId
            String path = req.getRequestURI();
			String attributeName = "questionId";
            path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());

            final int questionId = Integer.parseInt(path.substring(1));


            // creates a new object for accessing the database and search the solutions
            el = new SelectSolutionByQuestionIdDatabase(con, questionId).SelectSolution();

            if(el != null)
			{
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(el).toJSON(res.getOutputStream());
            }
			
			else
			{
                // it should not happen
                m = new Message("Cannot search solution: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
		
		catch (Throwable t)
		{
            m = new Message("Cannot search solution: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}