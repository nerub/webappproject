package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Insert a new tag into the database.
 *
 * @version 1.00
 * @since 1.00
 */
public final class InsertTagDatabase
{

    /**
    * SQL statement that does the insertion.
    */
    private static final String STATEMENT = "INSERT INTO IronJobs.Tag (name) VALUES (?)";

    /**
    * SQL statement that does the insertion.
    */
    private final Connection con;

    /**
    * Object tag which will be inserted into the database.
    */
    private final Tag tag;

    /**
    * Creates a new object for inserting the tag.
    * @param con The connection to the database.
    * @param tag The new tag object to be inserted.
    */
    public InsertTagDatabase(final Connection con, final Tag tag)
    {
        this.con = con;
        this.tag = tag;
    }

    /**
    * Inserts the new tag into the database.
    * @throws SQLException if the insertion goes wrong.
    */
    public void InsertTag() throws SQLException
    {
        PreparedStatement pstmt = null;

        try
        {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setString(1, tag.getName());

            pstmt.execute();

        }

        finally
        {
            if (pstmt != null)
            {
                pstmt.close();
            }

            con.close();
        }
    }
}
