package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
* Searches for all the tags of a question.
*
* @version 1.00
* @since 1.00
*/

public final class SelectTagDatabaseByQuestionId
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "SELECT name FROM IronJobs.related WHERE questionId = ?";

  /**
  * Connection to the database.
  */
	private final Connection con;

   /**
   * Id of the question
   */
   private final int questionId;

  /**
  * Creates a new object for searching the tags.
  * @param con The connection to the database.
  * @param questionId The id of the question to searach for tags
  */
	public SelectTagDatabaseByQuestionId(final Connection con, int questionId)
	{
		this.con = con;
      this.questionId = questionId;
	}

  /**
  * Searches for the tags.
  * @return A list of the tags in the database.
  * @throws SQLException if the search goes wrong.
  */
	public List<Tag> selectTags() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

      final List<Tag> tags = new ArrayList<>();
      try
      {
         pstmt = con.prepareStatement(STATEMENT);
         pstmt.setInt(1, questionId);
         rs = pstmt.executeQuery();

         while (rs.next())
         {  // get all the tags and return them
            tags.add(new Tag(rs.getString("name")));
         }
      }
      finally
      {
         if (rs != null)
         {
            rs.close();
         }

         if (pstmt != null)
         {
            pstmt.close();
         }

         con.close();
      }

      return tags;
   }
}
