package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.*;

import java.io.*;
/**
 * Represents a {@link Possess} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Possess extends Resource
{
	/**
	 * the mail
	 */
	private final String email;
	/**
	 *  the name
	 */
	private final String name;
	/**
	 *  the rating
	 */
	private int rating;

	/**
	 * Creates a possess for a User
	 *
	 * @param email The mail of the User.
	 *
	 * @param name The name of the competence to add.
	 *
	 * @param rating The rating of the competence.
	 */
	public Possess(final String email, final String name, int rating)
	{
		this.email = email;
		this.name = name;
		this.rating = rating;
	}

	// Get methods

	/**
	 * Returns the email address of the user.
	 * @return email of the user.
	 */
	public final String getEmail()
	{
		return email;
	}

	/**
	 * Returns the name of the competence.
	 * @return name of the competence.
	 */
	public final String getName()
	{
		return name;
	}

	/**
	 * Returns the rating of the competence.
	 * @return rating of the competence.
	 */
	public int getRating()
	{
		return rating;
	}

	///////////////

	// Set methods

	/**
	 * Sets the rating of the competence.
	 * @param rating the value to assign to the competence.
	 */
	public void setRating(int rating)	// 0 <= rating <= 10
	{
		this.rating = rating;
	}

	///////////////
	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("possess");

		jg.writeStartObject();

		jg.writeStringField("email", email);

		jg.writeStringField("name", name);

		jg.writeNumberField("rating", rating);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	/**
	 * Creates a {@code Possess} from its JSON representation.
	 *
	 * @param in the input stream containing the JSON document.
	 *
	 * @return the {@code Possess} created from the JSON representation.
	 *
	 * @throws IOException if something goes wrong while parsing.
	 */
	public static Possess fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		String jEmail = null;
		String jName = null;
		int jRating = -1;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "possess".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no possess object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "email":
						jp.nextToken();
						jEmail = jp.getText();
						break;
					case "name":
						jp.nextToken();
						jName = jp.getText();
						break;
					case "rating":
						jp.nextToken();
						jRating = jp.getIntValue();
						break;
				}
			}
		}

		return new Possess(jEmail,jName,jRating);
	}
}
