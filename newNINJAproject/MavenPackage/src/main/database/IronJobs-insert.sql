--
-- Insertion into the UserAccount table
--
INSERT INTO IronJobs.UserAccount (Username, Birthdate, Points, Email, Avatar, Details, Degree, Pswd, CurrentJob, Type, RegistrationTime)
VALUES	('Acipi8', '21-10-1993', 6, 'acipi8@gmail.com', 0, 'I am an electronic engineer with nice hair', 'Information Engineering Bachelor Degree', 'ADCbp', 'Electronic Engineer', 1, '21-01-2018'),
	('polipo93', '27-09-1993', 3, 'finny@gmail.com', 5, 'I am a great worker and I love cats', 'Computer Science Master Degree', 'polipo', 'Unemployed', 1, '24-02-2018'),
	('chuljo', '14-05-1993', 5, 'wind0@gmail.com', 7, 'I like to think in an abstract way', 'Mathematics Master Degree', 'password', 'Researcher', 0, '29-03-2018');

--
-- Insertion into the Company table
--
INSERT INTO IronJobs.Company (VatIn, Name, Generality, Venue, RegistrationTime)
VALUES ('00001111222', 'Apple', 'Smartphone manufacturer', 'Cupertino', '01-01-2018'),
	('11112222333', 'Google', 'Online services', 'Mountain View', '03-01-2018'),
	('22223333444', 'Amazon', 'Online retailer', 'Seattle', '04-01-2018');

--
-- Insertion into the Competence table
--
INSERT INTO IronJobs.Competence(Name)
VALUES  ('Teamwork'),
	('Java'),
	('SQL'),
	('Android'),
	('HTML'),
	('CSS'),
	('Javascript'),
	('English'),
	('Leadership'),
	('Listening'),
	('Problem Solving'),
	('Writing Skills'),
	('Verbal Comunication'),
	('Adaptable'),
	('Reliability'),
	('Negotiation'),
	('Presentation'),
	('Public Speaking'),
	('Critical Observer'),
	('Flexible'),
	('Troubleshooting'),
	('Tolerant of Change'),
	('Data Mining'),
	('Cloud Management'),
	('Database Administration'),
	('Association Analysis'),
	('Linux'),
	('C++'),
	('Computer Vision'),
	('Project Management'),
	('Software Engineering'),
	('Machine Learning');

--
-- Insertion into the Question table
--
INSERT INTO IronJobs.Question(QuestionID, UpVote, DownVote, Difficulty, isPrivate, Body, Title, Views)
VALUES	(DEFAULT, 6, 2, 2, false, 'Given an array of integers,  return indices of the two numbers such that they add up to a specific target.
You may assume that each input would have exactly one solution,  and you may not use the same element twice.', 'Two Sum', 103),
	(DEFAULT, 9, 3, 3, false, 'Given a string,  find the length of the longest substring without repeating characters.', 'Longest Substring Without Repeating Characters', 305),
	(DEFAULT, 21, 5, 4, false, 'The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility) and then read line by line: "PAHNAPLSIIGYIR". Write the code that will take a string and make this conversion given a number of rows', 'ZigZag Conversion', 45),
	(DEFAULT, 2, 1, 1, false, 'Write a function to find the longest common prefix string amongst an array of strings. If there is no common prefix,  return an empty string', 'Longest Common Prefix', 5),
	(DEFAULT, 17, 4, 5, false, 'Given a 32-bit signed integer, reverse digits of an integer.', 'Reverse Integer',276);
--
-- Insertion into the Comment table
--
INSERT INTO IronJobs.Comment (CommentID, UpVote, DownVote, CommentText, WriteTime, Email)
VALUES	(DEFAULT, 6, 2, 'Keep track of the carry using a variable and simulate digits-by-digits sum starting from the head of list,  which contains the least-significant digit.', '11-09-2018', 'wind0@gmail.com'),
	(DEFAULT, 4, 0, 'Note that we use a dummy head to simplify the code. Without a dummy head,  you would have to write extra conditional statements to initialize the head''s value.', '12-09-2018', 'finny@gmail.com'),
	(DEFAULT, 8, 1, 'Check all the substring one by one to see if it has no duplicate character.', '13-09-2018', 'wind0@gmail.com'),
	(DEFAULT, 1, 0, 'To enumerate all substrings of a given string,  we enumerate the start and end indices of them. Suppose the start and end indices are iii and jjj,  respectively. Then we have 0?i<j?n0 \leq i \lt j \leq n0?i<j?n (here end index jjj is exclusive by convention). Thus,  using two nested loops with iii from 0 to n?1n - 1n?1 and jjj from i+1i+1i+1 to nnn,  we can enumerate all the substrings of s.', '14-09-2018', 'acipi8@gmail.com'),
	(DEFAULT, 6, 4, 'To check if one string has duplicate characters,  we can use a set. We iterate through all the characters in the string and put them into the set one by one. Before putting one character,  we check if the set already contains it. If so,  we return false. After the loop,  we return true.', '15-09-2018', 'acipi8@gmail.com');

--
-- Insertion into the Solution table
--
INSERT INTO IronJobs.Solution (SolutionID, UpVote, DownVote, isBest, SolutionText, isPremium, SubmitTime, Email, QuestionID)
VALUES	(DEFAULT, 5, 6, false, 'This is not the best solution', false, '20-10-2018', 'acipi8@gmail.com', 1),
	(DEFAULT, 15, 6, true, 'This is the best solution', false, '20-10-2018', 'wind0@gmail.com', 1),
	(DEFAULT, 0, 16, false, 'I cannot answer this one', false, '20-10-2018', 'wind0@gmail.com', 2),
	(DEFAULT, 2, 9, false, 'Anyone can solve this?', false, '20-10-2018', 'wind0@gmail.com', 3);

--
-- Insertion into the Tag table
--
INSERT INTO IronJobs.Tag (Name)
VALUES	('String'),
	('Tree'),
	('Stack'),
	('Design'),
	('Greedy'),
	('Heap');

--
-- Insertion into the Post table
--
INSERT INTO IronJobs.Post (PostTime, Email, QuestionID)
VALUES	('11-11-2018', 'acipi8@gmail.com', 1),
	('12-11-2018', 'acipi8@gmail.com', 2),
	('13-11-2018', 'wind0@gmail.com', 3),
	('15-11-2018', 'wind0@gmail.com', 4),
	('22-06-2018', 'finny@gmail.com', 5);


--
-- Insertion into the Reply table
--
INSERT INTO IronJobs.Reply (ReplyTime, CommentID1, CommentID2)
VALUES	('12-09-2018', 1, 2),
	('14-09-2018', 2, 4),
	('15-09-2018', 4, 5);

--
-- Insertion into the Submit table
--
INSERT INTO IronJobs.Submit (SubmitTime, VatIn, QuestionID)
VALUES	('12-03-2018', '00001111222', 1),
	('12-03-2018', '00001111222', 2),
	('13-03-2018', '11112222333', 3),
	('14-03-2018', '22223333444', 4);

--
-- Insertion into the RespondTo table
--
INSERT INTO IronJobs.RespondTo (CommentID, SolutionID, QuestionID)
VALUES	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 2);

--
-- Insertion into the Interview table
--
INSERT INTO IronJobs.Interview(InterviewID, ExpiringDate, Description, CreateTime, VatIn)
VALUES  (DEFAULT, '11-09-2019', 'First Interview', '10-05-2018', '00001111222'),
	(DEFAULT, '12-05-2019', 'Second Interview', '11-05-2018', '22223333444'),
	(DEFAULT, '13-05-2019', 'Third Interview', '11-05-2018', '22223333444'),
	(DEFAULT, '18-05-2019', 'Fourth Interview', '19-05-2018', '22223333444'),
	(DEFAULT, '19-05-2019', 'Fifth Interview', '12-05-2018', '11112222333');

--
-- Insertion into the Compose table
--
INSERT INTO IronJobs.Compose (InterviewID, QuestionID)
VALUES	(1, 1),
	(1, 2),
	(2, 3),
	(3, 4);


--
-- Insertion into the Require table
--
INSERT INTO IronJobs.Require(Name, InterviewID)
VALUES	('Teamwork', '1'),
	('Java', 1),
	('Android', 1),
	('SQL', 2),
	('CSS', 2),
	('English', 4),
	('Javascript', 5),
	('Teamwork', 5),
	('Teamwork', 3);

--
-- Insertion into the Possess table
--
INSERT INTO IronJobs.Possess(Rating, Email, Name)
VALUES	(8, 'acipi8@gmail.com', 'Teamwork'),
	(6, 'acipi8@gmail.com', 'English'),
	(2, 'acipi8@gmail.com', 'Java'),
	(5, 'finny@gmail.com', 'Android'),
	(9, 'finny@gmail.com', 'SQL'),
	(10, 'finny@gmail.com', 'CSS'),
	(1, 'wind0@gmail.com', 'Problem Solving'),
	(9, 'wind0@gmail.com', 'English'),
	(4, 'wind0@gmail.com', 'Leadership');

--
-- Insertion into the Take table
--
INSERT INTO IronJobs.Take(TakeTime, Email, InterviewID)
VALUES	('10-06-2018', 'acipi8@gmail.com', 1),
	('10-06-2018', 'acipi8@gmail.com', 2),
	('10-06-2018', 'acipi8@gmail.com', 3),
	('10-06-2018', 'acipi8@gmail.com', 4),
	('10-06-2018', 'acipi8@gmail.com', 5),
	('11-06-2018', 'finny@gmail.com', 1),
	('12-06-2018', 'finny@gmail.com', 2),
	('14-06-2018', 'finny@gmail.com', 3),
	('15-06-2018', 'finny@gmail.com', 4),
	('10-06-2018', 'wind0@gmail.com', 2);

--
-- Insertion into the Related table
--
INSERT INTO IronJobs.Related (QuestionID, Name)
VALUES	(1, 'Tree'),
	(1, 'Stack'),
	(2, 'Stack'),
	(2, 'Design'),
	(2, 'Greedy'),
	(3, 'Stack'),
	(3, 'Heap'),
	(4, 'Stack'),
	(4, 'Heap'),
	(4, 'String'),
	(5, 'String'),
	(5, 'Greedy'),
	(5, 'Design');

--
-- Insertion into the Has table
--
INSERT INTO IronJobs.Has (QuestionID, CommentID)
VALUES	(1, 1),
	(1, 2),
	(2, 1),
	(2, 4),
	(2, 5);
