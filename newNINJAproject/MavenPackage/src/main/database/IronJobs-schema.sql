-- #################################################################################################
-- ## Creation of a schema to avoid name clashes                                                  ##
-- #################################################################################################

-- Drop the IronJobs schema, if exists, and any object within it
DROP SCHEMA IF EXISTS IronJobs CASCADE;

-- Create the IronJobs schema
CREATE SCHEMA IronJobs;

-- #################################################################################################
-- ## Creation of the tables                                                                      ##
-- #################################################################################################

--
-- This table represents a generic user account
--
CREATE TABLE IronJobs.UserAccount
(
  Username VARCHAR(20) NOT NULL,
  Birthdate TEXT NOT NULL,
  Points INT NOT NULL,
  Email TEXT NOT NULL,
  Avatar INT NOT NULL,
  Details TEXT,
  Degree TEXT NOT NULL,
  Pswd VARCHAR(30) NOT NULL,
  CurrentJob TEXT,
  Type INT NOT NULL CHECK (Type >= 0 AND Type <= 3),
  RegistrationTime TEXT NOT NULL,
  PRIMARY KEY (Email)
);

--
-- This table represents a company
--
CREATE TABLE IronJobs.Company
(
  VatIn CHAR(11) NOT NULL,
  Name VARCHAR(30) NOT NULL,
  Generality TEXT,
  Venue TEXT NOT NULL,
  RegistrationTime TEXT NOT NULL,
  PRIMARY KEY (VatIn)
);

--
-- This table represents a competence of a user
--
CREATE TABLE IronJobs.Competence
(
  Name VARCHAR(30) NOT NULL,
  PRIMARY KEY (Name)
);

--
-- This table represents a question
--
CREATE TABLE IronJobs.Question
(
  QuestionID SERIAL NOT NULL,
  UpVote INT NOT NULL CHECK(UpVote >= 0),
  DownVote INT NOT NULL CHECK(DownVote >= 0),
  Difficulty INT NOT NULL CHECK (Difficulty > 0 AND Difficulty <= 5),
  isPrivate BOOLEAN NOT NULL,
  Body TEXT NOT NULL,
  Title TEXT NOT NULL,
  Views INT NOT NULL CHECK (Views >= 0),
  PRIMARY KEY (QuestionID)
);

--
-- This table represents the comment written by an user to a solution or a question
--
CREATE TABLE IronJobs.Comment
(
  CommentID SERIAL NOT NULL,
  UpVote INT NOT NULL  CHECK (UpVote >= 0),
  DownVote INT NOT NULL  CHECK (DownVote >= 0),
  CommentText TEXT NOT NULL,
  WriteTime TEXT NOT NULL,
  Email TEXT NOT NULL,
  PRIMARY KEY (CommentID),
  FOREIGN KEY (Email) REFERENCES IronJobs.UserAccount(Email)
);

--
-- This table represents the solution to a question
--
CREATE TABLE IronJobs.Solution
(
  SolutionID SERIAL NOT NULL,
  UpVote INT NOT NULL CHECK (UpVote >= 0),
  DownVote INT NOT NULL CHECK (DownVote >= 0),
  isBest BOOLEAN NOT NULL,
  SolutionText TEXT NOT NULL,
  isPremium BOOLEAN NOT NULL,
  SubmitTime TEXT NOT NULL,
  Email TEXT NOT NULL,
  QuestionID SERIAL NOT NULL,
  PRIMARY KEY (SolutionID, QuestionID),
  FOREIGN KEY (Email) REFERENCES IronJobs.UserAccount(Email),
  FOREIGN KEY (QuestionID) REFERENCES IronJobs.Question(QuestionID)
);

--
-- This table represents a tag
--
CREATE TABLE IronJobs.Tag
(
  Name VARCHAR(30) NOT NULL,
  PRIMARY KEY (Name)
);

--
-- This table represents the relationship between an user and a question
--
CREATE TABLE IronJobs.Post
(
  PostTime TEXT NOT NULL,
  Email TEXT NOT NULL,
  QuestionID SERIAL NOT NULL,
  PRIMARY KEY (Email, QuestionID),
  FOREIGN KEY (Email) REFERENCES IronJobs.UserAccount(Email),
  FOREIGN KEY (QuestionID) REFERENCES IronJobs.Question(QuestionID)
);

--
-- This table represents the relationship between two comments
--
CREATE TABLE IronJobs.Reply
(
  ReplyTime TEXT NOT NULL,
  CommentID1 SERIAL NOT NULL,
  CommentID2 SERIAL NOT NULL,
  PRIMARY KEY (CommentID1, CommentID2),
  FOREIGN KEY (CommentID1) REFERENCES IronJobs.Comment(CommentID),
  FOREIGN KEY (CommentID2) REFERENCES IronJobs.Comment(CommentID)
);

--
-- This table represents the relationship between an user and a solution
--
CREATE TABLE IronJobs.Submit
(
  SubmitTime TEXT NOT NULL,
  VatIn CHAR(11) NOT NULL,
  QuestionID SERIAL NOT NULL,
  PRIMARY KEY (VatIn, QuestionID),
  FOREIGN KEY (VatIn) REFERENCES IronJobs.Company(VatIn),
  FOREIGN KEY (QuestionID) REFERENCES IronJobs.Question(QuestionID)
);

--
-- This table represents the relationship between a comment and a solution
--
CREATE TABLE IronJobs.RespondTo
(
  CommentID SERIAL NOT NULL,
  SolutionID SERIAL NOT NULL,
  QuestionID SERIAL NOT NULL,
  PRIMARY KEY (CommentID, SolutionID, QuestionID),
  FOREIGN KEY (CommentID) REFERENCES IronJobs.Comment(CommentID),
  FOREIGN KEY (SolutionID, QuestionID) REFERENCES IronJobs.Solution(SolutionID, QuestionID)
);

--
-- This table represents an interview proposed by a company to an user
--
CREATE TABLE IronJobs.Interview
(
  InterviewID SERIAL NOT NULL,
  ExpiringDate TEXT NOT NULL,
  Description TEXT,
  CreateTime TEXT NOT NULL,
  VatIn CHAR(11) NOT NULL,
  PRIMARY KEY (InterviewID),
  FOREIGN KEY (VatIn) REFERENCES IronJobs.Company(VatIn)
);

--
-- This table represents the relationship between an interview and a question
--
CREATE TABLE IronJobs.Compose
(
  InterviewID SERIAL NOT NULL,
  QuestionID SERIAL NOT NULL,
  PRIMARY KEY (InterviewID, QuestionID),
  FOREIGN KEY (InterviewID) REFERENCES IronJobs.Interview(InterviewID),
  FOREIGN KEY (QuestionID) REFERENCES IronJobs.Question(QuestionID)
);

--
-- This table represents the relationship between an interview and a competence
--
CREATE TABLE IronJobs.Require
(
  Name VARCHAR(30) NOT NULL,
  InterviewID SERIAL NOT NULL,
  PRIMARY KEY (Name, InterviewID),
  FOREIGN KEY (Name) REFERENCES IronJobs.Competence(Name),
  FOREIGN KEY (InterviewID) REFERENCES IronJobs.Interview(InterviewID)
);

--
-- This table represents the relationship between an user and a competence
--
CREATE TABLE IronJobs.Possess
(
  Rating INT NOT NULL CHECK (Rating >= 1 AND Rating <= 10),
  Email TEXT NOT NULL,
  Name VARCHAR(30) NOT NULL,
  PRIMARY KEY (Email, Name),
  FOREIGN KEY (Email) REFERENCES IronJobs.UserAccount(Email),
  FOREIGN KEY (Name) REFERENCES IronJobs.Competence(Name)
);

--
-- This table represents the relationship between an user and an interview
--
CREATE TABLE IronJobs.Take
(
  TakeTime TEXT NOT NULL,
  Email TEXT NOT NULL,
  InterviewID SERIAL NOT NULL,
  PRIMARY KEY (Email, InterviewID),
  FOREIGN KEY (Email) REFERENCES IronJobs.UserAccount(Email),
  FOREIGN KEY (InterviewID) REFERENCES IronJobs.Interview(InterviewID)
);

--
-- This table represents the relationship between a tag and a question
--
CREATE TABLE IronJobs.Related
(
  QuestionID SERIAL NOT NULL,
  Name VARCHAR(30) NOT NULL,
  PRIMARY KEY (QuestionID, Name),
  FOREIGN KEY (QuestionID) REFERENCES IronJobs.Question(QuestionID),
  FOREIGN KEY (Name) REFERENCES IronJobs.Tag(Name)
);

--
-- This table represents the relationship between a question and a comment
--
CREATE TABLE IronJobs.Has
(
  QuestionID SERIAL NOT NULL,
  CommentID SERIAL NOT NULL,
  PRIMARY KEY (QuestionID, CommentID),
  FOREIGN KEY (QuestionID) REFERENCES IronJobs.Question(QuestionID),
  FOREIGN KEY (CommentID) REFERENCES IronJobs.Comment(CommentID)
);
