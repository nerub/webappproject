--
--SELECTS
--

--
--Find the UserName, Birthdate, Points, Email, Avatar, Details, Degree, Pswd,
--CurrentJob, Type, RegistrationTime of the accounts with the specific Email inserted
--
SELECT UserName, Birthdate, Points, Email, Avatar, Details, Degree, Pswd, CurrentJob, Type, RegistrationTime
    FROM IronJobs.UserAccount
	  WHERE Email = ?;

--
--Retrieve the Name of all the tags
--
SELECT Name
  FROM IronJobs.Tag;

--
--Find solutionId,upvote,downvote,isBest,solutiontext,isPremium,submitTime,email and questionId
--of the solutions with the specific QuestionID inserted
--
SELECT solutionId,upvote,downvote,isBest,solutiontext,isPremium,submitTime,email,questionId
    FROM IronJobs.Solution 
    WHERE IronJobs.Solution.QuestionID = ?;

--
--Find SolutionID, SolutionText, isPremium, isBest, UpVote, DownVote, SubmitTime, QuestionID and Email
--of the solutions with the specific Email inserted
--

SELECT SolutionID, UpVote, DownVote, isBest, SolutionText, isPremium, SubmitTime, Email, QuestionID
		FROM IronJobs.Solution
		WHERE IronJobs.Solution.Email = ?;

--
--Find questionId, difficulty, isPrivate, upvote, downvote, views, body, title of the question elements related with a specific Tag name in 
-- Related table 
--
SELECT IronJobs.Question.questionId, difficulty, isPrivate, upvote, downvote, views, body, title 
		FROM IronJobs.Related INNER JOIN IronJobs.Question 
		ON IronJobs.Related.questionId = IronJobs.Question.questionId 
		WHERE name = ?;

--
--Find QuestionID, Difficulty, isPrivate, UpVote, DownVote, Views, Body and Title 
--of the questions whose posts have the specific Email inserted
--
SELECT IronJobs.Question.QuestionID, Difficulty, isPrivate, UpVote, DownVote, Views, Body, Title
    FROM IronJobs.Post INNER JOIN IronJobs.Question
    ON IronJobs.Post.QuestionID = IronJobs.Question.QuestionID
    WHERE IronJobs.Post.Email = ?;

--
--Retrieve QuestionID, Difficulty, isPrivate, UpVote, DownVote, Views, Body and Title 
--of all the questions
--
SELECT QuestionID, Difficulty, isPrivate, UpVote, DownVote, Views, Body, Title
		FROM IronJobs.Question;

--
--Find SolutionID, UpVote, DownVote, isBest, SolutionText, isPremium, SubmitTime, Email, QuestionID
--of the solutions with the specific QuestionID inserted
--
SELECT SolutionID, UpVote, DownVote, isBest, SolutionText, isPremium, SubmitTime, Email, QuestionID
		FROM IronJobs.Solution
		WHERE IronJobs.Solution.QuestionID = ?;

--
--Retrieve Name of all the competences
--
SELECT Name
    FROM IronJobs.Competence;

--
--Find CommentID, CommentText, UpVote, DownVote, WriteTime
--of the comments associated with the specific QuestionID inserted
--

SELECT CommentID, CommentText, UpVote, DownVote, WriteTime 
	FROM IronJobs.Comment INNER JOIN IronJobs.Has
	ON IronJobs.Comment.CommentID = IronJobs.Has.CommentID
	WHERE IronJobs.Has.QuestionID = ?;

--
--Find questionId, difficulty, isPrivate, upvote, downvote, views, body, title
--of the Question associated with the specific QuestionID inserted
--

SELECT questionId, difficulty, isPrivate, upvote, downvote, views, body, title
	FROM IronJobs.Question
	WHERE IronJobs.Question.questionId = ?

--
--INSERTS
--

--
--Insert a new user with the specific values of: Username, Birthdate, Points, Email, Avatar, Details, Degree, Pswd, CurrentJob, Type, RegistrationTime
--
INSERT INTO IronJobs.User (UserName, Birthdate, Points, Email, Avatar, Details, Degree, Pswd, CurrentJob, Type, RegistrationTime)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);

--
--Insert a new Tag with the specific values of: Name
--

INSERT INTO IronJobs.Tag (name)
    VALUES (?);

--
--Insert a new solution with the specific values of: SolutionID, UpVote, DownVote, isBest, SolutionText, isPremium, SubmitTime, Email, QuestionID
--
INSERT INTO IronJobs.Solution (SolutionID, UpVote, DownVote, isBest, SolutionText, isPremium, SubmitTime, Email, QuestionID)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);

--
--Insert a new respondto elements with the specific values of: SolutionID, QuestionID, CommentID
--
INSERT INTO IronJobs.RespondTo (SolutionID, QuestionID, CommentID) 
    VALUES (?, ?, ?);

--
--Insert a new possess element with the specific values of: Email, Name, Rating
--
INSERT INTO IronJobs.Possess (Email, Name, Rating) 
    VALUES (?, ?, ?);

--
--Insert a new comment with the specific values of: CommentID, CommentText, UpVote, DownVote, WriteTime
--
INSERT INTO IronJobs.Comment (CommentID, CommentText, UpVote, DownVote, WriteTime) 
    VALUES (?, ?, ?, ?, ?);

--
--Insert a new Competence with the specific values of: name
--
INSERT INTO IronJobs.Competence (name)
    VALUES (?)



--
--DELETES
--

--
--Delete the comment element with the specific CommentID inserted
--
DELETE
    FROM IronJobs.Comment
    WHERE IronJobs.Comment.commentId = ?

--
--Delete the respondTo elements with the specific CommentID inserted
--
DELETE
    FROM IronJobs.RespondTo
    WHERE IronJobs.RespondTo.commentId = ?