
# POST METHODS

## Creation Competence

curl -v -X POST -H "Content-Type: application/json" -d "{\"competence\":{\"name\":\"CompetenzaRest1\"}}" http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/competence

## Creation possess
curl -v -X POST -H "Content-Type: application/json" -d "{\"possess\":{\"rating\":5, \"email\":\"acipi8@gmail.com\",\"name\":\"CompetenzaRest1\"}}" http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/possess

## Creation User
curl -v -X POST -H "Content-Type: application/json" -d "{\"user\":{\"username\":\"pippo\",\"birthDate\":\"21-10-1993\",\"points\":20,\"email\":\"pippo@gmail.com\",\"avatar\":1,\"details\":\"dettagli\",\"degree\":\"first\",\"pswd\":\"pippo\",\"currentJob\":\"barbone\",\"type\":1,\"registrationTime\":\"11-10-1999\"}}" http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/user

## Creation Solution
curl -v -X POST -H "Content-Type: application/json" -d "{\"solution\":{\"solutionId\":7,\"upvote\":1,\"downvote\":1,\"isBest\":false,\"solutionText\":\"TestoSoluzione\",\"isPremium\":true,\"submitTime\":\"11-10-1999\",\"email\":\"acipi8@gmail.com\",\"questionId\":1}}" http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/solution

## Creation Comment
curl -v -X POST -H "Content-Type: application/json" -d "{\"comment\":{\"commentId\":7,\"upvote\":2,\"downvote\":1,\"commentText\":\"TestoCommento\",\"writeTime\":\"11-10-1999\" \"email\":\"acipi8@gmail.com\"}}" http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/comment

## Creation RespondTo
curl -v -X POST -H "Content-Type: application/json" -d "{\"respondTo\":{\"solutionId\":4, \"questionId\":3,\"commentId\":3}}" http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/respondTo


# GET

## Select QuestionByQestionId
curl -v -G http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/question/1

## Select SolutionByQuestionId
curl -v -G http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/solution/questionId/1

## Select CommentJoinHasByQuestionId
curl -v -G http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/comment/questionId/1

## Search a user by email
curl -v -G http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/user/example@email.com


# DELETE


## Delete RespondTo by commentId
curl -v -X DELETE http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/respondTo/3

## Delete comment by commentId
curl -v -X DELETE http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/comment/3
