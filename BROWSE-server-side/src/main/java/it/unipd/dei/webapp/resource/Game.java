package it.unipd.dei.webapp.resource;

/**
 * Represents a {@link Game} resource.
 *
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */
public class Game {

	// The id of the game 
	private final int id;

	// The category of the game 
	private final String category;

	// The game name
	private final String name;

	// The age range for the game 
	private final String ageRange;

	// The price for the game 
	private final double price;

	/**
	 * Creates a game.
	 *
	 * @param id
	 *            the id.
	 * @param category
	 *            the category.
	 * @param name
	 *            game name.
	 * @param ageRange
	 *            age range.
	 * @param price
	 *            price.
	 */

	public Game(final int id, final String category, final String name, final String ageRange, final double price) {
		this.id = id;
		this.category = category;
		this.name = name;
		this.ageRange = ageRange;
		this.price = price;
	}

	/**
	 * Returns the id.
	 *
	 * @return the id.
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Returns the category.
	 *
	 * @return the category.
	 */
	public final String getCategory() {
		return category;
	}

	/**
	 * Returns the name.
	 *
	 * @return the name.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Returns the age range.
	 *
	 * @return the age range.
	 */
	public final String getAgeRange() {
		return ageRange;
	}

	/**
	 * Returns the price.
	 *
	 * @return the price.
	 */
	public final double getPrice() {
		return price;
	}

}
