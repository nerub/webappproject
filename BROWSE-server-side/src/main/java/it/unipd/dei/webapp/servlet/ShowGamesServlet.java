package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.ShowGamesDatabase;
import it.unipd.dei.webapp.resource.Game;
import it.unipd.dei.webapp.resource.Message;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


/**
 * Show all games into the database. 
 * 
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */

public final class ShowGamesServlet extends AbstractDatabaseServlet
{
    /**
   * Show the games into the database. 
   * 
   * @param req
   *            the HTTP request from the client.
   * @param res
   *            the HTTP response from the server.
   * 
   * @throws ServletException
   *             if any error occurs while executing the servlet.
   * @throws IOException
   *             if any error occurs in the client/server communication.
   */
   
   public void doPost(HttpServletRequest req, HttpServletResponse res)
           throws ServletException, IOException {
      
      // model
      List<Game> el = null;
      Message m = null;

      try {

      // creates a new object for accessing the database and searching the employees
      el = new ShowGamesDatabase(getDataSource().getConnection())
          .showGames();

      m = new Message("Games successfully searched.");
      
    } catch (NumberFormatException ex) {
      m = new Message("Cannot search for games.", 
          "E100", ex.getMessage());
    } catch (SQLException ex) {
        m = new Message("Cannot search for games: unexpected error while accessing the database.", 
            "E200", ex.getMessage());
    }


  /* PAGINA HTML CREATA PER MOSTRARE IL RISULTATO */
  
    // set the MIME media type of the response
    res.setContentType("text/html; charset=utf-8");

    // get a stream to write the response
    PrintWriter out = res.getWriter();

    // write the HTML page
    out.printf("<!DOCTYPE html>%n");
    
    out.printf("<html lang=\"en\">%n");
    out.printf("<head>%n");
    out.printf("<meta charset=\"utf-8\">%n");
    out.printf("<title>Browse Games</title>%n");
    out.printf("</head>%n");

    out.printf("<body>%n");
    out.printf("<h1>Browse Games</h1>%n");
    out.printf("<hr/>%n");

    if(m.isError()) {
      out.printf("<ul>%n");
      out.printf("<li>error code: %s</li>%n", m.getErrorCode());
      out.printf("<li>message: %s</li>%n", m.getMessage());
      out.printf("<li>details: %s</li>%n", m.getErrorDetails());
      out.printf("</ul>%n");
    } else {
      out.printf("<p>%s</p>%n", m.getMessage());
      
      out.printf("<table>%n");
      out.printf("<tr>%n");
      out.printf("<td>Id</td><td>Category</td><td>Name</td><td>Age Range</td><td>Price</td>%n");
      out.printf("</tr>%n");
      
      for(Game e: el) {
        out.printf("<tr>%n");
        out.printf("<td>%s</td><td>%s</td><td>%s</td><td>%s</td>%n", 
          e.getId(), e.getCategory(), e.getName(), e.getAgeRange(), e.getPrice());
        out.printf("</tr>%n");
      }
      out.printf("</table>%n");
    }

    out.printf("</body>%n");
    
    out.printf("</html>%n");

    // flush the output stream buffer
    out.flush();

    // close the output stream
    out.close();

  }

}
