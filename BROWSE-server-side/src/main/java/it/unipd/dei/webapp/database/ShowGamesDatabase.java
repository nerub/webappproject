package it.unipd.dei.webapp.database;

/* Importo la tabella del database che mi interessa */
import it.unipd.dei.webapp.resource.Game;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class ShowGamesDatabase
{
	/* Statement SQL per SELECT */
	/* Le tabelle da cui si attinge devono essere tutte importate */
	private static final String STATEMENT = "SELECT *" +
											"FROM games.Game";

	/* Connessione al databsase*/
    private final Connection con;

    /**
  	* Creates a new object for searching the games.
  	* @param con The connection to the database.
  	*/

    /* Costruttore, inserisco tutte le variabili*/
    public ShowGamesDatabase(final Connection con)
    {
    	this.con = con;
    }

	/**
  	* Searches for the games.
  	* @return A list of the games in the database.
  	* @throws SQLException if the search goes wrong.
  	*/

	public List<Game> showGames() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Game> games = new ArrayList<Game>(); //competences è una variabile nuova

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				games.add(new Game(  	rs.getInt("id"),
										rs.getString("category"),
										rs.getString("name"),
										rs.getString("ageRange"),
										rs.getDouble("price")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return games;
	}
}