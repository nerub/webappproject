package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Game;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Creates a new game into the database.
 * 
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */
public final class CreateGameDatabase {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "INSERT INTO Ferro.Game (id, category, name, ageRange, price) VALUES (DEFAULT, ?, ?, ?, ?)";

	/**
	 * The connection to the database
	 */
	private final Connection con;

	/**
	 * The employee to be stored into the database
	 */
	private final Game game;

	/**
	 * Creates a new object for storing an employee into the database.
	 * 
	 * @param con
	 *            the connection to the database.
	 * @param game
	 *            the game to be stored into the database.
	 */
	public CreateGameDatabase(final Connection con, final Game game) {
		this.con = con;
		this.game = game;
	}

	/**
	 * Stores a new employee into the database
	 * 
	 * @throws SQLException
	 *             if any error occurs while storing the employee.
	 */
	public void createGame() throws SQLException {

		PreparedStatement pstmt = null;

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setInt(1, game.getId());
			pstmt.setString(2, game.getCategory());
			pstmt.setString(3, game.getName());
			pstmt.setString(4, game.getAgeRange());
			pstmt.setDouble(4, game.getPrice());

			pstmt.execute();

		} finally {
			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

	}
}
