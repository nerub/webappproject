package it.unipd.dei.webapp.resource;

/**
 * Represents a {@link Game} resource.
 *
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */
public class Game {

	// The id of the game 
	private final int id;

	// The category of the game 
	private final String category;

	// The game name
	private final String name;

	// The age range for the game 
	private final String ageRange;

	// The price for the game 
	private final double price;

	/**
	 * Creates a game.
	 *
	 * @param id
	 *            the id.
	 * @param category
	 *            the category.
	 * @param name
	 *            game name.
	 * @param ageRange
	 *            age range.
	 * @param price
	 *            price.
	 */

	public Game(final int id, final String category, final String name, final String ageRange, final double price) {
		this.id = id;
		this.category = category;
		this.name = name;
		this.ageRange = ageRange;
		this.price = price;
	}

	/**
	 * Returns the id.
	 *
	 * @return the id.
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Returns the category.
	 *
	 * @return the category.
	 */
	public final String getCategory() {
		return category;
	}

	/**
	 * Returns the name.
	 *
	 * @return the name.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Returns the age range.
	 *
	 * @return the age range.
	 */
	public final String getAgeRange() {
		return ageRange;
	}

	/**
	 * Returns the price.
	 *
	 * @return the price.
	 */
	public final double getPrice() {
		return price;
	}

	/**************/
	/* Parte JSON */
	/**************/

	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("Game");

		jg.writeStartObject();

		jg.writeNumberField("id", id); // Per gli int

		jg.writeStringField("category", category); // Per le stringhe

		jg.writeStringField("name", name);

		jg.writeStringField("ageRange", ageRange);

		jg.writeNumberField("price", price);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	public static <NomeClasse> fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		int jId = -1;
		String jCategory = null;
		String jName = null;
		String jAgeRange = null;
		int jPrice = -1;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "Game".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no comment object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "id":
						jp.nextToken();
						jId = jp.getIntValue();
						break;
					case "category":
						jp.nextToken();
						jCategory = jp.getText();
						break;
					case "name":
						jp.nextToken();
						jName = jp.getText();
						break;
					case "ageRange":
						jp.nextToken();
						jAgeRange = jp.getText();
						break;
					case "price":
						jp.nextToken();
						jPrice = jp.getDoubleValue();
						break;
				}
			}
		}

		return new Comment(jId, jCategory, jName, jAgeRange, jPrice);
	}
}
