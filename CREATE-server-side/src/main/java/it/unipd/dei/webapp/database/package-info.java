

/**
 * Provides access to the underlying database.
 *
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */
package it.unipd.dei.webapp.database;