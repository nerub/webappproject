package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreateGameDatabase;
import it.unipd.dei.webapp.resource.Game;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Creates a new game into the database. 
 * 
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */
public final class CreateGameServlet extends AbstractDatabaseServlet {

	/**
	 * Creates a new game into the database. 
	 * 
	 * @param req
	 *            the HTTP request from the client.
	 * @param res
	 *            the HTTP response from the server.
	 * 
	 * @throws ServletException
	 *             if any error occurs while executing the servlet.
	 * @throws IOException
	 *             if any error occurs in the client/server communication.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// request parameters
		int id = -1;
		String category = null;
		String name = null;
		String ageRange = null;
		double price = -1;

		// model
		Game e  = null;
		Message m = null;

		try{
			// retrieves the request parameters
			id = Integer.parseInt(req.getParameter("id"));
			category = req.getParameter("category");
			name = req.getParameter("name");
			ageRange = req.getParameter("ageRange");
			price = Double.parseDouble(req.getParameter("price"));

			// creates a new employee from the request parameters
			e = new Game(id, category, name, ageRange, price);

			// creates a new object for accessing the database and stores the employee
			new CreateGameDatabase(getDataSource().getConnection(), e).createGame();
			
			m = new Message(String.format("Game %s successfully created.", category));

		} catch (NumberFormatException ex) {
			m = new Message("Cannot create the game. Invalid input parameters: id must be integer.", 
					"E100", ex.getMessage());
		} catch (SQLException ex) {
			if (ex.getSQLState().equals("23505")) {
				m = new Message(String.format("Cannot create the game: game %s already exists.", name),
						"E300", ex.getMessage());
			} else {
				m = new Message("Cannot create the game: unexpected error while accessing the database.", 
						"E200", ex.getMessage());
			}
		}
		
		// 3.CreateTag
      	System.out.println(name);
        req.setAttribute("Game", e);
        req.setAttribute("message", m);
        req.getRequestDispatcher("/jsp/create-game-result.jsp").forward(req, res);
        //
		
	}

}
