

/**
 * Manages the Web application (controller and view).
 *
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */
package it.unipd.dei.webapp.servlet;