
/**
 * Provides resources for representing and exchanging data about the main entities of the 
 * application.
 *
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */
package it.unipd.dei.webapp.resource;