package it.unipd.dei.webapp.resource;

/**
 * Represents a {@link Dogs} resource.
 *
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */
public class Game {

	// The id of the dog 
	private final int id;

	// The category of the dog 
	private final String category;

	// The image of the dog
	//QUI NON SAPREI COME GESTIRE L'IMMAGINE
	//private final 

	/**
	 * Creates a dog.
	 *
	 * @param id
	 *            the id.
	 * @param category
	 *            the category.
	 */

	public Game(final int id, final String category) {
		this.id = id;
		this.category = category;
	}

	/**
	 * Returns the id.
	 *
	 * @return the id.
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Returns the category.
	 *
	 * @return the category.
	 */
	public final String getCategory() {
		return category;
	}

}
