# Modifiche aggiunte a al progetto per gestire anche rest

* [x] **Pom.xml** (importare dipendenza json)(NB: se maven si incazza come una bestia bisgna updatare le referenze)
* [x] **Message.java**
* [x] **Resource.java**
* [x] **ResourceList.java**
* [x] **CARTELLA REST** nella stessa di servlet
* [x] **RestResource.java**

Delle successive bisgogna aggiungerne/modificarne per ogni query gestita

* [x] **Web.xml** (aggiungere riferimento al restmanager, e dargli l'ordine di ridirezionare ogni rest request)
* [x] Ogni **classe.java** entità nella cartella Resource(Bisogna aggiungere i metodi toJSON e fromJSON)
    * [x] user
    * [x] competence
    * [x] possess
    * [x] question
    * [x] solution
    * [x] comment
    * [x] respondTo
* [x] **EntitàRestResource.java** (Una per ogni entità, estende la RestResource e deve gestire con metodi ad hoc tutte le azioni necessarie riguardo ad una entità: creare,updatere,deletare,leggere)
In particolare dobbiamo implementare:
    * [x] insertUser
    * [x] insertCompetence
    * [x] insertPossess
    * [x] selectQuestionByQuestionId
    * [x] selectSolutionByQuestionId
    * [x] selectCommentJoinHasByQuestionId
    * [x] insertSolution
    * [x] insertComment
    * [x] insertRespondTo
    * [x] deleteComment
    * [x] deleteRespondToByCommentId
* [x] **QueryDatabase** Per ogni query riportata sopra bisogna aggiornare le insert..Databasae, e creare le mancanti.
* [x] **RestManagerServlet.java** (Oltre a delle cose sempre uguali, bisogna mettere per ogni entità un metodod ProcessEntità che gestisce tutte le possibili chiamate.)
Dentro questa classe vengono gestiti tutti i tipi di richieste che il server puo' ricevere. Quindi opera un discernimento dell'uri, comprendendo quale entità stiamo andando a manovrare. A seconda dell'entità va a chiamre il metodo processaEntità definito all'interno di questa classe che si occupa di fare le cose a seconda del metodo http.
    * [x] insertUser
    * [x] insertCompetence
    * [x] insertPossess
    * [x] selectQuestionByQuestionId
    * [x] selectSolutionByQuestionId
    * [x] selectCommentJoinHasByQuestionId
    * [x] insertSolution
    * [x] insertComment
    * [x] insertRespondTo
    * [x] deleteComment
    * [x] deleteRespondToByCommentId