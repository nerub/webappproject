package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Comment;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Insert a new comment into the database.
 *
 * @version 1.00
 * @since 1.00
 */
public final class InsertCommentDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "INSERT INTO IronJobs.Comment (commentId, CommentText, upvote, downvote, writeTime) " +
											"VALUES (?, ?, ?, ?, ?)" +
											" RETURNING *";

  /**
  * Connection to the database.
  */
	private final Connection con;

  /**
  * Object comment which will be inserted into the database.
  */
	private final Comment comment;

  /**
  * Creates a new object for inserting a new comment.
  * @param con The connection to the database.
  * @param comment The new comment to be inserted.
  */
	public InsertCommentDatabase(final Connection con, final Comment comment)
	{
		this.con = con;
		this.comment = comment;
	}

  /**
  * Inserts the new comment into the database.
  * @return The comment inserted.
  * @throws SQLException if the insertion goes wrong.
  */
	public Comment InsertComment() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Comment e = null;

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

			pstmt.setInt(1, comment.getCommentId());
			pstmt.setString(2, comment.getCommentText());
			pstmt.setInt(3, comment.getUpvote());
			pstmt.setInt(4, comment.getDownvote());
			pstmt.setString(5, comment.getWriteTime());

			rs = pstmt.executeQuery();
			if (rs.next()) {
				e = new Comment(rs.getInt("commentId"),
						rs.getString("commentText"),
						rs.getInt("upvote"),
						rs.getInt("downvote"),
						rs.getString("writeTime"));
			}
		}

		finally
		{
			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}
		return e;
	}
}
