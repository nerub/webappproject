package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Competence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Searches for the competences.
 *
 * @version 1.00
 * @since 1.00
 */
public final class SelectCompetenceDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "SELECT name FROM IronJobs.Competence";

  /**
  * Connection to the database.
  */
	private final Connection con;

  /**
  * Creates a new object for searching the database for competences.
  * @param con The connection to the database.
  */
	public SelectCompetenceDatabase(final Connection con)
	{
		this.con = con;
	}

  /**
  * Searches the database for compentences.
  * @return A list of the competences found.
  * @throws SQLException if the search goes wrong.
  */
	public List<Competence> SelectCompetence() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Competence> competences = new ArrayList<Competence>();

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				competences.add(new Competence(rs.getString("name")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return competences;
	}
}
