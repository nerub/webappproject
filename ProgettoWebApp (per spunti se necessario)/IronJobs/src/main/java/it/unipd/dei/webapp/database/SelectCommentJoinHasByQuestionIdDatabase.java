package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Comment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Searches for the comments of a question.
 *
 * @version 1.00
 * @since 1.00
 */
public final class SelectCommentJoinHasByQuestionIdDatabase
{
  /**
  * SQL statement that does the insertion.
  */
  private static final String STATEMENT = "SELECT IronJobs.Comment.commentId, upvote, downvote, commentText, writeTime"
  + " FROM IronJobs.Comment INNER JOIN IronJobs.Has"
  + " ON IronJobs.Comment.commentId = IronJobs.Has.commentId "
  + " WHERE IronJobs.Has.questionId = ?";

  /**
  * Connection to the database.
  */
  private final Connection con;

  /**
  * Id of the question.
  */
  private final int questionId;

  /**
  * Creates a new object for searching the question.
  * @param con The connection to the database.
  * @param questionId The id of the question to search for comments.
  */
	public SelectCommentJoinHasByQuestionIdDatabase(final Connection con, final int questionId)
	{
        this.con = con;
        this.questionId = questionId;
	}

  /**
  * Searches the comments of the question.
  * @return A list of the comments with all the attributes.
  * @throws SQLException if the search goes wrong.
  */
	public List<Comment> SelectComment() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Comment> comments = new ArrayList<Comment>();

		try
		{
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, questionId);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				comments.add(new Comment(rs.getInt("commentId"),
											rs.getString("commentText"),
											rs.getInt("upvote"),
											rs.getInt("downvote"),
											rs.getString("writeTime")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return comments;
	}
}
