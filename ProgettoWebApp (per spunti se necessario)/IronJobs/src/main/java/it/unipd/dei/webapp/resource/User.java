package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.*;

import java.io.*;

/**
 * Represents a {@link User} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class User extends Resource
{
	/**
	 * The username
	 */
	private final String username;
	/**
	 * The password
	 */
	private String pswd;
	/**
	 * The birth date
	 */
	private final String birthDate;
	/**
	 * The number of points acquired
	 */
	private int points;
	/**
	 * The email
	 */
	private final String email;
	/**
	 * The index of the avatar chosen
	 */
	private int avatar;
	/**
	 * The type of user
	 */
	private int type;
	/**
	 * The current job
	 */
	private String currentJob;
	/**
	 * The account details
	 */
	private String details;
	/**
	 * The degree of the user
	 */
	private String degree;
	/**
	 * The registration time
	 */
	private final String registrationTime;


	/**
	 * Creates a user.
	 *
	 * @param username
	 *            the username.
	 * @param birthDate
	 *            the birthdate.
	 * @param points
	 *            number of points acquired.
	 * @param email
	 *            email of the user.
	 * @param avatar
	 *            index of the avatar chosen.
	 * @param details
	 *            account details.
	 * @param degree
	 *            user degree.
	 * @param pswd
	 *            the password.
	 * @param currentJob
	 *            the user current job.
	 * @param type
	 *            the type of account.
	 * @param registrationTime
	 *            the registration time.
	 */
	public User(final String username, final String birthDate, int points, final String email, int avatar,
			 String details, String degree, String pswd, String currentJob, int type, final String registrationTime)
	{
		this.username = username;
		this.pswd = pswd;
		this.birthDate = birthDate;
		this.points = points;
		this.email = email;
		this.avatar = avatar;
		this.type = type;
		this.currentJob = currentJob;
		this.details = details;
		this.degree = degree;
		this.registrationTime = registrationTime;
	}

	// Get methods

	/**
	 * Returns the username.
	 *
	 * @return username.
	 */
	public final String getUsername()
	{
		return username;
	}

	/**
	 * Returns the password
	 *
	 * @return pswd
	 */
	public String getPswd()
	{
		return pswd;
	}

	/**
	 * Returns the birthdate.
	 *
	 * @return birthDate.
	 */
	public String getBirthDate()
	{
		return birthDate;
	}

	/**
	 * Returns the points.
	 *
	 * @return points.
	 */
	public int getPoints()
	{
		return points;
	}

	/**
	 * Returns the email.
	 *
	 * @return email.
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * Returns the avatar index.
	 *
	 * @return avatar.
	 */
	public int getAvatar()
	{
		return avatar;
	}

	/**
	 * Returns the type.
	 *
	 * @return type.
	 */
	public int getType()
	{
		return type;
	}

	/**
	 * Returns the current job.
	 *
	 * @return currentJob.
	 */
	public String getCurrentJob()
	{
		return currentJob;
	}

	/**
	 * Returns the details.
	 *
	 * @return details.
	 */
	public String getDetails()
	{
		return details;
	}

	/**
	 * Returns the degree.
	 *
	 * @return degree.
	 */
	public String getDegree()
	{
		return degree;
	}

	/**
	 * Returns the registration time.
	 *
	 * @return registrationTime.
	 */
	public String getRegistrationTime()
	{
		return registrationTime;
	}

	///////////////

	// Set methods

	/**
	 * Set the password.
	 *
	 * @param pswd.
	 */
	public void setPswd(String pswd)
	{
		this.pswd = pswd;
	}

	/**
	 * Set the points.
	 *
	 * @param points.
	 */
	public void setPoints(int points)
	{
		this.points = points;
	}

	/**
	 * Set the avatar index.
	 *
	 * @param avatar.
	 */
	public void setAvatar(int avatar)
	{
		this.avatar = avatar;
	}

	/**
	 * Set the type.
	 *
	 * @param type.
	 */
	public void setType(int type)
	{
		this.type = type;
	}

	/**
	 * Set the current job.
	 *
	 * @param currentJob
	 */
	public void setCurrentJob(String currentJob)
	{
		this.currentJob = currentJob;
	}

	/**
	 * Set the details.
	 *
	 * @param details.
	 */
	public void setDetails(String details)
	{
		this.details = details;
	}

	/**
	 * Set the degree.
	 *
	 * @param degree.
	 */
	public void setDegree(String degree)
	{
		this.degree = degree;
	}

	///////////////

	// JSON part

	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("user");

		jg.writeStartObject();

		jg.writeStringField("username", username);

		jg.writeStringField("birthDate", birthDate);

		jg.writeNumberField("points", points);

		jg.writeStringField("email", email);

		jg.writeNumberField("avatar", avatar);

		jg.writeStringField("details",details);

		jg.writeStringField("degree", degree);

		jg.writeStringField("pswd",pswd);

		jg.writeStringField("currentJob", currentJob);

		jg.writeNumberField("type", type);

		jg.writeStringField("registrationTime", registrationTime);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	/**
	 * Creates a {@code User} from its JSON representation.
	 *
	 * @param in the input stream containing the JSON document.
	 *
	 * @return the {@code User} created from the JSON representation.
	 *
	 * @throws IOException if something goes wrong while parsing.
	 */
	public static User fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		String jUsername = null;
		String jBirthDate = null;
		int jPoints=-1;
		String jEmail = null;
		int jAvatar=-1;
		String jDetails = null;
		String jDegree = null;
		String jPswd = null;
		String jCurrentJob = null;
		int jType =-1;
		String jRegistrationTime = null;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "user".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no user object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "username":
						jp.nextToken();
						jUsername = jp.getText();
						break;
					case "birthDate":
						jp.nextToken();
						jBirthDate = jp.getText();
						break;
					case "points":
						jp.nextToken();
						jPoints = jp.getIntValue();
						break;
					case "email":
						jp.nextToken();
						jEmail = jp.getText();
						break;
					case "avatar":
						jp.nextToken();
						jAvatar = jp.getIntValue();
						break;
					case "details":
						jp.nextToken();
						jDetails = jp.getText();
						break;
					case "degree":
						jp.nextToken();
						jDegree = jp.getText();
						break;
					case "pswd":
						jp.nextToken();
						jPswd = jp.getText();
						break;
					case "currentJob":
						jp.nextToken();
						jCurrentJob = jp.getText();
						break;
					case "type":
						jp.nextToken();
						jType = jp.getIntValue();
						break;
					case "registrationTime":
						jp.nextToken();
						jRegistrationTime = jp.getText();
						break;

				}
			}
		}

		return new User(jUsername,jBirthDate,jPoints,jEmail,jAvatar,jDetails,jDegree,jPswd,jCurrentJob,jType,jRegistrationTime);
	}
}
