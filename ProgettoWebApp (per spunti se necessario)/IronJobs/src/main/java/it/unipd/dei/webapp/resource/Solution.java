package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.*;

import java.io.*;
/**
 * Represents a {@link Solution} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Solution extends Resource
{
	/**
	 *the solution id
	 */
	private final int solutionId;
	/**
	 *the solution text
	 */
	private final String solutionText;
	/**
	 *the premium state
	 */
	private final boolean isPremium;
	/**
	 *the best state
	 */
	private boolean isBest;
	/**
	 *the number of upvotes
	 */
	private int upvote;
	/**
	 *the number of downvotes
	 */
	private int downvote;
	/**
	 *the submit time
	 */
	private final String submitTime;
	/**
	 *the questionId
	 */
	private final int questionId;
	/**
	 *the email
	 */
	private final String email;

	/**
	 *Creates a new solution
	 * the questio id
	 * @param solutionId
	 * the number upvote
	 * @param upvote
	 * the number of downvote
	 * @param downvote
	 * the best state
	 * @param isBest
	 * the solution text
	 * @param solutionText
	 * the premium state
	 * @param isPremium
	 * the submit time
	 * @param submitTime
	 * the user email
	 * @param email
	 * the questio id
	 * @param questionId
	 *
	 */
	
	public Solution(final int solutionId,int upvote, int downvote, boolean isBest, final String solutionText, final boolean isPremium,
			final String submitTime, final String email,final int questionId ){
		this.solutionId = solutionId;
		this.questionId = questionId;
		this.solutionText = solutionText;
		this.isPremium = isPremium;
		this.isBest = isBest;
		this.upvote = upvote;
		this.downvote = downvote;	
		this.submitTime = submitTime;
		this.email=email;
	}




	// Get methods

	/**
	 *the question id
	 * @return questioId
	 */
	public int getQuestionId()
	{
		return questionId;
	}

	/**
	 *the submit time
	 * @return
	 */
	public String getSubmitTime()
	{
		return submitTime;
	}

	/**
	 *the user email
	 * @return email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 *the number of downvotes
	 * @return number of downvotes
	 */
	public int getDownvote()
	{
		return downvote;
	}

	/**
	 * the solutionId
	 * @return solutionId
	 */
	public final int getSolutionId()
	{
		return solutionId;
	}

	/**
	 *the solutionText
	 * @return solutionText
	 */
	public final String getSolutionText() { return solutionText; }

	/**
	 *number of upvotes
	 * @return number of upvotes
	 */
	public int getUpvote()
	{
		return upvote;
	}

	/**
	 *the best status
	 * @return best
	 */
	public boolean getIsBest() { return isBest; }

	/**
	 *the premium status
	 * @return
	 */
	public boolean getIsPremium()
	{
		return isPremium;
	}

	///////////////
	
	// Set methods

	/**
	 *the best state
	 * @param isBest
	 */
	public void setIsBest(boolean isBest)
	{
		this.isBest = isBest;
	}

	/**
	 * the number of upvotes
	 * @param upvote
	 */
	public void setUpvote(int upvote)
	{
		this.upvote = upvote;
	}

	/**
	 * the number of downvotes
	 * @param downvote
	 */
	public void setDownvote(int downvote)
	{
		this.downvote = downvote;
	}
	
	///////////////
	@Override
	public final void toJSON(final OutputStream out) throws IOException {


		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("solution");

		jg.writeStartObject();

		jg.writeNumberField("solutionId", solutionId);

		jg.writeNumberField("upvote", upvote);

		jg.writeNumberField("downvote",downvote);

		jg.writeBooleanField("isBest", isBest);

		jg.writeStringField("solutionText", solutionText);

		jg.writeBooleanField("isPremium", isPremium);

		jg.writeStringField("submitTie", submitTime);

		jg.writeStringField("email", email);

		jg.writeNumberField("questionId",questionId);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	/**
	 * Creates a {@code Solution} from its JSON representation.
	 *
	 * @param in the input stream containing the JSON document.
	 *
	 * @return the {@code Solution} created from the JSON representation.
	 *
	 * @throws IOException if something goes wrong while parsing.
	 */
	public static Solution fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		int jSolutionId = -1;
		String jSolutionText = null;
		boolean jIsPremium = false;
		boolean jIsBest = false;
		int jUpvote = -1;
		int jDownvote = -1;
		String jSubmitTime = null;
		int jQuestionId = -1;
		String jEmail = null;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "solution".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no solution object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "solutionId":
						jp.nextToken();
						jSolutionId = jp.getIntValue();
						break;
					case "upvote":
						jp.nextToken();
						jUpvote = jp.getIntValue();
						break;
					case "downvote":
						jp.nextToken();
						jDownvote = jp.getIntValue();
						break;
					case "isBest":
						jp.nextToken();
						jIsBest = jp.getBooleanValue();
						break;

					case "solutionText":
						jp.nextToken();
						jSolutionText = jp.getText();
						break;
					case "isPremium":
						jp.nextToken();
						jIsPremium = jp.getBooleanValue();
						break;
					case "submitTime":
						jp.nextToken();
						jSubmitTime = jp.getText();
						break;
					case "email":
						jp.nextToken();
						jEmail = jp.getText();
						break;
					case "questionId":
						jp.nextToken();
						jQuestionId= jp.getIntValue();
						break;

				}
			}
		}

		return new Solution(jSolutionId,jUpvote,jDownvote,jIsBest,jSolutionText,jIsPremium,jSubmitTime,jEmail,jQuestionId);
	}
}


