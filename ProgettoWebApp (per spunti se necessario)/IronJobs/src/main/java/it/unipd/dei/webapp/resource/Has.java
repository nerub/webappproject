package it.unipd.dei.webapp.resource;

/**
 * Represents an {@link Has} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Has
{
	/**
	 * the comment id
	 */
	private final int commentId;

	/**
	 * the questionId
	 */
	private final int questionId;

	/**
	 * Creates an Has
	 * the comment id
	 * @param commentId
	 * the questionId
	 * @param questionId
	 */
	public Has(final int commentId, final int questionId)
	{
		this.commentId = commentId;
		this.questionId = questionId;
	}
	
	// Get methods

	/**
	 * return the comment id
	 * @return commentId
	 */
	public final int getCommentId()
	{
		return commentId;
	}

	/**
	 * return th questionId
	 * @return questionID
	 */
	public final int getQuestionId()
	{
		return questionId;
	}
	
	///////////////
	
	// Set methods
	
	///////////////
}