package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.RespondTo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Deletes a comment from the RespondTo table in the database
 *
 * @version 1.00
 * @since 1.00
 */
public final class DeleteRespondToByCommentIdDatabase
{
    /**
    * SQL statement to be executed
    */
    private static final String STATEMENT = "DELETE "
            + " FROM IronJobs.RespondTo"
            + " WHERE IronJobs.RespondTo.commentId = ?"
            + " RETURNING *";
    /**
    * Connection to the databsase
    */
    private final Connection con;

    /**
    * Id of the comment to be deleted
    */
    private final int commentId;

    /**
    * Creates an object from which to delete the comment which id is passed to
    * the constructor.
    * @param con Connection to the database.
    * @param commentId ID of the comment to be deleted.
    */
    public DeleteRespondToByCommentIdDatabase(final Connection con, final int commentId)
    {
        this.con = con;
        this.commentId = commentId;
    }

    /**
    * Deletes the comment.
    * @return The comment deleted.
    * @throws SQLException if something goes wrong while doing the deletion.
    */
    public RespondTo DeleteRespondTo() throws SQLException
    {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        RespondTo e = null;

        try
        {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, commentId);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                     e = new RespondTo(rs.getInt("solutionId"),
                        rs.getInt("questionId"),
                        rs.getInt("commentId"));
            }

        }

        finally
        {
            if (pstmt != null)
            {
                pstmt.close();
            }

            con.close();
        }
        return e;
    }
}
