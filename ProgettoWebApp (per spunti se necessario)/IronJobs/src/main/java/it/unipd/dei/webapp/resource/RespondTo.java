package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.*;

import java.io.*;
/**
 * Represents a {@link RespondTo} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class RespondTo extends Resource
{
	/**
	 * the solution id
	 */
	private final int solutionId;
	/**
	 * the question id
	 */
	private final int questionId;
	/**
	 * the comment id
	 */
	private final int commentId;

	/**
	 * Creates a new RespondTo
	 * the solution id
	 * @param solutionId
	 * the question id
	 * @param questionId
	 * the comment id
	 * @param commentId
	 */
	public RespondTo(final int solutionId, final int questionId, final int commentId)
	{
		this.solutionId = solutionId;
		this.questionId = questionId;
		this.commentId = commentId;
	}

	// Get methods

	/**
	 * the solutionid
	 * @return solutionid
	 */
	public final int getSolutionId()
	{
		return solutionId;
	}

	/**
	 * the questionId
	 * @return questionId
	 */
	public final int getQuestionId()
	{
		return questionId;
	}

	/**
	 * the comment id
	 * @return comment id
	 */
	public final int getCommentId()
	{
		return commentId;
	}

	///////////////

	// Set methods

	///////////////

	// JSON part

	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);
			jg.writeStartObject();

			jg.writeFieldName("respondTo");

			jg.writeStartObject();

			jg.writeNumberField("commentId", commentId);

			jg.writeNumberField("solutionid", solutionId);

			jg.writeNumberField("questionid", questionId);

			jg.writeEndObject();

			jg.writeEndObject();

			jg.flush();
		}

		/**
		 * Creates a {@code RespondTo} from its JSON representation.
		 *
		 * @param in the input stream containing the JSON document.
		 *
		 * @return the {@code RespondTo} created from the JSON representation.
		 *
		 * @throws IOException if something goes wrong while parsing.
		 */
		public static RespondTo fromJSON(final InputStream in) throws IOException {

			// the fields read from JSON
			int jCommentId = -1;
			int jQuestionId = -1;
			int jSolutionId = -1;

			final JsonParser jp = JSON_FACTORY.createParser(in);

			// while we are not on the start of an element or the element is not
			// a token element, advance to the next element (if any)
			while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "respondTo".equals(jp.getCurrentName()) == false) {

				// there are no more events
				if (jp.nextToken() == null) {
					throw new IOException("Unable to parse JSON: no respondTo object found.");
				}
			}

			while (jp.nextToken() != JsonToken.END_OBJECT) {

				if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

					switch (jp.getCurrentName()) {
						case "commentId":
							jp.nextToken();
							jCommentId = jp.getIntValue();
							break;
						case "questionId":
							jp.nextToken();
							jQuestionId = jp.getIntValue();
							break;
						case "solutionId":
							jp.nextToken();
							jSolutionId = jp.getIntValue();
							break;
					}
				}
			}

			return new RespondTo(jSolutionId, jQuestionId, jCommentId);
		}
}
