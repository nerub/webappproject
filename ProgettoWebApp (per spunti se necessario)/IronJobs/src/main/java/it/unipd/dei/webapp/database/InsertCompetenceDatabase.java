package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Competence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Insert a new competence into the database.
 *
 * @version 1.00
 * @since 1.00
 */
public final class InsertCompetenceDatabase
{

    /**
    * SQL statement that does the insertion.
    */
    private static final String STATEMENT = "INSERT INTO IronJobs.Competence (name) VALUES (?) RETURNING *";

    /**
    * Connection to the database.
    */
    private final Connection con;

    /**
    * Object competence which will be inserted into the database.
    */
    private final Competence competence;

    /**
    * Creates a new object for inserting a new competence.
    * @param con The connection to the database.
    * @param competence The new competence to be inserted.
    */
    public InsertCompetenceDatabase(final Connection con, final Competence competence)
    {
        this.con = con;
        this.competence = competence;
    }

    /**
    * Inserts the new competence into the database.
    * @return The competence inserted.
    * @throws SQLException if the insertion goes wrong.
    */
    public Competence InsertCompetence() throws SQLException
    {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        Competence e = null;

        try
        {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setString(1, competence.getName());

            rs = pstmt.executeQuery();

            if (rs.next()) {
                e = new Competence(rs.getString("name"));
            }

        }

        finally
        {
            if (pstmt != null)
            {
                pstmt.close();
            }

            con.close();
        }
        return e ;
    }
}
