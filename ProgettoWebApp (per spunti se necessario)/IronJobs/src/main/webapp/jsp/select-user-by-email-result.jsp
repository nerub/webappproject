<%--
  Created by IntelliJ IDEA.
  User: nerub
  Date: 20/05/18
  Time: 0.47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Search User By Email Result</title>
</head>

<body>
<h1>Search User By Email Result</h1>
<hr/>

<!-- display the message -->
<c:import url="/jsp/include/show-message.jsp"/>

<!-- display the list of found users, if any -->
<c:if test='${not empty userlist}'>
    <table>
        <thead>
        <tr>
            <th>email</th><th>birthdate</th><th>points</th><th>username</th><th>avatar</th><th>type</th><th>currentjob</th><th>details</th><th>degree</th><th>registrationtime</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="user" items="${userlist}">
            <tr>
                <td><c:out value="${user.email}"/></td>
                <td><c:out value="${user.birthDate}"/></td>
                <td><c:out value="${user.points}"/></td>
                <td><c:out value="${user.username}"/></td>
                <td><c:out value="${user.avatar}"/></td>
                <td><c:out value="${user.type}"/></td>
                <td><c:out value="${user.currentJob}"/></td>
                <td><c:out value="${user.details}"/></td>
                <td><c:out value="${user.degree}"/></td>
                <td><c:out value="${user.registrationTime}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>
</body>
</html>
