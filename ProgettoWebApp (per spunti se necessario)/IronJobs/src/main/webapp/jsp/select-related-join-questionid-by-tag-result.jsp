<%--
  Created by IntelliJ IDEA.
  Question: nerub
  Date: 20/05/18
  Time: 0.47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Search Related QuestionId By Tag Results</title>
</head>

<body>
<h1>Search Related QuestionId By Tag Results</h1>
<hr/>

<!-- display the message -->
<c:import url="/jsp/include/show-message.jsp"/>

<!-- display the list of found questions, if any -->
<c:if test='${not empty questionidlist}'>

    <table>
        <thead>
        <tr>
            <th>questionid</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="related" items="${questionidlist}">
            <tr>
                <td><c:out value="${related.questionId}"/></td>

            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>
</body>
</html>
