package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.*;

import java.io.*;
/**
 * Represents a {@link Tag} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Tag extends Resource
{
	/**
	 * the Tag name
	 */
	private final String name;

	/**
	 * Creates a new Tag.
 	 * @param name of the new tag. Acts as the identifier.
	 */
	public Tag (final String name)
	{
		this.name = name;
	}

	// Get methods

	/**
	 * Returns the tag name.
	 * @return tag name.
	 */
	public String getName()
	{
		return name;
	}

	///////////////

	// Set methods

	///////////////
	//none

	///////////////
	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("tag");

		jg.writeStartObject();

		jg.writeStringField("name", name);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	/**
	 * Creates a {@code Tag} from its JSON representation.
	 *
	 * @param in the input stream containing the JSON document.
	 * @return the {@code Tag} created from the JSON representation.
	 * @throws IOException if something goes wrong while parsing.
	 */
	public static Tag fromJSON(final InputStream in) throws IOException {


		// the fields read from JSON
		String jName = null;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "tag".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no tag object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "name":
						jp.nextToken();
						jName = jp.getText();
						break;
				}
			}
		}
		return new Tag(jName);
	}
}


