/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.resource.*;
import it.unipd.dei.webapp.rest.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Manages the REST API for the different REST resources.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class RestManagerServlet extends AbstractDatabaseServlet
{
    /**
     * The JSON MIME media type
     */
    private static final String JSON_MEDIA_TYPE = "application/json";

    /**
     * The JSON UTF-8 MIME media type
     */
    private static final String JSON_UTF_8_MEDIA_TYPE = "application/json; charset=utf-8";

    /**
     * The any MIME media type
     */
    private static final String ALL_MEDIA_TYPE = "*/*";

    @Override
    protected final void service(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException
	{
        res.setContentType(JSON_UTF_8_MEDIA_TYPE);
        final OutputStream out = res.getOutputStream();

        try
		{
            // if the request method and/or the MIME media type are not allowed, return.
            // Appropriate error message sent by {@code checkMethodMediaType}
            if (!checkMethodMediaType(req, res))
			{
                return;
            }

            // if the requested resource was an User, delegate its processing and return
            if (processUser(req, res))
			{
                return;
            }

            // if the requested resource was a Competence, delegate its processing and return
            if (processCompetence(req, res))
			{
                return;
            }

            // if the requested resource was a Possess, delegate its processing and return
            if (processPossess(req, res))
			{
                return;
            }

            // if the requested resource was a Question, delegate its processing and return
            if (processQuestion(req, res))
			{
                return;
            }


            // if the requested resource was a Solution, delegate its processing and return
            if (processSolution(req, res))
			{
                return;
            }


            // if the requested resource was a Comment, delegate its processing and return
            if (processComment(req, res))
			{
                return;
            }


            // if the requested resource was a RespondTo, delegate its processing and return
            if (processRespondTo(req, res))
			   {
                return;
            }

            if (processPost(req, res))
            {
               return;
            }

            // if none of the above process methods succeeds, it means an unknow resource has been requested
            final Message m = new Message("Unknown resource requested.", "E4A6",
                    String.format("Requested resource is %s.", req.getRequestURI()));
            res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            m.toJSON(out);
        }

		finally
		{
            // ensure to always flush and close the output stream
            out.flush();
            out.close();
        }
    }

    /**
     * Checks that the request method and MIME media type are allowed.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request method and the MIME type are allowed; {@code false} otherwise.
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean checkMethodMediaType(final HttpServletRequest req, final HttpServletResponse res) throws IOException
	{
        final String method = req.getMethod();
        final String contentType = req.getHeader("Content-Type");
        final String accept = req.getHeader("Accept");
        final OutputStream out = res.getOutputStream();

        Message m = null;

        if (accept == null)
		{
            m = new Message("Output media type not specified.", "E4A1", "Accept request header missing.");
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            m.toJSON(out);

			return false;
        }

        if (!accept.contains(JSON_MEDIA_TYPE) && !accept.equals(ALL_MEDIA_TYPE))
		{
            m = new Message("Unsupported output media type. Resources are represented only in application/json.",
                    "E4A2", String.format("Requested representation is %s.", accept));
            res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            m.toJSON(out);

			return false;
        }

        switch (method)
		{
            case "GET":
            case "DELETE":
                // nothing to do
                break;

            case "POST":
            case "PUT":
                if (contentType == null)
				{
                    m = new Message("Input media type not specified.", "E4A3", "Content-Type request header missing.");
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    m.toJSON(out);

					return false;
                }

                if (!contentType.contains(JSON_MEDIA_TYPE))
				{
                    m = new Message("Unsupported input media type. Resources are represented only in application/json.",
                            "E4A4", String.format("Submitted representation is %s.", contentType));
                    res.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
                    m.toJSON(out);

					return false;
                }

                break;
            default:
                m = new Message("Unsupported operation.",
                        "E4A5", String.format("Requested operation %s.", method));
                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                m.toJSON(out);

				return false;
        }

        return true;
    }

    /**
     * Checks whether the request if for an {@link Competence} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code Competence}; {@code false} otherwise.
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processCompetence(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not an competence
        if (path.lastIndexOf("rest/competence") <= 0)
		{
            return false;
        }

        //NB: we need only to insert a new competence
        try
		{
            // strip everyhing until after the /competence
			String className = "competence";
            path = path.substring(path.lastIndexOf(className) + className.length());

            // the request URI is: /competence
            // if method POST, insert competence
            if (path.length() == 0 || path.equals("/"))
			{
                switch (method)
				{
                    case "POST":
                        new CompetenceRestResource(req, res, getDataSource().getConnection()).insertCompetence();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /competence.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }
            }
        }

		catch (Throwable t)
		{
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;
    }

    /**
     * Checks whether the request if for an {@link Possess} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code Possess}; {@code false} otherwise.
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processPossess(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not an possess
        if (path.lastIndexOf("rest/possess") <= 0)
		{
            return false;
        }

        try
		{
            // strip everyhing until after the /possess
            String className = "possess";
			path = path.substring(path.lastIndexOf(className) + className.length());

            // the request URI is: /possess
            // if method GET, list possesss
            // if method POST, insert possess
            if (path.length() == 0 || path.equals("/"))
			{
                switch (method)
				{
                    case "POST":
                        new PossessRestResource(req, res, getDataSource().getConnection()).insertPossess();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /possess.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }
            }
        }

		catch (Throwable t)
		{
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;
    }

    /**
     * Checks whether the request if for an {@link Solution} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code Solution}; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processSolution(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not an solution
        if(path.lastIndexOf("rest/solution") <= 0)
		{
            return false;
        }

        try
		{
            // strip everyhing until after the /solution
			String className = "solution";
            path = path.substring(path.lastIndexOf(className) + className.length());

            // the request URI is: /solution
            // if method GET, list solutions
            // if method POST, insert solution
            if (path.length() == 0 || path.equals("/"))
			{
                switch (method)
				{
                    case "GET":
                        //get not needed
                        break;
                    case "POST":
                        new SolutionRestResource(req, res, getDataSource().getConnection()).insertSolution();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /solution.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }
            }

			else
			{
                // the request URI is: /solution/questionId/{questionId}
                String attributeName = "questionId";
				if (path.contains(attributeName))
				{
                    path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());

                    if (path.length() == 0 || path.equals("/"))
					{
                        m = new Message("Wrong format for URI /solution/questionId/{questionId}: no {questionId} specified.",
                                "E4A7", String.format("Requesed URI: %s.", req.getRequestURI()));
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }

					else
					{
                        switch (method)
						{
                            case "GET":
                                // check that the parameter is actually an integer
                                try
								{
                                    Integer.parseInt(path.substring(1));

                                    new SolutionRestResource(req, res, getDataSource().getConnection()).selectSolutionByQuestionId();
                                } catch (NumberFormatException e) {
                                    m = new Message(
                                            "Wrong format for URI /solution/questionId/{questionId}: {questionId} is not an integer.",
                                            "E4A7", e.getMessage());
                                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                    m.toJSON(res.getOutputStream());
                                }

                                break;
                            default:
                                m = new Message("Unsupported operation for URI /solution/questionId/{questionId}.", "E4A5",
                                        String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                                break;
                        }
                    }
                }

				else
				{
                    // the request URI is: /solution/{any}
                    try
					{
                        // check that the parameter is actually an integer
                        Integer.parseInt(path.substring(1));

                        switch (method)
						{
                            case "GET":
                                //get not needed
                                break;
                            case "PUT":
                                //get not needed
                                break;
                            case "DELETE":
                                //get not needed
                                break;
                            default:
                                m = new Message("Unsupported operation for URI /solution/{any}.",
                                        "E4A5", String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                        }
                    }

					catch (NumberFormatException e)
					{
                        m = new Message("Wrong format for URI /solution/{any}: {any} is not an integer.",
                                "E4A7", e.getMessage());
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }
                }
            }
        }

		catch(Throwable t)
		{
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;
    }

    /**
     * Checks whether the request if for an {@link User} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code User}; {@code false} otherwise.
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processUser(HttpServletRequest req, HttpServletResponse res) throws IOException
	{

        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not an user
        if (path.lastIndexOf("rest/user") <= 0)
        {
            return false;
        }

        try
        {
            // strip everyhing until after the /user
			String className = "user";
            path = path.substring(path.lastIndexOf(className) + className.length());

            // the request URI is: /user
            // if method POST, insert user
            if (path.length() == 0 || path.equals("/"))
            {
                switch (method) {
                    case "POST":
                        new UserRestResource(req, res, getDataSource().getConnection()).insertUser();
                        break;

                    default:
                        m = new Message("Unsupported operation for URI /user.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }
            }
            else
            {
               switch(method)
               {
                  case "GET":
                     new UserRestResource(req, res, getDataSource().getConnection()).searchUser();
                     break;

                  default:
                     m = new Message("Unsupported operation for URI /user/email.",
                     "E4A5", String.format("Requested operation %s.", method));
                     res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                     m.toJSON(res.getOutputStream());
                     break;
               }
            }
        }

        catch (Throwable t)
        {
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;
    }

    /**
     * Checks whether the request if for an {@link Question} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code Question}; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processQuestion(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not an question
        if(path.lastIndexOf("rest/question") <= 0)
		{
            return false;
        }

        try
		{
            // strip everyhing until after the /question
			String className = "question";
            path = path.substring(path.lastIndexOf(className) + className.length());

            // the request URI is: /question
            // if method GET, list questions
            // if method POST, insert question
            if (path.length() == 0 || path.equals("/"))
			{
                switch (method)
				{
                    case "GET":
                        //get not needed
                        break;
                    case "POST":
                        //post not needed
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /question.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }
            }

			else
			{
                // the request URI is: /question/any/{any}
                String attributeName = "any";
				if (path.contains(attributeName))
				{
                    path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());

                    if (path.length() == 0 || path.equals("/"))
					{
                        m = new Message("Wrong format for URI /question/any/{any}: no {any} specified.",
                                "E4A7", String.format("Requesed URI: %s.", req.getRequestURI()));
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }

					else
					{
                        switch (method)
						{
                            case "GET":

                                // check that the parameter is actually an integer
                                try
								{
                                    Integer.parseInt(path.substring(1));

                                    //get not needed
                                }

								catch (NumberFormatException e)
								{
                                    m = new Message(
                                            "Wrong format for URI /question/any/{any}: {any} is not an integer.",
                                            "E4A7", e.getMessage());
                                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                    m.toJSON(res.getOutputStream());
                                }

                                break;
                            default:
                                m = new Message("Unsupported operation for URI /question/any/{any}.", "E4A5",
                                        String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                                break;
                        }
                    }
                }

				else
				{
                    // the request URI is: /question/{questionId}
                    try
					{
                        // check that the parameter is actually an integer
                        Integer.parseInt(path.substring(1));

                        switch (method)
						{
                            case "GET":
                                new QuestionRestResource(req, res, getDataSource().getConnection()).selectQuestionByQuestionId();
                                break;
                            case "PUT":
                                //put not needed
                                break;
                            case "DELETE":
                                //delete not needed
                                break;
                            default:
                                m = new Message("Unsupported operation for URI /question/{questionId}.",
                                        "E4A5", String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                        }
                    }

					catch (NumberFormatException e)
					{
                        m = new Message("Wrong format for URI /question/{questionId}: {questionId} is not an integer.",
                                "E4A7", e.getMessage());
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }
                }
            }
        }

		catch(Throwable t)
		{
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;
    }


    /**
     * Checks whether the request if for an {@link Comment} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code Comment}; {@code false} otherwise.
     *
     * @throws IOException if questionId error occurs in the client/server communication.
     */
    private boolean processComment(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not an comment
        if(path.lastIndexOf("rest/comment") <= 0)
		{
            return false;
        }

        try
		{
            // strip everyhing until after the /comment
			String className = "comment";
            path = path.substring(path.lastIndexOf(className) + className.length());

            // the request URI is: /comment
            // if method GET, list comments
            // if method POST, insert comment
            if (path.length() == 0 || path.equals("/"))
			{
                switch (method) {
                    case "GET":
                        //get not needed
                        break;
                    case "POST":
                        new CommentRestResource(req, res, getDataSource().getConnection()).insertComment();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /comment.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }
            }

			else
			{
                // the request URI is: /comment/questionId/{questionId}
                String attributeName = "questionId";
				if (path.contains(attributeName))
				{
                    path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());

                    if (path.length() == 0 || path.equals("/"))
					{
                        m = new Message("Wrong format for URI /comment/questionId/{questionId}: no {questionId} specified.",
                                "E4A7", String.format("Requesed URI: %s.", req.getRequestURI()));
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }

					else
					{
                        switch (method)
						{
                            case "GET":
                                // check that the parameter is actually an integer
                                try
								{
                                    Integer.parseInt(path.substring(1));

                                    new CommentRestResource(req, res, getDataSource().getConnection()).selectCommentJoinHasByQuestionId();
                                }

								catch (NumberFormatException e)
								{
                                    m = new Message(
                                            "Wrong format for URI /comment/questionId/{questionId}: {questionId} is not an integer.",
                                            "E4A7", e.getMessage());
                                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                    m.toJSON(res.getOutputStream());
                                }

                                break;
                            default:
                                m = new Message("Unsupported operation for URI /comment/questionId/{questionId}.", "E4A5",
                                        String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                                break;
                        }
                    }
                }

				else
				{
                    // the request URI is: /comment/{commentId}
                    try
					{
                        // check that the parameter is actually an integer
                        Integer.parseInt(path.substring(1));

                        switch (method)
						{
                            case "GET":
                                // get not needed
                                break;
                            case "PUT":
                                //put not needed
                                break;
                            case "DELETE":
                                new CommentRestResource(req, res, getDataSource().getConnection()).deleteComment();
                                break;
                            default:
                                m = new Message("Unsupported operation for URI /comment/{commentId}.",
                                        "E4A5", String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                        }
                    }

					catch (NumberFormatException e)
					{
                        m = new Message("Wrong format for URI /comment/{commentId}: {commentId} is not an integer.",
                                "E4A7", e.getMessage());
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }
                }
            }
        }

		catch(Throwable t)
		{
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;

    }

    /**
     * Checks whether the request if for an {@link RespondTo} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code RespondTo}; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processRespondTo(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not an respondTo
        if(path.lastIndexOf("rest/respondTo") <= 0)
		{
            return false;
        }

        try
		{
            // strip everyhing until after the /respondTo
			   String className = "respondTo";
            path = path.substring(path.lastIndexOf(className) + className.length());

            // the request URI is: /respondTo
            // if method GET, list respondTos based on the solutionid
            // if method POST, insert respondTo
            if (path.length() == 0 || path.equals("/"))
			   {
                switch (method)
				{
                    case "GET":
                        //get not needed
                        break;
                    case "POST":
                        new RespondToRestResource(req, res, getDataSource().getConnection()).insertRespondTo();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /respondTo.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }
            }

			else
			{
            // the request URI is: /respondTo/solutionid/{number}
            String attributeName = "solutionid";
				if (path.contains(attributeName))
				{
               switch (method)
               {
                   case "GET":
                      path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());

                      if (path.length() > 1)
                      {
                         try
                         {
                           // Check if it is an integer
                           Integer.parseInt(path.substring(1));
                           new RespondToRestResource(req, res, getDataSource().getConnection()).getRespondToBySolutionId();
                      }
                         catch (NumberFormatException e)
                         {
                           m = new Message("Wrong format for URI /respondTo/solutionid/id: id is not an integer.",
                           "E4A7", e.getMessage());
                           res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                           m.toJSON(res.getOutputStream());
                         }
                      }

                   break;

                   default:
                       m = new Message("Unsupported operation for URI /respondTo/solutionid/{number}.", "E4A5",
                              String.format("Requested operation %s.", method));
                       res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                       m.toJSON(res.getOutputStream());
                   break;
               }

            }

				else
				{
                    // the request URI is: /respondTo/{commentId}
                    try
					{
                        // check that the parameter is actually an integer
                        Integer.parseInt(path.substring(1));

                        switch (method){

                            case "GET":
                                // get not needed
                                break;
                            case "PUT":
                                // put not needed
                                break;
                            case "DELETE":
                                new RespondToRestResource(req, res, getDataSource().getConnection()).deleteRespondTo();
                                break;
                            default:
                                m = new Message("Unsupported operation for URI /respondTo/{commentId}.",
                                        "E4A5", String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                        }
                    }

					catch (NumberFormatException e)
					{
                        m = new Message("Wrong format for URI /respondTo/{commentId}: {commentId} is not an integer.",
                                "E4A7", e.getMessage());
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }
                }
            }
        }

		catch(Throwable t)
		{
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;
    }

    /**
     * Checks whether the request if for an {@link Post} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code Post}; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processPost(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
      final String method = req.getMethod();
      final OutputStream out = res.getOutputStream();

      String path = req.getRequestURI();
      Message m = null;

      // the requested resource was not an respondTo
      if(path.lastIndexOf("post/questionId/") <= 0)
      {
          return false;
      }

      try
      {
         // strip everyhing until after the rest/post
         String className = "post/questionId";
         path = path.substring(path.lastIndexOf(className) + className.length());

         // the request URI is: post/{}
         // if method GET, list Post based on the questionid
         if (path.length() > 1)
         {
            try
            {
               Integer.parseInt(path.substring(1));
               switch (method)
               {
                  case "GET":
                  new PostRestResource(req, res, getDataSource().getConnection()).getUserByQuestion();
                  break;

                  default:
                     m = new Message("Unsupported operation for URI post/questionId.",
                     "E4A5", String.format("Requested operation %s.", method));
                     res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                     m.toJSON(res.getOutputStream());
                     break;
               }
            }
            catch (Exception e)
            {
               m = new Message("Wrong format for URI post/questionId/{questionid}: {questionid} is not an integer.",
                       "E4A7", e.getMessage());
               res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
               m.toJSON(res.getOutputStream());
            }
         }
         else
         {
            m = new Message("Unsupported operation for URI /post.",
                    "E4A5", String.format("Requested operation %s.", method));
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
         }
      }
      catch (Throwable t)
      {
         m = new Message("Unexpected error.", "E5A1", t.getMessage());
         res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
         m.toJSON(res.getOutputStream());
      }
      return true;
    }
}
