package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SelectTagDatabaseByQuestionId;
import it.unipd.dei.webapp.resource.ResourceList;
import it.unipd.dei.webapp.resource.Tag;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
* Searches all the tags .
*
* @author Teofan Clipa
* @version 1.00
* @since 1.00
*/
public final class SelectTagByQuestionIdServlet extends AbstractDatabaseServlet
{

   /**
   * Searches all the tags for a specific question.
   *
   * @param req
   *            the HTTP request from the client.
   * @param res
   *            the HTTP response from the server.
   *
   * @throws ServletException
   *             if any error occurs while executing the servlet.
   * @throws IOException
   *             if any error occurs in the client/server communication.
   */
   public void doPost(HttpServletRequest req, HttpServletResponse res)
           throws ServletException, IOException {

      // model
      List<Tag> el = null;
      Message m = null;

      int questionid = Integer.parseInt(req.getParameter("questionId"));

      try {

           // creates a new object for accessing the database and searching the tags
           el = new SelectTagDatabaseByQuestionId(getDataSource().getConnection(), questionid)
                   .selectTags();

           //m = new Message("Tags successfully searched.");

           res.setStatus(HttpServletResponse.SC_OK);

      } catch (NumberFormatException ex) {
           m = new Message("Cannot search for tags.",
                   "E100", ex.getMessage());
                   res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
           m.toJSON(res.getOutputStream());
      } catch (SQLException ex) {
           m = new Message("Cannot search for tags: unexpected error while accessing the database.",
                   "E200", ex.getMessage());
                   res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
           m.toJSON(res.getOutputStream());
      }

      // stores the tag list and the message as a request attribute
      req.setAttribute("taglist", el);
      // req.setAttribute("message", m);

      // forwards the control
      req.getRequestDispatcher("/jsp/question.jsp").forward(req, res);

   }


}
