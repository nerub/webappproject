package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Related} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Related
{
	/**
	 * the question id
	 */
	private final int questionId;
	/**
	 * the tag name
	 */
	private final String name;

	/**
	 * Creates a new related entry.
	 *
	 * @param questionId ID of the question.
	 *
	 * @param name The name of the tag the question is related to.
	 */
	public Related(final int questionId, final String name)
	{
		this.questionId = questionId;
		this.name = name;
	}

	// Get methods

	/**
	 * Returns the questionid.
	 * @return question id of the question.
	 */
	public final int getQuestionId()
	{
		return questionId;
	}

	/**
	 * Returns the tag name.
	 * @return tag name to which the questin is related.
	 */
	public final String getName()
	{
		return name;
	}

	///////////////

	// Set methods

	///////////////
}
