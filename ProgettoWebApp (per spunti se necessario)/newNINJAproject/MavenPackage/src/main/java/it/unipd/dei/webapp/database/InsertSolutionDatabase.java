package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Solution;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Insert a new solution into the database.
 *
 * @version 1.00
 * @since 1.00
 */
public final class InsertSolutionDatabase
{
	private static final String STATEMENT = "INSERT INTO IronJobs.Solution (SolutionID,UpVote,DownVote,isBest,SolutionText,isPremium,SubmitTime,Email,QuestionID) "+
                                            " VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?) " +
											"RETURNING *";

  /**
  * SQL statement that does the insertion.
  */
	private final Connection con;

  /**
  * Object solution which will be inserted into the database.
  */
	private final Solution solution;

  /**
  * Creates a new object for inserting the solution.
  * @param con The connection to the database.
  * @param solution The new solution object to be inserted.
  */
	public InsertSolutionDatabase(final Connection con, final Solution solution)
	{
		this.con = con;
		this.solution = solution;
	}

  /**
  * Inserts the new solution into the database.
  * @return The solution inserted.
  * @throws SQLException if the insertion goes wrong.
  */
	public Solution InsertSolution() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Solution e = null;

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

         pstmt.setInt(1, solution.getUpvote());
         pstmt.setInt(2, solution.getDownvote());
         pstmt.setBoolean(3, solution.getIsBest());
			pstmt.setString(4, solution.getSolutionText());
			pstmt.setBoolean(5, solution.getIsPremium());
			pstmt.setString(6, solution.getSubmitTime());
         pstmt.setString(7, solution.getEmail());
			pstmt.setInt(8, solution.getQuestionId());

			rs = pstmt.executeQuery();

			if (rs.next()) {
				e = new Solution(rs.getInt("solutionId"),
						rs.getInt("upvote"),
						rs.getInt("downvote"),
						rs.getBoolean("isBest"),
						rs.getString("solutionText"),
						rs.getBoolean("isPremium"),
						rs.getString("submitTime"),
						rs.getString("email"),
						rs.getInt("questionId"));
			}
		}

		finally
		{
			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}
		return e;
	}
}
