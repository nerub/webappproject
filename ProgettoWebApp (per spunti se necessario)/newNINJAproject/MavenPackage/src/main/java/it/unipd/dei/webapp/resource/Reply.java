package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Reply} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Reply
{
	/**
	 * the reply time
	 */
	private final String replyTime;
	/**
	 * the first comment
	 */
	private final int commentIdFirst;
	/**
	 * the second comment
	 */
	private final int commentIdSecond;

	/**
	 * Creates a new Reply.
	 *
	 * @param replyTime the timestamp of the reply.
	 *
	 * @param commentIdFirst the ID of the comment.
	 *
	 * @param commentIdSecond the ID of the second comment which replies on the first.
	 */
	public Reply(final String replyTime, final int commentIdFirst, final int commentIdSecond)
	{
		this.replyTime = replyTime;
		this.commentIdFirst = commentIdFirst;
		this.commentIdSecond = commentIdSecond;
	}

	// Get methods

	/**
	 * Returns the replytime.
	 * @return replytime the timestamp of the reply comment.
	 */
	public String getReplyTime()
	{
		return replyTime;
	}

	/**
	 * Returns the first comment.
	 * @return first comment.
	 */
	public int getCommentIdFirst()
	{
		return commentIdFirst;
	}

	/**
	 * Returns the second comment.
	 * @return second comment which is the reply to the first comment.
	 */
	public int getCommentIdSecond()
	{
		return commentIdSecond;
	}

	///////////////

	// Set methods

	///////////////
}
