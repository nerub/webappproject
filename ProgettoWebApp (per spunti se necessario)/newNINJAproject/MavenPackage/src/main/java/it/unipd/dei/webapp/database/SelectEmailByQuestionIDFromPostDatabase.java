package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Searches for the competences.
 *
 * @version 1.00
 * @since 1.00
 */

public final class SelectEmailByQuestionIDFromPostDatabase
{
   /**
   * SQL statement that does the search.
   */
    private static final String STATEMENT = "SELECT * FROM IronJobs.post WHERE questionId = ?";

   /**
   * Connection to the database.
   */
    private final Connection con;

    /**
    * Id of the question for which to search the user
    */
    private final int questionid;

   /**
   * Creates a new object for searching the database for competences.
   * @param con The connection to the database.
   * @param questionid the id of the question.
   */
    public SelectEmailByQuestionIDFromPostDatabase(final Connection con, final int questionid)
    {
       this.con = con;
       this.questionid = questionid;
    }

   /**
   * Searches for the email of the user who posted the question.
   * @return A list of the competences found.
   * @throws SQLException if the search goes wrong.
   */
    public List<Post> getPost() throws SQLException
    {
       PreparedStatement pstmt = null;
       ResultSet rs = null;

       // the results of the search
       final List<Post> post = new ArrayList<>();

       try
       {
          pstmt = con.prepareStatement(STATEMENT);
          pstmt.setInt(1, questionid);
          rs = pstmt.executeQuery();

          while (rs.next())
          {
             post.add(new Post(rs.getString("email"), rs.getInt("questionId"), rs.getString("postTime")));
          }
       }

       finally
       {
          if (rs != null)
          {
             rs.close();
          }

          if (pstmt != null)
          {
             pstmt.close();
          }

          con.close();
       }

       return post;
    }
  }
