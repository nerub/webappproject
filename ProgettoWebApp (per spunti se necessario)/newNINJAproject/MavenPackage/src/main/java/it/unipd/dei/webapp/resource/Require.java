package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Require} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Require
{
	/**
	 * the competence name
 	 */
	private final String name;
	/**
	 * the interview id
	 */
	private final int interviewId;

	/**
	 * Creates a new requirement
	 *
	 * @param name the name of the competence required.
	 *
	 * @param interviewId the id of the interview.
	 */
	public Require(final String name, final int interviewId)
	{
		this.name = name;
		this.interviewId = interviewId;
	}

	// Get methods

	/**
	 * Returns the competence Name.
	 * @return competence name.
	 */
	public final String getName()
	{
		return name;
	}

	/**
	 * Returns the interview id.
 	 * @return interview id which require the competence.
	 */
	public final int getInterviewId()
	{
		return interviewId;
	}

	///////////////

	// Set methods

	///////////////
}
