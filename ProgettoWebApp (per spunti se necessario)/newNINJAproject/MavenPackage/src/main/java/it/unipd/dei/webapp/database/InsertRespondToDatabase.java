package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.RespondTo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Insert a new respondTo into the database.
 *
 * @version 1.00
 * @since 1.00
 */
public final class InsertRespondToDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "INSERT INTO IronJobs.RespondTo (solutionId, questionId, commentId) " +
											"VALUES (?, ?, ?) " +
											"RETURNING *";

  /**
  * Connection to the database.
  */
	private final Connection con;

  /**
  * Object respondTo which will be inserted into the database.
  */
	private final RespondTo respondTo;

  /**
  * Creates a new object for inserting.
  * @param con The connection to the database.
  * @param respondTo The new respondTo object to be inserted.
  */
	public InsertRespondToDatabase(final Connection con, final RespondTo respondTo)
	{
		this.con = con;
		this.respondTo = respondTo;
	}

  /**
  * Inserts the new respondTo object into the database.
  * @return The respondTo object inserted.
  * @throws SQLException if the insertion goes wrong.
  */
	public RespondTo InsertRespondTo() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		RespondTo e = null;

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

			pstmt.setInt(1, respondTo.getSolutionId());
            pstmt.setInt(2, respondTo.getQuestionId());
            pstmt.setInt(3, respondTo.getCommentId());

			rs = pstmt.executeQuery();

			if (rs.next()) {
				e = new RespondTo(rs.getInt("solutionId"),
						rs.getInt("questionId"),
						rs.getInt("CommentId")
				);
			}

		}

		finally
		{
			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}
		return e;
	}
}
