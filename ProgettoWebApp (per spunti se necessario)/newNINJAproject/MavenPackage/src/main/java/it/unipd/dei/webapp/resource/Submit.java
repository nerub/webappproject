package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Submit} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Submit
{
	/**
	 * the submit time
	 */
	private final String submitTime;
	/**
	 * the vatin
	 */
	private final String vatIn;
	/**
	 * the question id
	 */
	private final int questionId;

	/**
	 * Creates a new submit entry.
	 *
	 * @param submitTime timestamp of the submission.
	 *
	 * @param vatIn the id of the company.
	 *
	 * @param questionId the id of the question submitted.
	 */
	public Submit(final String submitTime, final String vatIn, final int questionId)
	{
		this.submitTime = submitTime;
		this.vatIn = vatIn;
		this.questionId = questionId;
	}

	// Get methods

	/**
	 * Returns the submit time.
	 * @return submittime of the question.
	 */
	public String getSubmitTime()
	{
		return submitTime;
	}

	/**
	 * Returns the vatin of the company who posted the question.
	 * @return vatin id of the company.
	 */
	public String getVatIn()
	{
		return vatIn;
	}

	/**
	 * Returns the question id.
	 * @return questionId of the question.
	 */
	public int getQuestionId()
	{
		return questionId;
	}

	///////////////

	// Set methods

	///////////////
}
