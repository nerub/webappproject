# EB: commenti vari:

## index.html
	Contiene la home page classica -> si può passare alle pagine di questions e interviews.
	DA VERIFICARE/FARE:	

* [ ] Bisogna controllare di essere loggati con un account prima di permettere lo spostastamento in altre pagine all'utente?
	
	--No.(Tra l'altro nella demo non dobbiamo fare un login, ma una registrazione)

* [ ] Facendo il login (fare il confronto con un account presente nel DB) dove si viene reindirizzati?
	
	--In teoria nella pagina "Account, in cui si ha un resoconto dei dati e due link alle myQuestions, mySolutions, che mandano alla pagina question, dove vengono filtrate le domande e le soluzioni per email dell'utente"
					
## questions.html
	deve contenere la tabella con le domande che bisogna tirar giù dal DB
	DA VERIFICARE/FARE:	
* [ ] la barra di ricerca deve filtrare dei valori

	--io non l'avrei neanche messa. (che query usiamo, abbiamo solo serach question by id, or by tag)

* [ ]  bisogna filtrare per categoria (con le checkbox come nel mockup?)
						
	--deve esserci affianco una colonna con le tag cliccabili che filtrano le question per tag.

## interviews.html
	deve contenere le proposte di colloquio (al momento bianca)

	--Questa puo' anche rimanere bianca perchè non fa parte della demo e non abbiamo il lato servert capace di gestirla
	
## registration.html
	contiene i campi principali per la registrazione di un utente; se viene completato del tutto un form e si invia allora si viene reindirizzati alla pagina skills.html dove si può completare con un ulteriori dettagli il profilo; se non si completano i campi il browser avvisa che ci sono delle caselle non compilate.
	DA VERIFICARE/FARE:	
* [ ] quando si submittano i dati bisogna fare la query per inserirli nel DB o "tenere i dati in pancia" per fare poi il submit finale
quando si completa il profilo con le skills e i titoli di studio
	
	--i le credenziali dello user sono da inserire subito. 

## skills.html
	pagina dove si possono inserire ulteriori info al profilo utente
	DA VERIFICARE/FARE:	
* [ ] completare il submit dei dati nel DB e reindirizzamento -> dove?

	--i dati non vanno subito nel db. vengono passati alla pagina "evaluationSkills" in cui l'utente attribuisce un rating ad ogni skill. Alla fine di quella pagina vengono messi dentro al db.

	--PROBLEMA: se facciamo in modo che un utente inserisca una sua skill(nuova che non c'è nel db) dobbiamo gestire il caso in cui venga messa nel db una competence che già esiste. Quindi secondo me è il caso di lasciare che l'utente inserisca anche cose nuove, ma prima di poterlo fare deve vedere un muro con dei bottonciniper le skills già presenti nel db, che puo' selezionare.
	
## js/bootstrap.min.js
	aggiunti metodi per creazione di nuove righe contenenti titoli di studio, skill + ratings -> controllare commenti sul file per verificare cosa fanno

## css/bootstrap-grid.min.css
	contiene gli stili utilizzati; alcuni sono modificati alla fine quindi fare attenzione
	
