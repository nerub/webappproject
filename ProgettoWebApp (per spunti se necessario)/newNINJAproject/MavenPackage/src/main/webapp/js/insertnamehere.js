


/* Redirect to skills page after registering user details */

function toSkills() {
	/* Add to sessionStorage user's parameters */
	var user = document.getElementById("username").value;
	var email = document.getElementById("email").value;
	
	/* Diagnostics */
	console.log("Dati: "+ user +" "+email);
	
	sessionStorage.setItem("username", user);
	sessionStorage.setItem("email", email);

	/* Diagnostics */
	console.log("Stored :" +sessionStorage.getItem("username")+" , "+ sessionStorage.getItem("email"));
	
	/* Store user details */
	insertUser();

	return false;
}

/* Store user details */
function insertUser() {
	var userurl = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/user";

	/* Detect the index of the checked avatar */
	var avatars = document.getElementsByName('avatar');
	var selectedavatar = 0;
	for(var i = 0; i < avatars.length; i++)
	{
		if(avatars[i].checked)
			selectedavatar = i;
	}

	var user = [{username : document.getElementById('username').value,
		pswd : document.getElementById('password').value,
		birthDate: document.getElementById('birthdate').value,
		points: 0,
		email: document.getElementById('email').value,
		avatar : selectedavatar,
		type : 0,
		currentJob: document.getElementById('currentjob').value,
		details: document.getElementById('details').value,
		degree: document.getElementById('degree').value,
		registrationTime: new Date().toUTCString()
	}];

	$.ajax({
		method: "POST",
		url: userurl,
		data: JSON.stringify({user : user}),
		contentType: 'application/json',
		error: function( status ) {
		  alert( "Status: " + status.responseText);
		  console.log(status.responseText);
		}
	}).done(function(){
		 // Now we reload the page to show the new comment
		 location.replace("skills.html");
		});
}


function submitSoftCompetences()
{
	var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/competence";
	var email = sessionStorage.getItem("email");
	if (email == undefined)
	{
		alert("You need to register first!");
		return;
	}

	var strToCompare = "SoftInput";
	var tmpInputs = document.getElementsByTagName("input");
	var softInputs = [];
	softIndex = 0;
	for (var index = 0; index < tmpInputs.length; index++)
	{
		if (tmpInputs[index].id.toLowerCase().indexOf(strToCompare.toLowerCase()) != -1)
		{
			softInputs[softIndex] = tmpInputs[index].value;
			softIndex++;
		}
	}

	for (var i = 0; i < softInputs.length; i++)
	{
		var skillName = softInputs[i];
		var competence = [{name : skillName}];
		
		$.ajax({
			method: "POST",
			url: url,
			data: JSON.stringify({competence : competence}),
			contentType: 'application/json',
			error: function( status ) {
				alert("Status: " + status.responseText);
				console.log(status.responseText);
			}
		});	
	}
}

/* Save the skills chosen by the user */
function submitSoftSkills() {
	var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/possess";
	var email = sessionStorage.getItem("email");
	if (email == undefined)
	{
		alert("You need to register first!");
		return;
	}

	/* Submit Soft Skills */
	var tableSoft = document.getElementById("tblSoft");
	for (var i = 0, row; row = tableSoft.rows[i]; i++) {
		/* Look in the first cell for the skill name */
		var skillName = row.cells[0].innerHTML;
		
		var rating = 1;
		for(var j = 1; j <= 10; j++)
		{
			var row = i+1;
			var col = j;
			var softradioXY = "SoftRadio" + row + "Id" + col;
			var radiobutton = document.getElementById(softradioXY);
			
			if(radiobutton.checked)
				rating = j;
		}

		var possess = [{email : email,
			name : skillName,
			rating : rating
		}];
		
		$.ajax({
			method: "POST",
			url: url,
			data: JSON.stringify({possess : possess}),
			contentType: 'application/json',
			error: function( status ) {
				alert("Status: " + status.responseText);
				console.log(status.responseText);
			}
		});		
	}
}

function submitHardCompetences()
{
	var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/competence";
	var email = sessionStorage.getItem("email");
	if (email == undefined)
	{
		alert("You need to register first!");
		return;
	}

	var strToCompare = "HardInput";
	var tmpInputs = document.getElementsByTagName("input");
	var softInputs = [];
	softIndex = 0;
	for (var index = 0; index < tmpInputs.length; index++)
	{
		if (tmpInputs[index].id.toLowerCase().indexOf(strToCompare.toLowerCase()) != -1)
		{
			softInputs[softIndex] = tmpInputs[index].value;
			softIndex++;
		}
	}

	for (var i = 0; i < softInputs.length; i++)
	{
		var skillName = softInputs[i];
		var competence = [{name : skillName}];
		
		$.ajax({
			method: "POST",
			url: url,
			data: JSON.stringify({competence : competence}),
			contentType: 'application/json',
			error: function( status ) {
				alert("Status: " + status.responseText);
				console.log(status.responseText);
			}
		});	
	}
}

/* Submit Hard Skills */
function submitHardSkills() {
	var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/possess";
	var email = sessionStorage.getItem("email");
	if (email == undefined)
	{
		alert("You need to register first!");
		return;
	}

	var tableHard = document.getElementById("tblHard");
	for (var i = 0, row; row = tableHard.rows[i]; i++) {
		/* Look in the first cell for the skill name */
		var skillName = row.cells[0].innerHTML;
		
		var rating = 1;
		for(var j = 1; j <= 10; j++)
		{
			var row = i+1;
			var col = j;
			var hardradioXY = "HardRadio" + row + "Id" + col;
			var radiobutton = document.getElementById(hardradioXY);
			
			if(radiobutton.checked)
				rating = j;
		}

		var possess = [{
			email : email,
			name : skillName,
			rating : rating
		}];

		$.ajax({
			method: "POST",
			url: url,
			data: JSON.stringify({possess : possess}),
			contentType: 'application/json',
			error: function( status ) {
				alert("Status: " + status.responseText);
				console.log(status.responseText);
			}
		});		
	}
}

/* Navigate through tabs */
function openPage(evt, pageName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(pageName).style.display = "block";
	evt.currentTarget.className += " active";
	
}



/* Add qualification to the user's qualifications table */
function addQualification()
{
	var table = document.getElementById("tblQualification");
	var newRow = table.insertRow(table.rows.length);
	var newCell  = newRow.insertCell(0);

	var newQualification  = document.createElement("input");
	newQualification.setAttribute("type", "text");
	newQualification.className = "form-control-plaintext";

	/* Qualifications will have as id a name like: Qualification1, Qualification2, ecc.. */
	var qualificationId = "Qualification" + table.rows.length;

	newCell.appendChild(newQualification);
}

/* Remove qualification from the user's qualifications table */
function removeQualification()
{
	var table = document.getElementById("tblQualification");
	table.deleteRow(-1);
}

/* Add soft skill to the user's softskills table */
function addSoftSkill()
{
	var table = document.getElementById("tblSoftSkills");
	var newRow = table.insertRow(table.rows.length);
	var newCell  = newRow.insertCell(0);

	var newSkill  = document.createElement("input");
	newSkill.setAttribute("type", "text");

	/* Soft skills will have as id a name like: SoftInput1, SoftInput2, ecc.. */
	var inputId = "SoftInput" + table.rows.length;

	newSkill.setAttribute("id", inputId)
	newSkill.style.width = "100%";
	newSkill.className = "form-control-plaintext";
	newCell.appendChild(newSkill);
}

/* Store new typed skills */
$(document).ready(function()
{
    document.getElementById("next-power").addEventListener("click", function(){
        var tableSoft = document.getElementById("tblSoftSkills");
        var numRowsSoft = tableSoft.rows.length;
        for(var i = 1; i <= numRowsSoft; i++)
        {
            var valSoft = document.getElementById("SoftInput" + i).value;
            var finalTableSoft = document.getElementById("tblSoft");
            var newRowSoft = finalTableSoft.insertRow(finalTableSoft.rows.length);
            var firstCellSoft  = newRowSoft.insertCell(0);
            firstCellSoft.innerHTML = valSoft;
            var secondCellSoft = newRowSoft.insertCell(1);
            var newRating  = createRatingDiv("Soft", finalTableSoft.rows.length);
            secondCellSoft.appendChild(newRating);
        }
        
        var tableHard = document.getElementById("tblHardSkills");
        var numRowsHard = tableHard.rows.length;
        for(var j = 1; j <= numRowsHard; j++)
        {
            var valHard = document.getElementById("HardInput" + j).value;
            var finalTableHard = document.getElementById("tblHard");
            var newRowHard = finalTableHard.insertRow(finalTableHard.rows.length);
            var firstCellHard = newRowHard.insertCell(0);
            firstCellHard.innerHTML = valHard;
            var secondCellHard = newRowHard.insertCell(1);
            var newRating  = createRatingDiv("Hard", finalTableHard.rows.length);
            secondCellHard.appendChild(newRating);
        }
    });
});

/*  */
function Soft(id)
{
	var table = document.getElementById("tblSoft");
	var skillName = id.innerHTML;
	var count = table.rows.length;
	for (var i = 0, row; row = table.rows[i]; i++) {
		var col = row.cells[0];
		if(col.innerHTML === skillName)
			table.deleteRow(i);
	}

	if(table.rows.length === count) // No deletions
	{
		var newRow = table.insertRow(table.rows.length);
		var firstCell  = newRow.insertCell(0);
		firstCell.innerHTML = skillName;
		var secondCell = newRow.insertCell(1);
		var newRating  = createRatingDiv("Soft", table.rows.length);
		secondCell.appendChild(newRating);
	}
}

/* Remove soft skill from the user's softskills table */
function removeSoftSkill()
{
	var table = document.getElementById("tblSoftSkills");
	table.deleteRow(-1);
}

/* Add hard skill to the user's hardskills table */
function addHardSkill()
{
	var table = document.getElementById("tblHardSkills");
	var newRow = table.insertRow(table.rows.length);
	var firstCell  = newRow.insertCell(0);
	var newSkill  = document.createElement("input");
	var inputId = "HardInput" + table.rows.length;	// Remark: parto da 1 con gli indici
	/* EB: la nuova hardskill avrà come "id" un nome tipo HardInput1, HardInput2, ecc..*/
	newSkill.setAttribute("id", inputId)
	newSkill.setAttribute("type", "text");
	newSkill.style.width = "100%";
	newSkill.className = "form-control-plaintext";
	firstCell.appendChild(newSkill);
}

/*  */
function Hard(id)
{
	var table = document.getElementById("tblHard");
	var skillName = id.innerHTML;
	var count = table.rows.length;
	for (var i = 0, row; row = table.rows[i]; i++) {
		var col = row.cells[0];
		if(col.innerHTML === skillName)
			table.deleteRow(i);
	}

	if(table.rows.length === count) // No deletions
	{
		var newRow = table.insertRow(table.rows.length);
		var firstCell  = newRow.insertCell(0);
		firstCell.innerHTML = skillName;
		var secondCell = newRow.insertCell(1);
		var newRating  = createRatingDiv("Hard", table.rows.length);
		secondCell.appendChild(newRating);
	}
}

/* Remove hard skill from the user's hardskills table */
function removeHardSkill()
{
	var table = document.getElementById("tblHardSkills");
	table.deleteRow(-1);
}


/*  */
function removeAllCompetences()
{
	var table = document.getElementById("tblQualification");
	while (table.rows.length != 0)
		removeQualification();

	table = document.getElementById("tblSoftSkills");
	while (table.rows.length != 0)
		removeSoftSkill();

	table = document.getElementById("tblHardSkills");
	while (table.rows.length != 0)
		removeHardSkill();
}

/* The groupName will be Soft or Hard */
/* The groupNumber will be the number of the row */
/* groupName and groupNumber are used to identify different groups of radiobutton */
function createRatingDiv(groupName, groupNumber)
{
	/* Create a div which contains a fieldset holding all the radiobuttons */
	var ratingDiv = document.createElement("div");
	var fieldset = document.createElement("fieldset");
	fieldset.setAttribute("class", "ratingFieldset");

	/* fieldsetId used with the attribute id will be of the type SoftFieldset1, SoftFieldset2, ecc.. */
	fieldsetId = groupName + "Fieldset" + groupNumber;
	fieldset.setAttribute("id", fieldsetId);
	ratingDiv.appendChild(fieldset);

	/* Add a radiobutton until 10 */
	for (i = 1; i <= 10; i++)
	{
		/* radioGroupName used in the attribute "name" will be of the type SoftRadio1, SoftRadio2, ecc..
		NB. all attributes "name" of the radiobuttons belonging to the same radio button group must have the same
		value in order to have only one active (by selecting one, the others can't be checked as selected) */

		var radioGroupName = groupName + "Radio" + groupNumber;
		var radioBtn = createSingleRatingPoint(i, radioGroupName);
		if (i == 1)
		{
			radioBtn.checked = true;
		}

		fieldset.appendChild(radioBtn);
	}

	return ratingDiv;
}

/*  */
function createSingleRatingPoint(value, radiogroupName)
{
	var radioBtn = document.createElement("input");
	radioBtn.setAttribute("type", "radio");

	/*  The new radio button will have as id a name like SoftRadioXIdY, where:
		X = number of the radio button group (the row);
		Y = number of the radio button in the group (from 1 to 10) */
	var id = radiogroupName + "Id" + value;
	radioBtn.setAttribute("id", id);

	/* The radioGroupName used in the attribute "name" will be of the type SoftRadio1 */
	radioBtn.setAttribute("name", radiogroupName);
	radioBtn.setAttribute("value", value);
	radioBtn.checked = false;
	return radioBtn;
}

/* Add a new class to a HTML element by its id */
function addClassToElement(elementIdName, className)
{
	var element = document.getElementById(elementIdName);
	element.classList.add(className);
}

/* Remove a class from a HTML element by its id */
function removeClassToElement(elementIdName, className)
{
	var element = document.getElementById(elementIdName);
	element.classList.remove(className);
}

/* Handle the registration in skills.html, by sending a GET to questions.jsp to load and pass the parameters for the sessionStorage */
function passEmailUsername()
{
	var tmpUser = sessionStorage.getItem("username");
	if (tmpUser != null)
		window.location.href = "../jsp/questions.jsp?username=" + tmpUser;
	else
		window.location.href = "../jsp/questions.jsp";
}

/* Get the user avatar based on the integer passed */
function getAvatar (int)
{
   switch (int) {
      case 0:
         return 'administrator.png';
         break;

      case 1:
         return 'anonymous.png';
         break;

      case 2:
         return 'avatar.png';
         break;

      case 3:
         return 'charlie.png';
         break;

      case 4:
         return 'female.png';
         break;

      case 5:
         return 'male.png';
         break;

      case 6:
         return 'grandfather.png';
         break;

      case 7:
         return 'grandmother.png';
         break;

      case 8:
         return 'ninja.png';
         break;

      case 9:
         return 'mario.png';
         break;

      case 10:
         return 'rick.png';
         break;

      case 11:
         return 'thorn.png';
         break;

      case 12:
         return 'doctor.png';
         break;
         
      default:
         return 'avatar.png';
   }
}
