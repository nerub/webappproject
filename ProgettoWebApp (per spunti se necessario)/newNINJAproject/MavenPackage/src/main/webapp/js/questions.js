// Self-invoking function that fill the questions table and the categories section by querying the database
(function() {

    //QUESTION QUERY
    var httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHttpRequest instance');
        return false;
    }
	var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/select-questions";
    httpRequest.onreadystatechange = writeQuestionResults;
	httpRequest.open('POST', url);
    httpRequest.send();

    //TAG QUERY
    var httpRequest1 = new XMLHttpRequest();
    if (!httpRequest1) {
        alert('Giving up :( Cannot create an XMLHttpRequest instance');
        return false;
    }
    var url1 = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/select-tag";
    httpRequest1.onreadystatechange = writeTagResults;
    httpRequest1.open('POST', url1);
    httpRequest1.send();

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    //functions

    /**
     * function to print tags
     */
    function writeTagResults()
    {
        if (httpRequest1.readyState === XMLHttpRequest.DONE)
        {
            // get the div where to write results
            var div = document.getElementById('list-results');

            // clean it up
            div.className = '';
            while (div.firstChild)
            {
                div.removeChild(div.firstChild);
            }

            // generic DOM elements
            var e;
            var ee;

            // if we get a 200 status write result table parsing it from JSON
            if (httpRequest1.status == 200)
            {
                // a generic row and column of the table
                var subDiv;
                var label;
                var input;

                // the JSON list of questions
                var jsonData = JSON.parse(httpRequest1.responseText);
                jsonData = jsonData['resource-list'];

                var tag;

                //creation of the table
                e = document.createElement('form');
                e.id='tag-form';
                div.appendChild(e);

                for (var i = 0; i < jsonData.length; i++)
                {
                    tag = jsonData[i].tag;

                    //contenitore di ogni tag
                    subDiv = document.createElement('div');
                    subDiv.className='form-check';
                    e.appendChild(subDiv);
                    //Ogni quadratino
                    input= document.createElement('input');
                    input.className='form-check-input';
                    input.type='checkbox';
                    input.name='tagCheck';
                    input.id="check"+i.toString();
                    input.value=tag['name'];
                    subDiv.appendChild(input);
                    //Ogni descrizione di quadratino
                    label = document.createElement('label');
                    label.className='form-check-label';
                    label.appendChild(document.createTextNode(tag['name']));
                    subDiv.appendChild(label);
                    // EB: aggiunge riga vuota alla fine delle categorie
                    /*if (i == jsonData.length - 1)
                    {
                        var blankLine = document.createElement("br");
                        subDiv.appendChild(br);
                    }*/
                }
                

            }

            else
            {
                div.className = 'alert alert-danger';

                e = document.createElement('p');
                e.appendChild(document.createTextNode('Unexpected error'));
                div.appendChild(e);
            }
        }
    }

    /**
     * Function to print questions
     */
    function writeQuestionResults()
    {
        if (httpRequest.readyState === XMLHttpRequest.DONE)
        {
            // get the div where to write results
            var div = document.getElementById('table-results');

            // clean it up
            div.className = '';
            while (div.firstChild)
            {
                div.removeChild(div.firstChild);
            }

            // generic DOM elements
            var e;
            var ee;

            // if we get a 200 status write result table parsing it from JSON
            if (httpRequest.status == 200)
            {
                // a generic row and column of the table
                var tr;
                var td;

                // the JSON list of questions
                var jsonData = JSON.parse(httpRequest.responseText);
                jsonData = jsonData['resource-list'];

                var question;

                //creation of the table
                e = document.createElement('table');
                e.className = 'table';
                e.id='table';
                div.appendChild(e);

                //creation of the head table
                ee = document.createElement('thead');
                ee.className = 'thead-light';
                e.appendChild(ee);

                tr = document.createElement('tr');
                ee.appendChild(tr);

                td = document.createElement('th');
                td.appendChild(document.createTextNode('#'));
                tr.appendChild(td);

                td = document.createElement('th');
                td.appendChild(document.createTextNode('Title'));
                tr.appendChild(td);

                //faccio diventare questo elemento cliccabile per ordinare le domande per views
                td = document.createElement('th');
                td.setAttribute("id", "views");
                td.innerHTML = "Views";
                td.classList.add("clickable");
                tr.appendChild(td);

                td = document.createElement('th');
                td.appendChild(document.createTextNode('Upvotes'));
                tr.appendChild(td);

                td = document.createElement('th');
                td.appendChild(document.createTextNode('DownVotes'));
                tr.appendChild(td);

                td = document.createElement('th');
                td.appendChild(document.createTextNode('Difficulty'));
                tr.appendChild(td);

                //creation of the dynamic table body
                ee = document.createElement('tbody');
                e.appendChild(ee);

                for (var i = 0; i < jsonData.length; i++)
                {
                    question = jsonData[i].question;

                    tr = document.createElement('tr');
                    ee.appendChild(tr);

                    td = document.createElement('td');
                    td.appendChild(document.createTextNode(i+1));
                    tr.appendChild(td);

                    //titolo diventa link. bisogna aggiungere un link diverso per ogni riga: 
                    //o mettiamo il valore questionId nel link
                    //o creiamo un form nascosto che passi il valore
                    td = document.createElement('td');
                    var a = document.createElement('a');
                    a.id = "link"+question['questionId'];
                    console.log(a);
                    a.onclick =  function(){
                        var solid = this.id;
                        solid = solid.substring(solid.lastIndexOf('link') + 4);
                        sessionStorage.setItem("questionId", solid);
                        console.log(solid);
                    };
                    a.href="http://localhost:8080/IronJobs-1.0-SNAPSHOT/jsp/question.jsp";
                    a.appendChild(document.createTextNode(question['title']));
                    td.appendChild(a);
                    tr.appendChild(td);

                    td = document.createElement('td');
                    td.appendChild(document.createTextNode(question['views']));
                    tr.appendChild(td);

                    //green upvotes
                    td = document.createElement('td');
                    var s = document.createElement('span');
                    s.className='green-upvotes';
                    var t = document.createTextNode(question['upvote']);
                    s.appendChild(t);
                    td.appendChild(s);
                    tr.appendChild(td);

                    //red downvotes
                    td = document.createElement('td');
                    var s = document.createElement('span');
                    s.className='red-downvotes';
                    var t = document.createTextNode(question['downvote']);
                    s.appendChild(t);
                    td.appendChild(s);
                    tr.appendChild(td);

                    //stelline per difficoltà
                    td = document.createElement('td');
                    var diff = question['difficulty'];
                    var diffStar = "";
                    for(var t=0; t < diff; t++){
                        diffStar = diffStar+"★ ";/*'&#9733;'*/
                    }
                    td.appendChild(document.createTextNode(diffStar));
                    tr.appendChild(td);
                }
            }

            else
            {
                div.className = 'alert alert-danger';

                e = document.createElement('p');
                e.appendChild(document.createTextNode('Unexpected error'));
                div.appendChild(e);
            }

            //adding event listener to the views button. Done now, because button is dynamically generated, and the function .addeventlister would find a null pointer
            document.getElementById("views").addEventListener("click", function()
            {
                //se l'array non è già stato ordinato o se lo è in senso crescente, ordina in senso decrescente
                if (views.innerHTML != "Views ▲"/*'&#x25BC;'*/)
                {
                    changeViewsHeader();
                    var table, rows, switching, i, x, y, shouldSwitch;
                    table = document.getElementById("table");
                    switching = true;
                    //Make a loop that will continue until
                    //no switching has been done:
                    while (switching)
                    {
                        //start by saying: no switching is done:
                        switching = false;
                        rows = table.getElementsByTagName("TR");
                        //Loop through all table rows (except the
                        //first, which contains table headers):
                        for (i = 1; i < (rows.length - 1); i++)
                        {
                            //start by saying there should be no switching:
                            shouldSwitch = false;
                            //Get the two elements you want to compare,
                            //one from current row and one from the next:
                            x = rows[i].getElementsByTagName("TD")[2];
                            y = rows[i + 1].getElementsByTagName("TD")[2];
                            //check if the two rows should switch place:
                            if (Number(x.innerHTML) < Number(y.innerHTML))
                            {
                                //if so, mark as a switch and break the loop:
                                shouldSwitch = true;
                                break;
                            }
                        }
                       
                        if (shouldSwitch)
                        {
                            //If a switch has been marked, make the switch
                           //and mark that a switch has been done:
                            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                            switching = true;
                        }
                    }
                }

                else
                {
                    changeViewsHeader();
                    var table, rows, switching, i, x, y, shouldSwitch;
                    table = document.getElementById("table");
                    switching = true;
                    //Make a loop that will continue until
                    //no switching has been done:
                    while (switching)
                    {
                        //start by saying: no switching is done:
                        switching = false;
                        rows = table.getElementsByTagName("TR");
                        //Loop through all table rows (except the
                        //first, which contains table headers):
                        for (i = 1; i < (rows.length - 1); i++)
                        {
                            //start by saying there should be no switching:
                            shouldSwitch = false;
                            //Get the two elements you want to compare,
                            //one from current row and one from the next:
                            x = rows[i].getElementsByTagName("TD")[2];
                            y = rows[i + 1].getElementsByTagName("TD")[2];
                            //check if the two rows should switch place:
                            if (Number(x.innerHTML) > Number(y.innerHTML)) {
                                //if so, mark as a switch and break the loop:
                                shouldSwitch = true;
                                break;
                            }
                        }
                        
                        if (shouldSwitch)
                        {
                            //If a switch has been marked, make the switch
                           //and mark that a switch has been done:
                            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                            switching = true;
                        }
                    }
                }

            });
        }
    }  
})();


/***************** EB: funzione per filtraggio domande in base ai tag scelti *****************/
/***************** Filtra mostrando solo le domande che contengono i tag selezionati dall'utente *****************/
$(document).ready(function()
{
	$("#button-search-by-tag").click(function()
	{
    	var inputs = document.getElementById('tag-form').getElementsByTagName("input");
    	var checkedTagsCnt = 0;

    	/* EB: controlli quali titoli sono presenti nella tabella */
    	var table = document.getElementById("table-results");
    	rows = table.getElementsByTagName("TR");

	    var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/select-related-join-questionid-by-multi-tag";

		var tagsSelected = [];
    	for (var i = 0; i < inputs.length; i++)
    	{
    		// EB: salvo in un array tutti i nomi dei tag selezionati
    		if (inputs[i].checked)
    		{
    			tagsSelected[checkedTagsCnt] = inputs[i].value;
    			checkedTagsCnt++;		    
    		}
    	}

    	var checkedTagstext = "N° of tags checked: " + checkedTagsCnt;
    	console.log(checkedTagstext);

    	// EB: se non ho selezionato alcun tag, allora mostro tutte le domande e esco dalla funzione
    	if (checkedTagsCnt == 0)
    	{
    		for (var rr = 1; rr < rows.length; rr++)
    		{
    			rows[rr].style.display = "";
    		}

    		return;
    	}

    	console.log("Inizio richiesta http");

    	var req = new XMLHttpRequest();
    
	    if (!req)
	    {
	        alert('Giving up :( Cannot create an XMLHttpRequest instance');
	        return false;
	    }

	    req.onreadystatechange = selectQuestionTitlesByMultiTag;
	    req.open('POST', url);

	    var data = "";
	    for (var c = 0; c < tagsSelected.length; c++)
	    {
	    	data += tagsSelected[c];
	    	if (c != tagsSelected.length -1)
	    	{
	    		data += ",";
	    	}
	    }

	    var textlink = ""; 
	    for (var tr = 1; tr < rows.length; tr++)
	    {
	    	var id = "link" + tr;
	    	textlink = document.getElementById(id).innerHTML;
	    	console.log(textlink);
	    }

		console.log(data);
	    req.send(data);
	    var text = "lo status è: " + req.status;
	    var statusText = "lo statustext è: " + req.statusText;

    	function selectQuestionTitlesByMultiTag()
		{
			console.log("Sono nella funzione selectQuestionTitlesByMultiTag");
	    	if (req.readyState === XMLHttpRequest.DONE)
	    	{
		    	if (req.status == 200 || req.status == 0)
	            {
		    		var jsonData = JSON.parse(req.responseText);
		    		jsonData = jsonData["resource-list"];
		    		var question;
		    		var questionTitlesArrayFromTag = [];

		    		console.log(jsonData);

		    		// EB: ottengo i titoli delle domande associati al tagname
		    		for (var j = 0; j < jsonData.length; j++)
		    		{
		    			question = jsonData[j].question;
		    			questionTitlesArrayFromTag[j] = question["title"];
		    		}

		    		for (var row = 1; row < rows.length; row++)
					{
						rows[row].style.display = "none";

						for (var t = 0; t < questionTitlesArrayFromTag.length; t++)
						{
							var linkId = "link" + row;

							if (document.getElementById(linkId).innerHTML == questionTitlesArrayFromTag[t])
							{
								rows[row].style.display = "";
							}
						}
					}
				}
	    	}
	    }
	});
});
/******************************* Fine funzione per filtraggio tramite tag selezionati ******************************************************/

function changeViewsHeader()
{
    var views = document.getElementById("views");
    
    if (views.innerHTML == "Views")
    {
        views.innerHTML = "Views ▲"/*'&#x25BC;'*/; // triangolo verso il basso
        //sort asc values
    }

    else if (views.innerHTML == "Views ▲"/*'&#x25BC;'*/) // triangolo verso il basso
    {
        views.innerHTML = "Views ▼"/*'&#x25B2;'*/; // triangolo verso l'alto
        //sort desc values
    }

    else if (views.innerHTML == "Views ▼" /*'&#x25B2;'*/) // triangolo verso l'alto
    {
        views.innerHTML = "Views ▲"/*'&#x25BC;'*/; // triangolo verso il basso
        //sort asc values
    }
}

/********* EB: functions for the search bar ********/

//Enter key trigger on search bar
$(document).ready(function()
{
    window.addEventListener("keyup", function(event)
    {
        if(event.keyCode === 13)
        {
            $("#btnSearch").click();
        }
    });
});


//click event on search button
$(document).ready(function()
{
    $("#btnSearch").click(function()
    {
        var text = document.getElementById("textSearch").value;
        document.getElementById("textSearch").value = "";
        document.getElementById("textSearch").placeholder = text;

        var table = document.getElementById("table");
        rows = table.getElementsByTagName("TR");
        
        if (text == "")
        {
            // EB: parto da 1 perchè la prima riga è l'header
            for (i = 1; i < rows.length; i++)
            {
                rows[i].style.display = "";
            }
        }

        else
        {
            var toLowerText = text.toLowerCase();
            // EB: parto da 1 perchè la prima riga è l'header
            for (i = 1; i < rows.length; i++)
            {
                if(rows[i].getElementsByTagName("TD")[1].innerHTML.toLowerCase().indexOf(toLowerText) != -1)
                {
                    rows[i].style.display = "";
                }

                else
                {
                     rows[i].style.display = "none";
                }       
            }
        }
    });
});


$(document).ready(function()
{
    $("#textSearch").click(function()
    {
        var text = document.getElementById("textSearch").value;
        document.getElementById("textSearch").value = "";
        document.getElementById("textSearch").placeholder = "";
    });

});
/******* functions for the search bar end ************/