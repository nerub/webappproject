/* Loads the contents of the page */
function loadSolutionWithComments()
{
   // Checks if the user il logged in
   var username = sessionStorage.getItem('username');

   if(!(username == undefined))
   {
      var div = document.getElementById("topnav-right");
      while (div.firstChild)
      {
         div.removeChild(div.firstChild);
      }
      var div = document.createElement('div');
      var button = document.createElement('button');
      button.className = 'login_button';
      button.type = 'button';
      button.setAttribute("onclick", "window.location.href='account.jsp?username=" + username + "'");
      button.append(document.createTextNode(username));

      div.append(button);
      $('#topnav-right').append(div);
   }
   // Query the solution part first
   var httpRequest = new XMLHttpRequest();
   if (!httpRequest)
   {
      alert('Giving up :( Cannot create an XMLHttpRequest instance');
      return false;
   }

   var questionId = sessionStorage.getItem('questionId');

   var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/question/" + questionId;
   httpRequest.onreadystatechange = writeQuestionTitle;
   httpRequest.open('GET', url);
   httpRequest.send();

   function writeQuestionTitle()
   {
      // Get complete
      if (httpRequest.readyState === XMLHttpRequest.DONE)
      {
         // DOM elements
         var e;
         var ee;

         // 500 status is an error
         if (httpRequest.status == 500)
         {

            // Gets the div that contains the question body
            var div = document.getElementById("solutionContent");

            // clean the div
            div.className = '';
            while (div.firstChild)
            {
               div.removeChild(div.firstChild);
            }

            // Let's incorporate the error message and then print it
            var jsonData = JSON.parse(httpRequest.responseText);
            jsonData = jsonData['message'];

            e = document.createElement('ul');
            div.appendChild(e);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Message: ' + jsonData['message']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Error code: ' + jsonData['error-code']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Details: ' + jsonData['error-details']));
            e.appendChild(ee);

         }
         else if (httpRequest.status == 200)
         {
            var jsonData = JSON.parse(httpRequest.responseText);
            jsonData = jsonData['resource-list'];

            var question = jsonData[0].question;

            $("#questionTitleSolution").append("Solution to Question: ");
            $("#questionTitleSolution").append(question['title']);

         }
         else
         {
            // Gets the div that contains the question body
            var div = document.getElementById("solutionContent");

            // clean the div
            div.className = '';
            while (div.firstChild)
            {
               div.removeChild(div.firstChild);
            }

            div.className = 'alert alert-danger';

            e = document.createElement('p');
            e.appendChild(document.createTextNode('Unexpected error...'));
            div.appendChild(e);
         }
      }
   }

   var solutionid = sessionStorage.getItem('solutionid');
   console.log("Solutionid read: " + solutionid);

   loadSolution(solutionid);
   loadCommentsSolution(solutionid);
}

/* Loads and displays the solution */
function loadSolution(solutionid)
{
   // Query the solution part first
   var httpRequest = new XMLHttpRequest();
   if (!httpRequest)
   {
      alert('Giving up :( Cannot create an XMLHttpRequest instance');
      return false;
   }

   var questionId = sessionStorage.getItem('questionId');
   var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/solution/questionId/" + questionId;
   httpRequest.onreadystatechange = writeSolutionDetails;
   httpRequest.open('GET', url);
   httpRequest.send();

   function writeSolutionDetails()
   {
      // Get complete
      if (httpRequest.readyState === XMLHttpRequest.DONE)
      {
         if (httpRequest.status == 200)
         {
            //console.log("Code 200");
            var jsonData = JSON.parse(httpRequest.responseText);
            jsonData = jsonData['resource-list'];
            var solution;

            for(var i = 0; i < jsonData.length; i++)
            {
               var temp = jsonData[i].solution;
               //console.log( temp['solutionId'] +'==' +solutionid);
               if (temp['solutionId'] == solutionid)
               {
                  solution = temp;
                  break;
               }
            }
            // check if found a solution
            if(solution == undefined)
            {
               alert("Unexpected error: solution not found");
               return;
            }

            // now we've got the solution, print it

            // check if it is best, if yes then star it
            if (solution.isBest)
            {
               console.log(solution.isBest);
               $("#questionTitleSolution").prepend("<i class='fas fa-star fa-sm' title='Best Solution'></i>");
            }

            // Display also username and avatar of the user who posted the solution

            var getUser = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/user/" + solution['email'];

            $.getJSON(getUser, function(data){
               var user = data['resource-list'][0].user;
               $('#username').append(user.username);
               var p = document.createElement('p');
               p.append(document.createTextNode(' ' + (solution.upvote - solution.downvote) + ' '));
               p.id = 'solutionVotes';
               $('#username').append(p);

               $('#solutionVotes').prepend("<i class='fas fa-arrow-circle-up'> </i>");
               $('#solutionVotes').append("<i class='fas fa-arrow-circle-down'> </i>");

               var avatarSrc = "<img title='Avatar of the user' src='../resource/avatar/" + getAvatar(user.avatar) + "'>";
               $("#avatar").append(avatarSrc);
            });


            $("#solutionTextSolution").append(solution['solutionText']);
         }
         // 500 status is an error
         else if (httpRequest.status == 500)
         {
            console.log("Error 500");
            div = document.getElementById(solutionBody);

            // clean the div
            div.className = '';
            while (div.firstChild)
            {
               div.removeChild(div.firstChild);
            }

            // Let's incorporate the error message and then print it
            var jsonData = JSON.parse(httpRequest.responseText);
            jsonData = jsonData['message'];
            var e;
            var ee;
            e = document.createElement('ul');
            div.appendChild(e);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Message: ' + jsonData['message']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Error code: ' + jsonData['error-code']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Details: ' + jsonData['error-details']));
            e.appendChild(ee);
         }
         else
         {
            console.log("Code " + httpRequest.status);
            div = document.getElementById(solutionBody);

            // clean the div
            div.className = '';
            while (div.firstChild)
            {
               div.removeChild(div.firstChild);
            }

            div.className = 'alert alert-danger';
            var e;
            e = document.createElement('p');
            e.appendChild(document.createTextNode('Unexpected error...'));
            div.appendChild(e);
         }
      }
   }
}

/* Loads and displays the comments on the solution */
function loadCommentsSolution(solutionid)
{
   // now we load the Comments

   var httpRequest = new XMLHttpRequest();
   if (!httpRequest)
   {
      alert('Giving up :( Cannot create an XMLHttpRequest instance');
      return false;
   }

   var url = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/respondTo/solutionid/" + solutionid;
   httpRequest.onreadystatechange = writeCommentsSolution;
   httpRequest.open('GET', url);
   httpRequest.send();

   function writeCommentsSolution()
   {
      // Get complete
      if (httpRequest.readyState === XMLHttpRequest.DONE)
      {
         // DOM elements
         var e;
         var ee;

         // 500 status is an error
         if (httpRequest.status == 500)
         {
            // Gets the div that should contain the comments
            var div = document.getElementById("solutionComments");

            // clean the div
            div.className = '';
            while (div.firstChild)
            {
               div.removeChild(div.firstChild);
            }

            // Let's incorporate the error message and then print it
            var jsonData = JSON.parse(httpRequest.responseText);
            jsonData = jsonData['message'];

            e = document.createElement('ul');
            div.appendChild(e);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Message: ' + jsonData['message']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Error code: ' + jsonData['error-code']));
            e.appendChild(ee);

            ee = document.createElement('li');
            ee.appendChild(document.createTextNode('Details: ' + jsonData['error-details']));
            e.appendChild(ee);

         }
         else if (httpRequest.status == 200)
         {
            var jsonData = JSON.parse(httpRequest.responseText);
            jsonData = jsonData['resource-list'];
            if (jsonData.length == 0)
               return;
            var div = document.getElementById('solutionComments');

            var e;
            var ee;

            // clean the div
            div.className = 'row';
            while (div.firstChild)
            {
               div.removeChild(div.firstChild);
            }

            // List all comments
            var comment;

            // Table first row

            var tableDiv = document.createElement('div');
            tableDiv.className = 'col-md-11';
            div.appendChild(tableDiv);

            e = document.createElement('table');
            e.className = 'addMargin table table-striped';
            tableDiv.appendChild(e);

            ee = document.createElement('thead');
            e.appendChild(ee);

            var tr = document.createElement('tr');
            ee.appendChild(tr);

            var th = document.createElement('th');
            th.className = 'user';
            th.appendChild(document.createTextNode('User'));
            tr.appendChild(th);

            th = document.createElement('th');
            th.className = 'commentTable';
            th.appendChild(document.createTextNode('Comment'));
            tr.appendChild(th);

            th = document.createElement('th');
            th.className = 'votes';
            th.appendChild(document.createTextNode('Votes'));
            tr.appendChild(th);

            th = document.createElement('th');
            th.className = 'writeTime';
            th.appendChild(document.createTextNode('Posted on'));
            tr.appendChild(th);

            var body = document.createElement('tbody');
            e.appendChild(body);


            for (var i = 0; i < jsonData.length; i++)
            {
               comment = jsonData[i].comment;

               tr = document.createElement('tr');
               body.appendChild(tr);

               var td = document.createElement('td');
               td.id = "username" + i;
               tr.appendChild(td);

               td = document.createElement('td');
               td.appendChild(document.createTextNode(comment['commentText']));
               tr.appendChild(td);

               td = document.createElement('td');
               var votes = parseInt(comment['upvote']) - parseInt(comment['downvote']);
               td.appendChild(document.createTextNode(' ' + votes + ' '));
               td.id = 'votes' + i;
               tr.appendChild(td);

               var id2 = '#votes' + i;
               $(id2).prepend("<i class='fas fa-arrow-circle-up'> </i>");
               $(id2).append("<i class='fas fa-arrow-circle-down'> </i>");

               td = document.createElement('td');
               td.appendChild(document.createTextNode(comment['writeTime']));
               tr.appendChild(td);

               // Let's write also the username and display the avatar of the user
               var solution = jsonData[i].solution;
               var getUser = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/user/" + comment['email'];

               $.getJSON(getUser, displayUsernameAndAvatar(i));
            }

         }
         else
         {
            // Gets the div that contains the question body
            var div = document.getElementById("solutionComments");

            // clean the div
            div.className = '';
            while (div.firstChild)
            {
               div.removeChild(div.firstChild);
            }

            div.className = 'alert alert-danger';

            e = document.createElement('p');
            e.appendChild(document.createTextNode('Unexpected error...'));
            div.appendChild(e);
         }
      }
   }
}

/* Display the avatar and the username */
function displayUsernameAndAvatar(item) {
   return function(data) {
      var id = "#username" + item;
      var avatarSrc = "<img title='Avatar of the user' src='../resource/avatar/" + getAvatar(data['resource-list'][0].user.avatar) + "'>";
      $(id).append(avatarSrc);
      p = document.createElement('p');
      p.append(document.createTextNode(data['resource-list'][0].user.username));
      $(id).append(p);
   };
}

/* It manages the submission of a new comment for a solution */
function submitNewComment()
{
   var commentUrl = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/comment";
   var respondToUrl = "http://localhost:8080/IronJobs-1.0-SNAPSHOT/rest/respondTo"
   var questionId = sessionStorage.getItem('questionId');
   var solutionId = sessionStorage.getItem('solutionid');

   // Read the input by the user
   var text = $('#user_comment_solution').val();
   // actual logged in user
   var email = sessionStorage.getItem('email');
   if (email == undefined)
   {
      alert("You need to login to be able to post a new comment!");
      return;
   }

   var comment = [{commentId:0,
      upvote: 0,
      downvote: 0,
      commentText: text,
      writeTime: new Date().toUTCString(),
      email: email
   }];

   $.ajax({
      method: "POST",
      url: commentUrl,
      data: JSON.stringify({comment: comment}),
      contentType: 'application/json',
      error: function( status ) {
         alert( "Status: " + status.responseText);
         console.log(status.responseText);
      },
   }).done(function(data){
      // get the new id of the posted comment
      console.log(data);
      var commentId = data.comment.commentId;

      var respondTo = [{
         solutionId: parseInt(solutionId),
         questionId: parseInt(questionId),
         commentId: parseInt(commentId)
      }];

      $.ajax({
         method: "POST",
         url: respondToUrl,
         data: JSON.stringify({respondTo: respondTo}),
         contentType: 'application/json',
         error: function( status ) {
            alert( "Status: " + status.responseText);
            console.log(status.responseText);
         },
      }).done(function(){
         // Now we reload the page to show the new comment
         location.reload(true);
      });
   });
}
