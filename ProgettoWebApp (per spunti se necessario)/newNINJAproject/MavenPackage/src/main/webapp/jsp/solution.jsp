<!--
 Copyright 2018 University of Padua, Italy

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Teofan Clipa
 Version: 1.0
 Since: 1.0
-->

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="author" content="Teofan Clipa">

      <!-- bootstrap css -->
      <link href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css"></link>
      <!-- FontAwesome -->
      <link href="<c:url value="/css/fontawesome-all.min.css"/>" rel="stylesheet" type="text/css"></link>
      <!-- custom css -->
      <link href="<c:url value="/css/insertnamehere.css"/>" rel="stylesheet" type="text/css"></link>
      <link href="<c:url value="/css/question.css"/>" rel="stylesheet" type="text/css"></link>
      <!-- JavaScript on the bottom -->

   </head>

   <body onload="loadSolutionWithComments()">
      <!-- Navigation Bar -->
		<div class="topnav">
		  <a href="<c:url value="/html/index.html"/>"> <img class="topnav-logo" src="<c:url value="/resource/SVG/logo.png"/>" alt="logo_nav"></a>
		  <a href="<c:url value="/jsp/questions.jsp"/>" class="burbank">QUESTIONS</a>
		  <a href="<c:url value= "/html/interviews.html"/>" class="burbank">INTERVIEWS</a>
		<!-- ************** -->

		<!-- Login Section -->

         <!-- Login Modal -->
         <div class="topnav-right" id = 'topnav-right'>
            <!-- Trigger the modal with a button -->
            <button type="button" class="login_button" data-toggle="modal" data-target="#myModal" id='login-text'>Login</button>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <h3 class="modal-title">Login</h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="modal-body">
                        <form class="form-group loginForm" method="POST"><br>
                           <input class="form-control-plaintext" name="userName" placeholder=" Username" type="text" required><br>
                           <input class="form-control-plaintext" name="password" placeholder=" Password" type="password" required><br>
                           <button class="btn mybtn-primary"><b>Login</b></button><br><br>
                           <p> Not a member? <button class="btn mybtn-primary" onclick="window.location.href='<c:url value= "/html/registration.html"/>';"><b>Register</b></button></p>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
		</div>
		<!-- ************** -->

      <!-- Solution page -->
      <div class="container containerOver">
         <div class="solutionPage addMargin" id="solutionContent">
            <h3 id="questionTitleSolution" class="questionTitle"></h3>
            <div class="row topPadding">
               <div class="col-md-1"></div>
               <div class="col-md-1" id="username">
                  <div id="avatar">
                  </div>
               </div>
               <div class="form-group col-md-8" id="solutionBody">
                  <textarea class="form-control questionBody" rows="7" id="solutionTextSolution" readonly="readonly"></textarea>
               </div>
            </div>

            <!-- Break the lines -->
            <div class="addMargin">
               <div class="col-md-1"></div>
               <div class="col-md-8"></div>
               <hr/>
               <div class="col-md-1"></div>
            </div>

            <!-- Comments -->
            <div id="solutionComments" class="commentsOnSolution">

            </div>

            <div class="form-group">
               <label for="comment"><b>Comment on the solution:</b></label>
               <textarea class="form-control" rows="5" id="user_comment_solution"></textarea>
            </div>
            <button name="btnSendCommentQuestion" type="submit" class="btn btn-primary" onclick="submitNewComment()">Post</button>
            <a class="btn btn-large btn-info" href="<c:url value='/jsp/question.jsp'/>">Back to the question</a>
         </div>

      </div> <!-- /Container -->
      <!-- ************** -->

      <!-- jQuery JS -->
      <script src="<c:url value="/js/jquery-3.3.1.min.js"/>"></script>
      <!-- Bootstrap JS -->
      <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
      <!-- Custom JS -->
      <script src="<c:url value="/js/solution.js"/>"></script>
      <script src="<c:url value="/js/insertnamehere.js"/>"></script>
   </body>
</html>
