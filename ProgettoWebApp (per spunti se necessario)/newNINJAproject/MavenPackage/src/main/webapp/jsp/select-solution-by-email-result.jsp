<%--
  Created by IntelliJ IDEA.
  User: nerub
  Date: 20/05/18
  Time: 0.47
  To change this template use File | Settings | File Templates.


--%>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Search Solution By Email Result</title>
</head>

<body>
<h1>Search Solution By Email Result</h1>
<hr/>

<!-- display the message -->
<c:import url="/jsp/include/show-message.jsp"/>

<!-- display the list of found solutions, if any -->
<c:if test='${not empty solutionlist}'>
    <table>
        <thead>
        <tr>
            <th>solutionId</th><th>upvote</th><th>downvote</th><th>isBest</th><th>solutiontext</th><th>isPremium</th><th>submitTime</th><th>email</th><th>questionId</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="solution" items="${solutionlist}">
            <tr>
                <td><c:out value="${solution.solutionId}"/></td>
                <td><c:out value="${solution.upvote}"/></td>
                <td><c:out value="${solution.downvote}"/></td>
                <td><c:out value="${solution.isBest}"/></td>
                <td><c:out value="${solution.solutionText}"/></td>
                <td><c:out value="${solution.isPremium}"/></td>
                <td><c:out value="${solution.submitTime}"/></td>
                <td><c:out value="${solution.email}"/></td>
                <td><c:out value="${solution.questionId}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>
</body>
</html>
