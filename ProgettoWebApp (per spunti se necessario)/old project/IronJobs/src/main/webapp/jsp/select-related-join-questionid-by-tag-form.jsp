<%--
  Created by IntelliJ IDEA.
  User: nerub
  Date: 20/05/18
  Time: 0.43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Search Related QuestionId By Tag Form</title>
</head>

<body>
<h1>Search Related QuestionId By Tag Form</h1>

<form method="POST" action="<c:url value="/select-related-join-questionid-by-tag"/>">
    <label for="tag">Tag:</label>
    <input name="tag" type="text"/><br/><br/>

    <button type="submit">Submit</button><br/>
    <button type="reset">Reset the form</button>
</form>
</body>
</html>