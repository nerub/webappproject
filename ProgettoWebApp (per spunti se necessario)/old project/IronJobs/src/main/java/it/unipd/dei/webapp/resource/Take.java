package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Take} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Take
{
	/**
	 * the take time
	 */
	private final String takeTime;
	/**
	 * the user mail
	 */
	private final String email;
	/**
	 * the interview id
	 */
	private final int interviewId;

	/**
	 * Creates a new take
	 * the taketime
	 * @param takeTime
	 * the user email
	 * @param email
	 * the interviewId
	 * @param interviewId
	 */
	public Take(final String takeTime, final String email, final int interviewId)
	{
		this.takeTime = takeTime;
		this.email = email;
		this.interviewId = interviewId;
	}
	
	// Get methods

	/**
	 * the taketime
	 * @return taketime
	 */
	public final String getTakeTime()
	{
		return takeTime;
	}

	/**
	 * the email
	 * @return email
	 */
	public final String getEmail()
	{
		return email;
	}

	/**
	 * interviewId
	 * @return interviewId
	 */
	public final int getInterviewId()
	{
		return interviewId;
	}
	
	///////////////
	
	// Set methods
	
	///////////////
}