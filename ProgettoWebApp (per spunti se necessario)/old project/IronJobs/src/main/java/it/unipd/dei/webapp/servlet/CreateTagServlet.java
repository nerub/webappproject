/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.InsertTagDatabase;
import it.unipd.dei.webapp.resource.Tag;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//necessario per il timestamp
import java.text.SimpleDateFormat;


/**
 * Creates a new Tag into the database.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */

public final class CreateTagServlet extends AbstractDatabaseServlet {

    /**
     * Creates a new Tag into the database.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        // request parameters
        String name = null;

        // model
        Tag e  = null;
        Message m = null;



        try{
            // retrieves the request parameters

            name = req.getParameter("name");

            // creates a new tag from the request parameters
            e = new Tag(name);

            // creates a new object for accessing the database and stores the tag
            new InsertTagDatabase(getDataSource().getConnection(), e).InsertTag();

            m = new Message(String.format("Tag %s successfully created.", name));

        } catch (NumberFormatException ex) {
            m = new Message("Cannot create the Tag. Invalid input parameter.",
                    "E100", ex.getMessage());
        } catch (SQLException ex) {
            if (ex.getSQLState().equals("23505")) {
                m = new Message(String.format("Cannot create the Tag: Tag %s already exists.", name),
                        "E300", ex.getMessage());
            } else {
                m = new Message("Cannot create the Tag: unexpected error while accessing the database.",
                        "E200", ex.getMessage());
            }
        }

        System.out.println(name);

        // stores the tag and the message as a request attribute
        req.setAttribute("Tag", e);
        req.setAttribute("message", m);

        // forwards the control to the create-tag-result JSP
        req.getRequestDispatcher("/jsp/create-tag-result.jsp").forward(req, res);
    }

}
