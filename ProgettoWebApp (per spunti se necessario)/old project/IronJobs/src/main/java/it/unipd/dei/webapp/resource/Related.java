package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Related} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Related
{
	/**
	 * the question id
	 */
	private final int questionId;
	/**
	 * the tag name
	 */
	private final String name;

	/**
	 * Creates a new related
	 * the question id
	 * @param questionId
	 * the tag name
	 * @param name
	 */
	public Related(final int questionId, final String name)
	{
		this.questionId = questionId;
		this.name = name;
	}
	
	// Get methods

	/**
	 * the questionid
	 * @return question id
	 */
	public final int getQuestionId()
	{
		return questionId;
	}

	/**
	 * the tag name
	 * @return tag name
	 */
	public final String getName()
	{
		return name;
	}
	
	///////////////
	
	// Set methods
	
	///////////////
}