package it.unipd.dei.webapp.resource;

/**
 * Represents a {@link Interview} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Interview
{
	/**
	 * the interviewId
	 */
	private final int interviewId;
	/**
	 * the expiring date
	 */
	private final String expiringDate;
	/**
	 * the description
	 */
	private final String description;
	/**
	 *  the currentTime
	 */
	private final String createTime;
	/**
	 *  the vatin
	 */
	private final String vatIn;

	/**
	 * Creates an Interview
	 * the interviewId
	 * @param interviewId
	 * the expiring date
	 * @param expiringDate
	 * the description
	 * @param description
	 * the crate time
	 * @param createTime
	 * the vatin
	 * @param vatIn
	 */
	public Interview(final int interviewId, final String expiringDate, final String description, final String createTime, final String vatIn)
	{
		this.interviewId = interviewId;
		this.expiringDate = expiringDate;
		this.description = description;
		this.createTime = createTime;
		this.vatIn = vatIn;
	}
	
	// Get methods

	/**
	 *the id
	 * @return id
	 */
	public final int getInterviewId()
	{
		return interviewId;
	}

	/**
	 * expiring date
	 * @return expirting date
	 */
	public final String getExpiringDate()
	{
		return expiringDate;
	}

	/**
	 * the description
	 * @return description
	 */
	public final String getDescription()
	{
		return description;
	}

	/**
	 * creationtime
	 * @return creation time
	 */
	public final String getCreateTime()
	{
		return createTime;
	}

	/**
	 * the vatin
	 * @return
	 */
	public final String getVatIn()
	{
		return vatIn;
	}

	///////////////
	
	// Set methods
	
	///////////////
}