
package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SelectSolutionByEmailDatabase;
import it.unipd.dei.webapp.resource.Solution;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Searches solutions by their email.
 *
 * @author Eooardo Furlan (edoardo.furlan.1@studenti.dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public final class SelectSolutionByEmailServlet extends AbstractDatabaseServlet {

    /**
     * Searches solutions by their email.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        //request parameter
        String email=null;

        // model
        List<Solution> el = null;
        Message m = null;

        // retrieves the request parameter
        email = req.getParameter("email");

        try {

            // creates a new object for accessing the database and searching the solutions
            el = new SelectSolutionByEmailDatabase(getDataSource().getConnection(),email)
                    .SelectSolutionByEmail();

            m = new Message("solutions successfully searched.");

        } catch (NumberFormatException ex) {
            m = new Message("Cannot search for solutions. Invalid input parameters: email must be integer.",
                    "E100", ex.getMessage());
        } catch (SQLException ex) {
            m = new Message("Cannot search for solutions: unexpected error while accessing the database.",
                    "E200", ex.getMessage());
        }

        // stores the solution list and the message as a request attribute
        req.setAttribute("solutionlist", el);
        req.setAttribute("message", m);

        // forwards the control to the search-solution-result JSP
        req.getRequestDispatcher("/jsp/select-solution-by-email-result.jsp").forward(req, res);

    }

}
