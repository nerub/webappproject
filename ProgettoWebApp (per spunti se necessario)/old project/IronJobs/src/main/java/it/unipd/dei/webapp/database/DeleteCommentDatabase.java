package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Comment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Deletes a comment from the database
 *
 * @version 1.00
 * @since 1.00
 */
public final class DeleteCommentDatabase
{
  /**
  * SQL statement to be executed
  */
  private static final String STATEMENT = "DELETE "
                                            + " FROM IronJobs.Comment"
                                            + " WHERE IronJobs.Comment.commentId = ?"
											+ " RETURNING *";
  /**
  * Connection to the database
  */
	private final Connection con;

  /**
  * Stores the Id of the comment to be deleted
  */
  private final int commentId;

  /**
	 * Creates a new object to delete a comment
	 *
	 * @param con
	 *            the connection to the database.
	 * @param commentId
	 *            the id of the comment to be deleted.
	 */
	public DeleteCommentDatabase(final Connection con, final int commentId)
	{
        this.con = con;
        this.commentId = commentId;
	}

/**
 * Deletes the comment from the database.
 *
 * @return The comment deleted.
 * @throws SQLException
 *             if any error occurs while deleting the comment.
 */
	public Comment DeleteComment() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Comment e = null;

		try
		{
			pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, commentId);

			rs = pstmt.executeQuery();

			if (rs.next()) {
				e = new Comment(rs.getInt("commentId"),
						rs.getString("commentText"),
						rs.getInt("upvote"),
						rs.getInt("downvote"),
						rs.getString("writeTime"));
			}

		}

		finally
		{
			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}
		return e;
	}
}
