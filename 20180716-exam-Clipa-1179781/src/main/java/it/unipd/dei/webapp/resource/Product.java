package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.*;

import java.io.*;

/** 
* Represents data about a product
*/
public class Product extends Resource
{
	/**
	* The id of the product
	*/
	private final int id;
	
	/*
	*	The category of the product
	*/
	private final String category;
	
	/*
	*	The name of the product
	*/
	private final String name;
	
	/*
	*	The age range of the product
	*/
	private final String ageRange;
	
	/*
	*	The price of the product
	*/
	private final double price;
	
	/**
	 * Creates a new product object
	 * 
	 * @param id the id number of the product
	 * @param category the category of the product.
	 * @param name the name of the product
	 * @param ageRange the age range of the product
	 * @param price the price of the product.
	 */
	public Product(final int id, final String category, final String name, final String ageRange, final double price) {
		this.id = id;
		this.category = category;
		this.name = name;
		this.ageRange = ageRange;
		this.price = price;
	}
	
	/**
	 * Returns the id number.
	 * 
	 * @return the id number of the product.
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Returns the category.
	 * 
	 * @return the category of the product.
	 */
	public final String getCategory() {
		return category;
	}
	
	/**
	 * Returns the name.
	 * 
	 * @return the name of the product.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Returns the ageRange.
	 * 
	 * @return the ageRange of the product.
	 */
	public final String getAgeRange() {
		return ageRange;
	}

	/**
	 * Returns the price.
	 * 
	 * @return the price of the product.
	 */
	public final double getPrice() {
		return price;
	}
	
	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("product");

		jg.writeStartObject();

		jg.writeNumberField("id", id);

		jg.writeStringField("category", category);
		
		jg.writeStringField("name", name);

		jg.writeStringField("ageRange", ageRange);

		jg.writeNumberField("price", price);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	
}

