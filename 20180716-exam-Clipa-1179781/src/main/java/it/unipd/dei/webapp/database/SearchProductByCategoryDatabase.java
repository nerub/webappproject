package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Product;
import it.unipd.dei.webapp.resource.ResourceList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Searches products by category.
 */
public final class SearchProductByCategoryDatabase {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "SELECT id, category, name, ageRange, price FROM MyToys.Product WHERE category= CAST(? AS MyToys.Category)";

	/**
	 * The connection to the database
	 */
	private final Connection con;

	/**
	 * The category of the product to search
	 */
	private final String category;

	/**
	 * Creates a new object for searching employees by salary.
	 * 
	 * @param con
	 *            the connection to the database.
	 * @param category
	 *            the category of the product.
	 */
	public SearchProductByCategoryDatabase(final Connection con, final String category) {
		this.con = con;
		this.category = category;
	}

	/**
	 * Searches products by their category
	 * 
	 * @return a list of {@code Employee} object matching the salary.
	 * 
	 * @throws SQLException
	 *             if any error occurs while searching for employees.
	 */
	public List<Product> searchProductByCategory() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Product> products = new ArrayList<Product>();

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, category);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				products.add(new Product(rs.getInt("id"), rs.getString("category"), rs.getString("name"), rs.getString("ageRange"),rs.getDouble("price")));
			}
		} finally {
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

		return products;
	}
}

