package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SearchProductByCategoryDatabase;
import it.unipd.dei.webapp.resource.ResourceList;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.Product;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Searches products by category
 */
public final class SearchProductByCategoryServlet extends AbstractDatabaseServlet {

	/**
	 * Searches products by category.
	 * 
	 * @param req
	 *            the HTTP request from the client.
	 * @param res
	 *            the HTTP response from the server.
	 * 
	 * @throws ServletException
	 *             if any error occurs while executing the servlet.
	 * @throws IOException
	 *             if any error occurs in the client/server communication.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// request parameter
		String category;

		// model
		List<Product> pl = null;
		Message m = null;

		try {
		
			// retrieves the request parameter
			category = (req.getParameter("category"));

			// creates a new object for accessing the database and searching the products
			pl = new SearchProductByCategoryDatabase(getDataSource().getConnection(), category)
					.searchProductByCategory();

			m = new Message("Products successfully searched.");
			
		} catch (SQLException ex) {
				m = new Message("Cannot search for products: unexpected error while accessing the database.", 
						"E100", ex.getMessage());
		}

		// set the MIME media type of the response
		res.setContentType("application/json; charset=utf-8");

		// get a stream to write the response
		OutputStream o = res.getOutputStream();
		
		ResourceList<Product> rs = new ResourceList(pl);
		
		rs.toJSON(o);
		
		o.flush();
		o.close();


	}

}

