I form NON devono essere ovviamente circondati da un tratteggio rosso.


Alla pagina, va aggiunto sotto un messaggio di errore uguale a quello della prova di esame.

Inoltre in caso di caricamento un messaggio di conferma è postato. Idenico a quello di errore, solo che anzichè avere i 3 puntini, ne ha 5, con le varie variabili inserite.

Creare validation in js per il form.
Fare in modo che js faccia una chiamata AJAX POST al server, al link magico che il prof ti dice. (i dati inseriti devono essere inseriti in un JSON)

In caso di rispoista negativa printa l'errore.

In caso di esito positivo printa i valori inseriti.