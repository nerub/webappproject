$(document).ready(function(){

    var httpRequest ;
    document.getElementById("form").addEventListener("submit",function(event){

        var bod = document.getElementById("body");
        httpRequest = new XMLHttpRequest();
        console.log(document.getElementById("cat").value);
        var url = document.getElementById("form").action +"?category="+ document.getElementById("cat").value;
        console.log(url);
        
        if(!httpRequest){
            alert("cannot create http");
            return false;
        }

        httpRequest.onreadystatechange= populate;
        httpRequest.open("GET", url);
        httpRequest.send();
        event.preventDefault();
    });


    function populate(){
        if(httpRequest.readyState === XMLHttpRequest.DONE){
            
            var tbody = document.getElementById("body");

            tbody.className="";
            while(tbody.firstchild){
                tbody.removeChild(tbody.firstchild);
            }

            if (httpRequest.status == 500) {
                console.log("500");
            }

            else if (httpRequest.status == 200) {
                var tr;
                var td;

                var jsonData = JSON.parse(httpRequest.responseText);
                jsonDataArray = jsonData["resource-list"];
                var game;

                for(var i = 0; i<jsonDataArray.length; i++){
                    game = jsonDataArray[i].game;

                    tr=document.createElement("tr");
                    tbody.appendChild(tr);

                    td=document.createElement("td")
                    td.appendChild(document.createTextNode(game["id"]));
                    tr.appendChild(td);

                    td=document.createElement("td")
                    td.appendChild(document.createTextNode(game["category"]));
                    tr.appendChild(td);

                    td=document.createElement("td")
                    td.appendChild(document.createTextNode(game["name"]));
                    tr.appendChild(td);

                    td=document.createElement("td")
                    td.appendChild(document.createTextNode(game["ageRange"]));
                    tr.appendChild(td);

                    td=document.createElement("td")
                    td.appendChild(document.createTextNode(game["price"]));
                    tr.appendChild(td);
                }


            } else{
                console.log("mi no so niente");
            }
        }
    }

});

