<!--
 
 Copyright 2018 University of Padua, Italy
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 Author: Edoardo Furlan(edoardo.furlan.1@studenti.unipd.it)
 Version: 1.0
 Since: 1.0
-->

<!-- qui importo le librerie per jsp -->
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="example web application for exam simulation">
    <meta name="author" content="Edoardo Furlan">

    <title>GamesTable</title>

    	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

	<!-- Font Awesome CSS-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

	<!-- Custom styles for this template -->
	 <link href="<c:url value="/css/Games.css"/>" rel="stylesheet">
    <!-- <link href="../css/Games.css" rel="stylesheet"> -->

    <!-- importo css, sia fonti esterne che il mio file 
    se vado in jsp l'import locale diviene:

    <link href="<c:url value="/css/NOMEFILE.css"/>" rel="stylesheet">
    -->

  </head>
    <body>
        <!-- Sistema di griglie -->
        <div class="container" >
            <div class=row>
                
                    <!-- Tabella con titolo e spiegazione -->
                    <div class="container" >
                        <h1>Games Table</h1>
                        <p>Here are reported our games</p>  
                            <form method="get" id="form" action="http://localhost:8080/20180620-examsim-furlan-1149318-1.0-SNAPSHOT/rest/search-games-by-category">
                                <input type="text" id="cat">         
                                <table class="table table-hover">
                                    
                                        <thead>
                                            <tr>
                                            <th>Id</th>
                                            <th>Category</th>
                                            <th>Name</th>
                                            <th>AgeRange</th>
                                            <th>Price</th>
                                            </tr>
                                        </thead>

                                        <tbody id="body">
                                            
                                        </tbody>
                                    

                                </table>
                            <button type="submit" id="button" class="btn btn-lg">
                                <i class="fas fa-search"></i> Search
                            </button>
                        </form>
                    </div>    
                    <!-- FineTabella -->
                </div>
                
            </div>
        </div>
        <!-- Fine sistema di griglie -->
       
        




        <!-- appena prima della fine del body importo js, fonti esterne e locali
            nel caso di locale con jsp diventa:
            <script src="<c:url value="/js/NOMEFILE.js"/>"></script>
        -->
	<!-- Bootstrap, Popper, and JQuery JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	
     <script src="<c:url value="/js/Games.js"/>"></script> 
    <!-- <script src="../js/Games.js"></script> -->

    </body>
</html>