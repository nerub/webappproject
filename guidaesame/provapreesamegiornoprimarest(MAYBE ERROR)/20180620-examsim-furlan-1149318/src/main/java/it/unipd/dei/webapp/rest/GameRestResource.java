
package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.*;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public final class GameRestResource extends RestResource
{
    
    public GameRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con)
	{
        super(req, res, con);
    }

    public void searchGames() throws IOException
    {
      List<Game> u  = null;
      Message m = null;

      try
      {
          // parse the URI path to extract the questionId
          /*String path = req.getRequestURI();
          String attributeName = "?category=";
          path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());
          path = "Doll";*/

          String category= req.getParameter("category");

          // creates a new object for accessing the database and create the comments
          u = new SearchGamesByCategoryDatabase(con, category).SearchGamesByCategory();

          if(u != null)
          {
              res.setStatus(HttpServletResponse.SC_OK);
              new ResourceList<Game>(u).toJSON(res.getOutputStream());
          }

          else
          {
              // it should not happen
              m = new Message("Cannot create comment: unexpected error.", "E5A1", null);
              res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
              m.toJSON(res.getOutputStream());
          }
      }

      catch (Throwable t)
      {
          m = new Message("Cannot create comment: unexpected error.", "E5A1", t.getMessage());
          res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
          m.toJSON(res.getOutputStream());
      }

    }
}
