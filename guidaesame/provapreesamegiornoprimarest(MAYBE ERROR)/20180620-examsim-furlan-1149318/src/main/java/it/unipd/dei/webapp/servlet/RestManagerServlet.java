

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.resource.*;
import it.unipd.dei.webapp.rest.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;


public final class RestManagerServlet extends AbstractDatabaseServlet
{
 
    private static final String JSON_MEDIA_TYPE = "application/json";


    private static final String JSON_UTF_8_MEDIA_TYPE = "application/json; charset=utf-8";


    private static final String ALL_MEDIA_TYPE = "*/*";

    @Override
    protected final void service(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException
	{
        res.setContentType(JSON_UTF_8_MEDIA_TYPE);
        final OutputStream out = res.getOutputStream();

        try
		{
            // if the request method and/or the MIME media type are not allowed, return.
            // Appropriate error message sent by {@code checkMethodMediaType}
            if (!checkMethodMediaType(req, res))
			{
                return;
            }

            // if the requested resource was an Game, delegate its processing and return
            if (processGame(req, res))
			{
                return;
            }

            // if none of the above process methods succeeds, it means an unknow resource has been requested
            final Message m = new Message("Unknown resource requested.", "E4A6",
                    String.format("Requested resource is %s.", req.getRequestURI()));
            res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            m.toJSON(out);
        }

		finally
		{
            // ensure to always flush and close the output stream
            out.flush();
            out.close();
        }
    }

    private boolean checkMethodMediaType(final HttpServletRequest req, final HttpServletResponse res) throws IOException
	{
        final String method = req.getMethod();
        final String contentType = req.getHeader("Content-Type");
        final String accept = req.getHeader("Accept");
        final OutputStream out = res.getOutputStream();

        Message m = null;

        if (accept == null)
		{
            m = new Message("Output media type not specified.", "E4A1", "Accept request header missing.");
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            m.toJSON(out);

			return false;
        }

        if (!accept.contains(JSON_MEDIA_TYPE) && !accept.equals(ALL_MEDIA_TYPE))
		{
            m = new Message("Unsupported output media type. Resources are represented only in application/json.",
                    "E4A2", String.format("Requested representation is %s.", accept));
            res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            m.toJSON(out);

			return false;
        }

        switch (method)
		{
            case "GET":
            case "DELETE":
                // nothing to do
                break;

            case "POST":
            case "PUT":
                if (contentType == null)
				{
                    m = new Message("Input media type not specified.", "E4A3", "Content-Type request header missing.");
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    m.toJSON(out);

					return false;
                }

                if (!contentType.contains(JSON_MEDIA_TYPE))
				{
                    m = new Message("Unsupported input media type. Resources are represented only in application/json.",
                            "E4A4", String.format("Submitted representation is %s.", contentType));
                    res.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
                    m.toJSON(out);

					return false;
                }

                break;
            default:
                m = new Message("Unsupported operation.",
                        "E4A5", String.format("Requested operation %s.", method));
                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                m.toJSON(out);

				return false;
        }

        return true;
    }

    


    private boolean processGame(HttpServletRequest req, HttpServletResponse res) throws IOException
	{

        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not an game
        if (path.lastIndexOf("rest/search-games-by-category") <= 0)
        {
            return false;
        }

        try
        {
            // strip everyhing until after the /game
			String className = "?category=";
            path = path.substring(path.lastIndexOf(className) + className.length());

            // the request URI is: /game
            // if method POST, insert game
            if (path.length() == 0 || path.equals("/"))
            {
                switch (method) {
                    case "POST":
                        
                        break;

                    default:
                        m = new Message("Unsupported operation for URI /game.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }
            }
            else
            {
               switch(method)
               {
                  case "GET":
                  
                    new GameRestResource(req, res, getDataSource().getConnection()).searchGames();
                     break;

                  default:
                     m = new Message("Unsupported operation for URI /game/email.",
                     "E4A5", String.format("Requested operation %s.", method));
                     res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                     m.toJSON(res.getOutputStream());
                     break;
               }
            }
        }

        catch (Throwable t)
        {
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;
    }

    
}
