package it.unipd.dei.webapp.resource;
import java.io.*;
import com.fasterxml.jackson.core.*;

public class Game extends Resource
{
	
	private final int id;
	
	private String category;
	
	private final String name;
	
	private String ageRange;

	private final double price;
	


	
	public Game(final int id, String category, final String name, String ageRange, final double price)
	{
		this.id=id;
        this.category=category;
		this.name=name;
		this.ageRange=ageRange;
		this.price=price;
	}

    // Get methods
    
    public int getId()
	{
		return id;
	}

	public final String getCategory()
	{
		return category;
	}

	public String getName()
	{
		return name;
	}

	public String getAgeRange()
	{
		return ageRange;
	}

	public double getPrice()
	{
		return price;
	}

	// JSON parsing

	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("game");

		jg.writeStartObject();

		jg.writeNumberField("id", id);

		jg.writeStringField("category", category);

		jg.writeStringField("name", name);

		jg.writeStringField("ageRange", ageRange);

		jg.writeNumberField("price", price);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}
}
