package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Game;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class SearchGamesDatabase
{

  private static final String STATEMENT = "SELECT * FROM games.giocattoli";


	private final Connection con;


	public SearchGamesDatabase(final Connection con)
	{
		this.con = con;
	}


	public List<Game> SearchGames() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Game> games = new ArrayList<Game>();

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
					games.add(new Game(rs.getInt("id"),
										rs.getString("category"),
										rs.getString("name"),
										rs.getString("ageRange"),
										rs.getDouble("price")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return games;
	}
}