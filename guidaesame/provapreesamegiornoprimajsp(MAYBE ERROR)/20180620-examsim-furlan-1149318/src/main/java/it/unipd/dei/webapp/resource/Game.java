package it.unipd.dei.webapp.resource;
import java.io.*;

public class Game
{
	
	private final int id;
	
	private String category;
	
	private final String name;
	
	private String ageRange;

	private final double price;
	


	
	public Game(final int id, String category, final String name, String ageRange, final double price)
	{
		this.id=id;
        this.category=category;
		this.name=name;
		this.ageRange=ageRange;
		this.price=price;
	}

    // Get methods
    
    public int getId()
	{
		return id;
	}

	public final String getCategory()
	{
		return category;
	}

	public String getName()
	{
		return name;
	}

	public String getAgeRange()
	{
		return ageRange;
	}

	public double getPrice()
	{
		return price;
	}
}