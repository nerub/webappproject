package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SearchGamesDatabase;
import it.unipd.dei.webapp.resource.Game;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

public final class SearchGamesServlet extends AbstractDatabaseServlet
{

   
   public void doPost(HttpServletRequest req, HttpServletResponse res)
           throws ServletException, IOException {

      // model
      List<Game> el = null;
      Message m = null;

      try {

           // creates a new object for accessing the database and searching the games
           el = new SearchGamesDatabase(getDataSource().getConnection())
                   .SearchGames();

           m = new Message("games successfully searched.");

           res.setStatus(HttpServletResponse.SC_OK);

      } catch (NumberFormatException ex) {
           m = new Message("Cannot search for games.",
                   "E100", ex.getMessage());
                   res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
           m.toJSON(res.getOutputStream());
      } catch (SQLException ex) {
           m = new Message("Cannot search for games: unexpected error while accessing the database.",
                   "E200", ex.getMessage());
                   res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
           m.toJSON(res.getOutputStream());
      }

      el.add(new Game(1,"a","b","c",3.6));
      // stores the game list and the message as a request attribute
      req.setAttribute("gameList", el);
      req.setAttribute("message", m);

      // forwards the control
      req.getRequestDispatcher("/jsp/gamesTable.jsp").forward(req, res);

   }


}