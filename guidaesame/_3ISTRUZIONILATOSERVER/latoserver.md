# Creazione classi e file(Idea, poi si vede l'effettiva implementazione)

### Dentro la cartella main/webapp

    -creare in /jsp/include un jsp per mostrare il messaggio d'errore(che nel caso di uso di jsp è chiamato da tutte le altre jsp prima di printare i risultati)

    -modificare adeguatamentie il web.xml, perchè gestisca i servlet.

    -/main/database(eventuale codice sql)

### Dentro src, se non già presente creo java/it/unipd/dei/webapp

Dentro webapp creo le cartelle:

-servlet    
    
    (contenente:
            -AbstractDatabseServlet.java
            -RestServletManager.java (solo se uso rest)
            -un servlet per ogni risorsa che voglio (se non uso rest, altrimenti gestisco tutto nel RestServletManager)
        )

-database

    (contiene una classe jdbc per ogni tipo di query che voglio fare)

-resource

    (contiene:
        -una classe per ogni tabella del DB( che nel caso di rest deve pure implementare i metodi toJson e fromJson, estendendo la classe Resource.java)
        -una classe che gestisce l'errore
        -nel caso di rest anche:
            -ResourceList.java
            -Resource.java
        )

-rest
    
    (da fare solo nel caso di rest) Contiene:

        -una classe RestResource.java estesa da tutte le altre
        -una "Classe"RestResource per ogni tabella, che dentro gestisce come un servlet tutte le azioni che io posso fare su quella tabella in metodi diversi. 
        NB: il servletRestManager, dopo aver filtrato gli indirizzi rimanda tutto qui
        


# WEB.XML

Devo aggiungere le istruzioni con cui il server tomcat distribuisce gli ordini che gli arrivano:
Per esempio in questo caso di ce che il servlet chiamato **CreateTag** va mappato al file che lo contiene in **it.unipd.dei.webapp.servlet.CreateTagServlet**

	<!-- NAMING SRVLETS-->

	<servlet>
		<servlet-name>CreateTag</servlet-name>
		<servlet-class>it.unipd.dei.webapp.servlet.CreateTagServlet</servlet-class>
	</servlet>

Successivamente per ogni url che io voglio gestire mappo l'url ad un servlet:
In questo caso sto mappando gli url **/create-tag** e **/create-tag-blue** al servlet chiamato
**CreateTag**
<!-- MAPPING SERVLET	-->

	<servlet-mapping>
		<servlet-name>CreateTag</servlet-name>
		<url-pattern>/create-tag</url-pattern>
	</servlet-mapping>

    <servlet-mapping>
		<servlet-name>CreateTag</servlet-name>
		<url-pattern>/create-tag-blue</url-pattern>
	</servlet-mapping>

    
Nel caso in cui io voglia usare Rest anziche i classici servlet divisi devo:
mappare ogni indirizzo **/rest/\*** al **RestManagerServlet** situato in **it.unipd.dei.webapp.servlet.RestManagerServlet**

	<!-- REST SRVLETS-->

	<servlet>
		<servlet-name>RestManagerServlet</servlet-name>
		<servlet-class>it.unipd.dei.webapp.servlet.RestManagerServlet</servlet-class>
	</servlet>

	<servlet-mapping>
		<servlet-name>RestManagerServlet</servlet-name>
		<url-pattern>/rest/*</url-pattern>
	</servlet-mapping>


Alla fine devo **Asoolutamente** specificare la connessione (con lo stesso nome specificato nel context.xml) al database e aggiungo:

    <!-- END -->
	<resource-ref>
      <description>Connection pool to the database</description>
      <res-ref-name>jdbc/20180620-examsim-furlan-1149318-pool</res-ref-name>
      <res-type>javax.sql.DataSource</res-type>
      <res-auth>Container</res-auth>
  	</resource-ref>


*************************************************************************************************

# SERVLET NORMALE NON REST (abbiamo bisogno di un jdbc, un AbstractDatabaseServlet, un "queryRisorsa"Servlet, e infine una classe risorsa.)

## JDBC

classe per cercare per valore:

    

    package it.unipd.dei.webapp.database;

    import it.unipd.dei.webapp.resource.Employee;

    import java.sql.Connection;
    import java.sql.PreparedStatement;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    import java.util.ArrayList;
    import java.util.List;

   
    public final class SearchEmployeeBySalaryDatabase {

        
        private static final String STATEMENT = "SELECT badge, surname, age, salary FROM Ferro.Employee WHERE salary > ?";

        
        private final Connection con;

        
        private final int salary;

       
        public SearchEmployeeBySalaryDatabase(final Connection con, final int salary) {
            this.con = con;
            this.salary = salary;
        }

        
        public List<Employee> searchEmployeeBySalary() throws SQLException {

            PreparedStatement pstmt = null;
            ResultSet rs = null;

            // the results of the search
            final List<Employee> employees = new ArrayList<Employee>();

            try {
                pstmt = con.prepareStatement(STATEMENT);
                pstmt.setInt(1, salary);

                rs = pstmt.executeQuery();

                while (rs.next()) {
                    employees.add(new Employee(rs.getInt("badge"), rs
                            .getString("surname"), rs.getInt("age"),
                            rs.getInt("salary")));
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }

                if (pstmt != null) {
                    pstmt.close();
                }

                con.close();
            }

            return employees;
        }
    }

Se creare una nuova entry nella tabella

    

    package it.unipd.dei.webapp.database;

    import it.unipd.dei.webapp.resource.Employee;

    import java.sql.Connection;
    import java.sql.PreparedStatement;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    import java.util.ArrayList;
    import java.util.List;

    
    public final class CreateEmployeeDatabase {

        
        private static final String STATEMENT = "INSERT INTO Ferro.Employee (badge, surname, age, salary) VALUES (?, ?, ?, ?) RETURNING *";

        
        private final Connection con;

        
        private final Employee employee;

        
        public CreateEmployeeDatabase(final Connection con, final Employee employee) {
            this.con = con;
            this.employee = employee;
        }

        
        public Employee createEmployee() throws SQLException {

            PreparedStatement pstmt = null;
            ResultSet rs = null;

            // the create employee
            Employee e = null;

            try {
                pstmt = con.prepareStatement(STATEMENT);
                pstmt.setInt(1, employee.getBadge());
                pstmt.setString(2, employee.getSurname());
                pstmt.setInt(3, employee.getAge());
                pstmt.setInt(4, employee.getSalary());

                rs = pstmt.executeQuery();

                if (rs.next()) {
                    e = new Employee(rs.getInt("badge"), rs
                            .getString("surname"), rs.getInt("age"),
                            rs.getInt("salary"));
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }

                if (pstmt != null) {
                    pstmt.close();
                }

                con.close();
            }

            return e;
        }
    }






## ABSTRACTDATABASE

    

    package it.unipd.dei.webapp.servlet;

    import javax.naming.InitialContext;
    import javax.naming.NamingException;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.ServletConfig;
    import javax.servlet.ServletException;
    import javax.sql.DataSource;

    
    public abstract class AbstractDatabaseServlet extends HttpServlet {

    	private DataSource ds;

        public void init(ServletConfig config) throws ServletException {

            // the JNDI lookup context
            InitialContext cxt;

            try {
                cxt = new InitialContext();
                ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/20180620-examsim-furlan-1149318-pool");
            } catch (NamingException e) {
                ds = null;

                throw new ServletException(
                        String.format("Impossible to access the connection pool to the  database: %s",
                                      e.getMessage()));
            }
        }
    
        public void destroy() {
            ds = null;
        }

        protected final DataSource getDataSource() {
            return ds;
        }

    }

## "queryRisorsa"Servlet
Naturalmente a secodna dell'azione che voglio compiere, passero' più o meno dati e chiamero' diversi jdbc(Sostituire VAR con la risorsa, e salary con il paramentro)

    

    package it.unipd.dei.webapp.servlet;

    import it.unipd.dei.webapp.database.SearchVARBySalaryDatabase;
    import it.unipd.dei.webapp.resource.VAR;
    import it.unipd.dei.webapp.resource.Message;

    import java.io.IOException;
    import java.io.PrintWriter;
    import java.sql.Connection;
    import java.sql.SQLException;
    import java.util.List;

    import javax.naming.InitialContext;
    import javax.naming.NamingException;
    import javax.servlet.ServletException;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import javax.sql.DataSource;


    public final class SearchVARBySalaryServlet extends AbstractDatabaseServlet {

    	
    	public void doPost(HttpServletRequest req, HttpServletResponse res)
    			throws ServletException, IOException {

    		// request parameter
    		int salary;

    		// model
    		List<VAR> el = null;
    		Message m = null;

    		try {
            
    			// retrieves the request parameter
    			salary = Integer.parseInt(req.getParameter("salary"));

    			// creates a new object for accessing the database and searching the VARs
    			el = new SearchVARBySalaryDatabase(getDataSource().getConnection(), salary)
    					.searchVARBySalary();

    			m = new Message("VARs successfully searched.");
    
    		} catch (NumberFormatException ex) {
    			m = new Message("Cannot search for VARs. Invalid input parameters: salary must be integer.", 
    					"E100", ex.getMessage());
    		} catch (SQLException ex) {
    				m = new Message("Cannot search for VARs: unexpected error while accessing the database.", 
    						"E200", ex.getMessage());
    		}

    		// stores the VAR list and the message as a request attribute
    		req.setAttribute("VARList", el);
    		req.setAttribute("message", m);
    
    		// forwards the control to the search-VAR-result JSP
    		req.getRequestDispatcher("/jsp/search-VAR-result.jsp").forward(req,   res);

    	    }

        }

## Una classe risorsa per l'oggetto

Classe di oggetto generico

    
    package it.unipd.dei.webapp.resource;

    public class VAR {

        
        private final int badge;

        
        private final String surname;

        
        private final int age;

        
        private final int salary;

        
        public VAR(final int badge, final String surname, final int age, final int salary) {
            this.badge = badge;
            this.surname = surname;
            this.age = age;
            this.salary = salary;
        }

        
        public final int getBadge() {
            return badge;
        }

        
        public final String getSurname() {
            return surname;
        }

        
        public final int getAge() {
            return age;
        }

        
        public final int getSalary() {
            return salary;
        }

    }

Classe di oggetto messaggio(una sola per gestire i messaggi)

   
    package it.unipd.dei.webapp.resource;

    
    public class Message {

       
        private final String message;

        
        private final String errorCode;

        
        private final String errorDetails;
        
        
        private final boolean isError;


        
        public Message(final String message, final String errorCode, final String errorDetails) {
            this.message = message;
            this.errorCode = errorCode;
            this.errorDetails = errorDetails;
            this.isError = true;
        }


        
        public Message(final String message) {
            this.message = message;
            this.errorCode = null;
            this.errorDetails = null;
            this.isError = false;
        }


        
        public final String getMessage() {
            return message;
        }

        
        public final String getErrorCode() {
            return errorCode;
        }
        
        
        public final String getErrorDetails() {
            return errorDetails;
        }

        
        public final boolean isError() {
            return isError;
        }

    }

****************************************************************************************************************************************************************************


****************************************************************************************************************************************************************************


****************************************************************************************************************************************************************************


## REST (Qui abbisogno di : RestManagerServlet, /resource(ResourceList, Resource, estendere le classi oggetto con json, /rest(RestResource e "Classe"RestResource))

## RestManagerServlet

Gestisce, tutte le chiamate rest, filtra le chiamate se hanno il giusto mediatype, e poi passano il lavoro ad un metodo specifico per ogni Risorsa. In questo metodo l'indirizzo è spolpato, se ne tirano fuori i valori, e si passano in una chiamata alla **"Classe"RestResource**

    

    package it.unipd.dei.webapp.servlet;

    import it.unipd.dei.webapp.resource.*;
    import it.unipd.dei.webapp.rest.VARRestResource;

    import javax.servlet.ServletException;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import java.io.IOException;
    import java.io.OutputStream;

    
    public final class RestManagerServlet extends AbstractDatabaseServlet {

       
        private static final String JSON_MEDIA_TYPE = "application/json";

        
        private static final String JSON_UTF_8_MEDIA_TYPE = "application/json; charset=utf-8";

        
        private static final String ALL_MEDIA_TYPE = "*/*";

        @Override
        protected final void service(final HttpServletRequest req, final HttpServletResponse res)
                throws ServletException, IOException {

            res.setContentType(JSON_UTF_8_MEDIA_TYPE);
            final OutputStream out = res.getOutputStream();

            try {
                // if the request method and/or the MIME media type are not allowed, return.
                // Appropriate error message sent by {@code checkMethodMediaType}
                if (!checkMethodMediaType(req, res)) {
                    return;
                }

                // if the requested resource was an VAR, delegate its processing and return
                if (processVAR(req, res)) {
                    return;
                }

                // if none of the above process methods succeeds, it means an unknow resource has been requested
                final Message m = new Message("Unknown resource requested.", "E4A6",
                                            String.format("Requested resource is %s.", req.getRequestURI()));
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(out);
            } finally {
                // ensure to always flush and close the output stream
                out.flush();
                out.close();
            }
        }

        
        private boolean checkMethodMediaType(final HttpServletRequest req, final HttpServletResponse res)
                throws IOException {

            final String method = req.getMethod();
            final String contentType = req.getHeader("Content-Type");
            final String accept = req.getHeader("Accept");
            final OutputStream out = res.getOutputStream();

            Message m = null;

            if(accept == null) {
                m = new Message("Output media type not specified.", "E4A1", "Accept request header missing.");
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                m.toJSON(out);
                return false;
            }

            if(!accept.contains(JSON_MEDIA_TYPE) && !accept.equals(ALL_MEDIA_TYPE)) {
                m = new Message("Unsupported output media type. Resources are represented only in application/json.",
                                "E4A2", String.format("Requested representation is %s.", accept));
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(out);
                return false;
            }

            switch(method) {
                case "GET":
                case "DELETE":
                    // nothing to do
                    break;

                case "POST":
                case "PUT":
                    if(contentType == null) {
                        m = new Message("Input media type not specified.", "E4A3", "Content-Type request header missing.");
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(out);
                        return false;
                    }

                    if(!contentType.contains(JSON_MEDIA_TYPE)) {
                        m = new Message("Unsupported input media type. Resources are represented only in application/json.",
                                        "E4A4", String.format("Submitted representation is %s.", contentType));
                        res.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
                        m.toJSON(out);
                        return false;
                    }

                    break;
                default:
                    m = new Message("Unsupported operation.",
                                    "E4A5", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                    return false;
            }

            return true;
        }


        

        private boolean processVAR(HttpServletRequest req, HttpServletResponse res) throws IOException {

            final String method = req.getMethod();
            final OutputStream out = res.getOutputStream();

            String path = req.getRequestURI();
            Message m = null;

            // the requested resource was not an VAR
            if(path.lastIndexOf("rest/VAR") <= 0) {
                return false;
            }

            try {
                // strip everyhing until after the /VAR
                path = path.substring(path.lastIndexOf("VAR") + 8);

                // the request URI is: /VAR
                // if method GET, list VARs
                // if method POST, create VAR
                if (path.length() == 0 || path.equals("/")) {

                    switch (method) {
                        case "GET":
                            new VARRestResource(req, res, getDataSource().getConnection()).listVAR();
                            break;
                        case "POST":
                            new VARRestResource(req, res, getDataSource().getConnection()).createVAR();
                            break;
                        default:
                            m = new Message("Unsupported operation for URI /VAR.",
                                            "E4A5", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(res.getOutputStream());
                            break;
                    }
                } else {
                    // the request URI is: /VAR/salary/{salary}
                    if (path.contains("salary")) {
                        path = path.substring(path.lastIndexOf("salary") + 6);

                        if (path.length() == 0 || path.equals("/")) {
                            m = new Message("Wrong format for URI /VAR/salary/{salary}: no {salary} specified.",
                                            "E4A7", String.format("Requesed URI: %s.", req.getRequestURI()));
                            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            m.toJSON(res.getOutputStream());
                        } else {
                            switch (method) {
                                case "GET":

                                    // check that the parameter is actually an integer
                                    try {
                                        Integer.parseInt(path.substring(1));

                                        new VARRestResource(req, res, getDataSource().getConnection()).searchVARBySalary();
                                    } catch (NumberFormatException e) {
                                        m = new Message(
                                                "Wrong format for URI /VAR/salary/{salary}: {salary} is not an integer.",
                                                "E4A7", e.getMessage());
                                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                        m.toJSON(res.getOutputStream());
                                    }

                                    break;
                                default:
                                    m = new Message("Unsupported operation for URI /VAR/salary/{salary}.", "E4A5",
                                                    String.format("Requested operation %s.", method));
                                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                    m.toJSON(res.getOutputStream());
                                    break;
                            }
                        }
                    } else {
                        // the request URI is: /VAR/{badge}
                        try {

                            // check that the parameter is actually an integer
                            Integer.parseInt(path.substring(1));

                            switch (method) {
                                case "GET":
                                    new VARRestResource(req, res, getDataSource().getConnection()).readVAR();
                                    break;
                                case "PUT":
                                    new VARRestResource(req, res, getDataSource().getConnection()).updateVAR();
                                    break;
                                case "DELETE":
                                    new VARRestResource(req, res, getDataSource().getConnection()).deleteVAR();
                                    break;
                                default:
                                    m = new Message("Unsupported operation for URI /VAR/{badge}.",
                                                    "E4A5", String.format("Requested operation %s.", method));
                                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                    m.toJSON(res.getOutputStream());
                            }
                        } catch (NumberFormatException e) {
                            m = new Message("Wrong format for URI /VAR/{badge}: {badge} is not an integer.",
                                            "E4A7", e.getMessage());
                            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            m.toJSON(res.getOutputStream());
                        }
                    }
                }
            } catch(Throwable t) {
                m = new Message("Unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }

            return true;

        }
    }



## /resource/ResourceList

Si occupa di definire i metodi di conversione dato java a **Json** estende resource.java ed è a sua volta estesa dalle altre risorse

    

    package it.unipd.dei.webapp.resource;

    import com.fasterxml.jackson.core.JsonGenerator;
    import java.io.*;

    
    public final class ResourceList<T extends Resource> extends Resource {


        private final Iterable<T> list;


        public ResourceList(final Iterable<T> list) {
            this.list = list;
        }

        @Override
        public final void toJSON(final OutputStream out) throws IOException {

            final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

            jg.writeStartObject();

            jg.writeFieldName("resource-list");

            jg.writeStartArray();

            jg.flush();

            boolean firstElement = true;

            for (final Resource r : list) {

                // very bad work-around to add commas between resources
                if (firstElement) {
                    r.toJSON(out);
                    jg.flush();

                    firstElement = false;
                } else {
                    jg.writeRaw(',');
                    jg.flush();

                    r.toJSON(out);
                    jg.flush();
                }
            }

            jg.writeEndArray();

            jg.writeEndObject();

            jg.flush();

            jg.close();
        }

    }

## /resource/Resource

Tipo una interfaccia che gestisce il json parser

    

    package it.unipd.dei.webapp.resource;

    import com.fasterxml.jackson.core.*;

    import java.io.*;

    public abstract class Resource {

        protected static final JsonFactory JSON_FACTORY;

        static {
            // setup the JSON factory
            JSON_FACTORY = new JsonFactory();
            JSON_FACTORY.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
            JSON_FACTORY.disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        }

        //metodo da scrivere in 
        public abstract void toJSON(final OutputStream out) throws IOException;
    }

## /resource/ classi oggetto + messaggio

Le classi vanno estese con i metodi atti a gestire json toJson e fromJson 

	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("VAR");

		jg.writeStartObject();

		jg.writeNumberField("badge", badge);

		jg.writeStringField("surname", surname);

		jg.writeNumberField("age", age);

		jg.writeNumberField("salary", salary);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}


	public static VAR fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		int jBadge = -1;
		String jSurname = null;
		int jAge = -1;
		int jSalary = -1;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "VAR".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no VAR object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "badge":
						jp.nextToken();
						jBadge = jp.getIntValue();
						break;
					case "surname":
						jp.nextToken();
						jSurname = jp.getText();
						break;
					case "age":
						jp.nextToken();
						jAge = jp.getIntValue();
						break;
					case "salary":
						jp.nextToken();
						jSalary = jp.getIntValue();
						break;
				}
			}
		}

		return new VAR(jBadge, jSurname, jAge, jSalary);
	}

e lo stesso vale per la classe messaggio

    	@Override
	    public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("message");

		jg.writeStartObject();

		jg.writeStringField("message", message);

		if(errorCode != null) {
			jg.writeStringField("error-code", errorCode);
		}

		if(errorDetails != null) {
			jg.writeStringField("error-details", errorDetails);
		}

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}


## /rest/RestResource

E' la classe che gestice la creazione di una nuova risorsa rest usata dal servlet vero e proprio per comunicare.

   

    package it.unipd.dei.webapp.rest;

    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import java.sql.Connection;

    /**
    * Represents a generic REST resource.
    * 
    * @author Nicola Ferro (ferro@dei.unipd.it)
    * @version 1.00
    * @since 1.00
    */
    public abstract class RestResource {

        /**
        * The HTTP request
        */
        protected final HttpServletRequest req;

        /**
        * The HTTP response
        */
        protected final HttpServletResponse res;

        /**
        * The connection to the database
        */
        protected final Connection con;

        /**
        * Creates a new REST resource.
        *
        * @param req the HTTP request.
        * @param res the HTTP response.
        * @param con the connection to the database.
        */
        protected RestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
            this.req = req;
            this.res = res;
            this.con = con;
        }
    }

## /rest/"Classe"RestResource

Classe che lavora come servlet, gestito dal servletrestmanager che per ogni risorsa crea una restResource e definisce tutti i metodi necessari a operare sulla o con la risorsa:

    

    package it.unipd.dei.webapp.rest;

    import it.unipd.dei.webapp.database.*;
    import it.unipd.dei.webapp.resource.*;

    import java.io.*;
    import java.sql.Connection;
    import java.sql.SQLException;
    import java.util.List;

    import javax.servlet.ServletException;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;

    /**
    * Manages the REST API for the {@link Employee} resource.
    * 
    * @author Nicola Ferro (ferro@dei.unipd.it)
    * @version 1.00
    * @since 1.00
    */
    public final class EmployeeRestResource extends RestResource {

        /**
        * Creates a new REST resource for managing {@code Employee} resources.
        *
        * @param req the HTTP request.
        * @param res the HTTP response.
        * @param con the connection to the database.
        */
        public EmployeeRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
            super(req, res, con);
        }


        /**
        * Creates a new employee into the database. 
        *
        * @throws IOException
        *             if any error occurs in the client/server communication.
        */
        public void createEmployee() throws IOException {

            Employee e  = null;
            Message m = null;

            try{

                final Employee employee =  Employee.fromJSON(req.getInputStream());

                // creates a new object for accessing the database and stores the employee
                e = new CreateEmployeeDatabase(con, employee).createEmployee();

                if(e != null) {
                    res.setStatus(HttpServletResponse.SC_CREATED);
                    e.toJSON(res.getOutputStream());
                } else {
                    // it should not happen
                    m = new Message("Cannot create the employee: unexpected error.", "E5A1", null);
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    m.toJSON(res.getOutputStream());
                }
            } catch (Throwable t) {
                if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                    m = new Message("Cannot create the employee: it already exists.", "E5A2", t.getMessage());
                    res.setStatus(HttpServletResponse.SC_CONFLICT);
                    m.toJSON(res.getOutputStream());
                } else {
                    m = new Message("Cannot create the employee: unexpected error.", "E5A1", t.getMessage());
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    m.toJSON(res.getOutputStream());
                }
            }
        }

        /**
        * Reads an employee from the database.
        *
        * @throws IOException
        *             if any error occurs in the client/server communication.
        */
        public void readEmployee() throws IOException {

            Employee e  = null;
            Message m = null;

            try{
                // parse the URI path to extract the badge
                String path = req.getRequestURI();
                path = path.substring(path.lastIndexOf("employee") + 8);

                final int badge = Integer.parseInt(path.substring(1));


                // creates a new object for accessing the database and reads the employee
                e = new ReadEmployeeDatabase(con, badge).readEmployee();

                if(e != null) {
                    res.setStatus(HttpServletResponse.SC_OK);
                    e.toJSON(res.getOutputStream());
                } else {
                    m = new Message(String.format("Employee %d not found.", badge), "E5A3", null);
                    res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    m.toJSON(res.getOutputStream());
                }
            } catch (Throwable t) {
                m = new Message("Cannot read employee: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }

        /**
        * Updates an employee in the database.
        *
        * @throws IOException
        *             if any error occurs in the client/server communication.
        */
        public void updateEmployee() throws IOException {

            Employee e  = null;
            Message m = null;

            try{
                // parse the URI path to extract the badge
                String path = req.getRequestURI();
                path = path.substring(path.lastIndexOf("employee") + 8);

                final int badge = Integer.parseInt(path.substring(1));
                final Employee employee =  Employee.fromJSON(req.getInputStream());

                if (badge != employee.getBadge()) {
                    m = new Message(
                            "Wrong request for URI /employee/{badge}: URI request and employee resource badges differ.",
                            "E4A7", String.format("Request URI badge %d; employee resource badge %d.",
                                                badge, employee.getBadge()));
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    m.toJSON(res.getOutputStream());
                    return;
                }

                // creates a new object for accessing the database and updates the employee
                e = new UpdateEmployeeDatabase(con, employee).updateEmployee();

                if(e != null) {
                    res.setStatus(HttpServletResponse.SC_OK);
                    e.toJSON(res.getOutputStream());
                } else {
                    m = new Message(String.format("Employee %d not found.", employee.getBadge()), "E5A3", null);
                    res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    m.toJSON(res.getOutputStream());
                }
            } catch (Throwable t) {
                if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                    m = new Message("Cannot update the employee: other resources depend on it.", "E5A4", t.getMessage());
                    res.setStatus(HttpServletResponse.SC_CONFLICT);
                    m.toJSON(res.getOutputStream());
                } else {
                    m = new Message("Cannot update the employee: unexpected error.", "E5A1", t.getMessage());
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    m.toJSON(res.getOutputStream());
                }
            }
        }


        /**
        * Deletes an employee from the database.
        *
        * @throws IOException
        *             if any error occurs in the client/server communication.
        */
        public void deleteEmployee() throws IOException {

            Employee e  = null;
            Message m = null;

            try{
                // parse the URI path to extract the badge
                String path = req.getRequestURI();
                path = path.substring(path.lastIndexOf("employee") + 8);

                final int badge = Integer.parseInt(path.substring(1));


                // creates a new object for accessing the database and deletes the employee
                e = new DeleteEmployeeDatabase(con, badge).deleteEmployee();

                if(e != null) {
                    res.setStatus(HttpServletResponse.SC_OK);
                    e.toJSON(res.getOutputStream());
                } else {
                    m = new Message(String.format("Employee %d not found.", badge), "E5A3", null);
                    res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    m.toJSON(res.getOutputStream());
                }
            } catch (Throwable t) {
                if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                    m = new Message("Cannot delete the employee: other resources depend on it.", "E5A4", t.getMessage());
                    res.setStatus(HttpServletResponse.SC_CONFLICT);
                    m.toJSON(res.getOutputStream());
                } else {
                    m = new Message("Cannot delete the employee: unexpected error.", "E5A1", t.getMessage());
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    m.toJSON(res.getOutputStream());
                }
            }
        }

        /**
        * Searches employees by their salary.
        *
        * @throws IOException
        *             if any error occurs in the client/server communication.
        */
        public void searchEmployeeBySalary()  throws IOException {

            List<Employee> el  = null;
            Message m = null;

            try{
                // parse the URI path to extract the salary
                String path = req.getRequestURI();
                path = path.substring(path.lastIndexOf("salary") + 6);

                final int salary = Integer.parseInt(path.substring(1));


                // creates a new object for accessing the database and search the employees
                el = new SearchEmployeeBySalaryDatabase(con, salary).searchEmployeeBySalary();

                if(el != null) {
                    res.setStatus(HttpServletResponse.SC_OK);
                    new ResourceList(el).toJSON(res.getOutputStream());
                } else {
                    // it should not happen
                    m = new Message("Cannot search employee: unexpected error.", "E5A1", null);
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    m.toJSON(res.getOutputStream());
                }
            } catch (Throwable t) {
                m = new Message("Cannot search employee: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }

        /**
        * Lists all the employees.
        *
        * @throws IOException
        *             if any error occurs in the client/server communication.
        */
        public void listEmployee() throws IOException {

            List<Employee> el  = null;
            Message m = null;

            try{
                // creates a new object for accessing the database and lists all the employees
                el = new ListEmployeeDatabase(con).listEmployee();

                if(el != null) {
                    res.setStatus(HttpServletResponse.SC_OK);
                    new ResourceList(el).toJSON(res.getOutputStream());
                } else {
                    // it should not happen
                    m = new Message("Cannot list employees: unexpected error.", "E5A1", null);
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    m.toJSON(res.getOutputStream());
                }
            } catch (Throwable t) {
                m = new Message("Cannot search employee: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }








































































































































