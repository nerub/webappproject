# Creazione classi e file



Dentro src, creo webapp

Dentro webapp creo le cartelle:

    -css

    -resource

    -js

    -jsp(il file html all'inizio finche testi che si veda, e poi jsp)

    -WEB-INF(qui dentro creo il web.xml)

    -META-INF(creare il context.xml)

*************************************************

# CONTEXT.XML

devo gestire la connessione al DB(ovviament e qui è settato quello di ferro)

    <?xml version="1.0"?>
    <!--

     Copyright 2018 University of Padua, Italy

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.

     Author: Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
     Version: 1.0
     Since: 1.0
    -->

    <Context>

       <Resource name="jdbc/20180620-examsim-furlan-1149318-pool"
              auth="Container"
              type="javax.sql.DataSource"
              factory="org.apache.tomcat.jdbc.pool.DataSourceFactory"
              driverClassName="org.postgresql.Driver"
              url="jdbc:postgresql://dbstud.dei.unipd.it:5432/webapp1718"
              username="webdb"
              password="webdb"
              testOnBorrow="true"
              validationQuery="SELECT 1"
              timeBetweenEvictionRunsMillis="30000"
              maxActive="10"
              minIdle="5"
              maxWait="10000"
              initialSize="2"
              removeAbandonedTimeout="60"
              removeAbandoned="true"
    	/>

    </Context>



**************************************************

# WEB.XML

Siccome non ci sono servlet da gestire, poichè siamo a lato client il web.xml contiene poca roba:


    <web-app id="mytoys-client" version="4.0" xmlns="http://xmlns.jcp.org/xml/ns/javaee"
    	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    	xsi:schemaLocation="http://java.sun.com/xml/ns/javaee   http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd">

    	<display-name>NOME APP - Exam Simulation</display-name>
    	<description>EXAM.</description>
    </web-app>


******************************************************************************************  ***********

# IDEA

Nel compito della Maria, si aveva una pagina che poteva benissimo essere fatta in html.   Infatti poi gestiva delle chaimate asincrone al Server con ajax nel js, e generava  dinamicamente delle parti di pagina.

Quindi :

creo l' web.xml

creo le risorse immagine

prima faccio l'html, con le parti che so essere sempre statiche.(importo dal webbe sia js che css di bootstrap, FontAwsome, popper, jquery ... quel che mi serve)

ci metto il css sopra così sembra bello

A questo punto faccio diventare l'html un jsp gestendo le importazioni:

    <%@ page contentType="text/html;charset=utf-8" %>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


Infine faccio il js per gestire le richieste del prof.

***************************************************************************************

# Html bone 



    <!-- qui importo le librerie per jsp 
    <%@ page contentType="text/html;charset=utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    -->

    <!DOCTYPE html>
    <html lang="en">
      <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="example web application for exam simulation">
        <meta name="author" content="Edoardo Furlan">

        <title>TITOLO</title>

        <!-- importo css, sia fonti esterne che il mio file 
        se vado in jsp l'import locale diviene:

        <link href="<c:url value="/css/NOMEFILE.css"/>" rel="stylesheet">
        -->

        <body>





            <!-- appena prima della fine del body importo js, fonti esterne e locali
                nel caso di locale con jsp diventa:
                <script src="<c:url value="/js/NOMEFILE.js"/>"></script>
            -->
        </body>
    </html>


**********************************************************************************************

# Esempio di CSS della maria


    */
    @charset "UTF-8";
    /* CSS Document */

    .addMargin {
    	margin: 2em 0em 2em 0em;	
    }

    #page_title {
    	font-size: 4em;
    	color: #810f7c;
    }

    #button_search {
    	color: white;
    	background-color: #810f7c;
    }

    tr:hover {
    	background-color: rgba(140,107,177, 0.5)!important;
    }

    #mytoys_logo {
    	width: 100%;
    }


    .containerOver {
    	margin-top: 4em;
    }

******************************************************************************

# Javascript

Importante:
Quello che metto dentro questa funzione avviene dopo il caricamento della pagina

    $(document).ready ( function(){
       alert('ok');
    });​

## Fare una call asincrona:
nella funzione che sicarica a inizion pagina, definisco la variabile globale:

    var httpRequest;

In questo modo anche se chiudo chiamata e gestione dell' httprequest in funzioni diverse, il tutto funziona. Magari impongo che al click di un bottone, al submit di un form, o al load della pagina succeda:

        var url = "...";
        httpRequest = new XMLHttpRequest();

		if (!httpRequest) {
			alert('Giving up :( Cannot create an XMLHttpRequest instance');
			return false;
		}

		httpRequest.onreadystatechange = NOMEFUNZIONE;
		httpRequest.open('GET', url);
		httpRequest.send();


poi anche fuori dalla funzione (ma dentro quella in cui si è definito "var httpRequest") in cui è definito il codice sopra, devo definire la funzione:

    function NOMEFUNZIONE(){
        
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status == 500) 
            {
                    //gestione erroe
                    var jsonData = JSON.parse(httpRequest.responseText);
				    jsonData = jsonData['message'];
                    //...code
            }else if (httpRequest.status == 200) 
            {       
                    //gestione connessione a buon fine
                    var jsonData = JSON.parse(httpRequest.responseText);
				    jsonData = jsonData['resource-list'];

                    for (var i = 0; i < jsonData.length; i++) 
                    {
					    product = jsonData[i].product;
                        //...code
                    }
            }

        }
    }

## Form validation

Non ho idea di come gestirlo, mai provatoXD guardate il lab della maria su questo