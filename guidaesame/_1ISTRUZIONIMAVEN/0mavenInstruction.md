
# GENERATE MAVEN PACKAGE:

## IN PRESENZATIONE MARKDOWN TAGLI IL CODICEE HTML; GUARDARE IL SORGERNTE.

-Se lato server

	mvn -B archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-simple -DgroupId=it.unipd.dei.webapp -DartifactId=20180620-examsim-furlan-1149318


## MAPPARE IL DB SULLA PORTA

## Se sono a casa:
 
 	ssh -L 5433:dbstud.dei.unipd.it:5432 -L 8081:dbstud.dei.unipd.it:8080 furlaned@login.dei.unipd.it

 per accedere al DB:

 	psql -U webdb -d webapp1718 -p 5433 -h localhost

 e per il tomcat

 	http://localhost:8081/manager/html/


## Se sono al dei:

non devo fare ponti ssh, accedo al tomcat direttamente con 
	
	http://dbstud.dei.unipd.it:8080/manager/html

per accedere al DB uso:

	psql -U webdb -d webapp1718 



# POM.xml

-------------------------------------------------------------------------------------------------------------

.appena sotto ---> version>1.0-SNAPSHOT</version

	<packaging>war</packaging>

eliminare la dipendenza a JUNIT che serve per i test

    <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
    </dependency>
    </dependencies>

e aggiungere :

                    <!-- Dependencies -->
                    	<dependencies>
                    		<dependency>
                    			<groupId>javax.servlet</groupId>
                    			<artifactId>javax.servlet-api</artifactId>
                    			<version>4.0.0</version>
                    			<scope>provided</scope>
                    		</dependency>

                    		<dependency>
                    		    <groupId>org.apache.taglibs</groupId>
                    		    <artifactId>taglibs-standard-spec</artifactId>
                    		    <version>1.2.5</version>
                    		</dependency>
                    		<dependency>
                    		    <groupId>org.apache.taglibs</groupId>
                    		    <artifactId>taglibs-standard-impl</artifactId>
                    		    <version>1.2.5</version>
                    		</dependency>
                    		<dependency>
                    		    <groupId>org.apache.taglibs</groupId>
                    		    <artifactId>taglibs-standard-jstlel</artifactId>
                    		    <version>1.2.5</version>
                    		</dependency>

                    		<dependency>
                    			<groupId>org.postgresql</groupId>
                    			<artifactId>postgresql</artifactId>
                    			<version>42.2.2</version>
                    		</dependency>

                    		<dependency>
                    			<groupId>org.apache.tomcat</groupId>
                    			<artifactId>tomcat-jdbc</artifactId>
                    			<version>9.0.7</version>
                    			<scope>provided</scope>
                    		</dependency>

                    		<dependency>
                    			<groupId>com.fasterxml.jackson.core</groupId>
                    			<artifactId>jackson-core</artifactId>
                    			<version>2.9.5</version>
                    		</dependency>

                    	</dependencies>

eliminare le righe 

	<maven.compiler.source>1.7</maven.compiler.source>

	<maven.compiler.target>1.7</maven.compiler.target>

NEL BUILD prima dei plugins

        <defaultGoal>compile</defaultGoal>
		
		<!-- source code folder -->
		<sourceDirectory>${basedir}/src/main/java</sourceDirectory>
		
		<!-- compiled code folder -->
		<directory>${basedir}/target</directory>
		
		<!-- name of the generated package -->
		<finalName>${project.artifactId}-${project.version}</finalName>

NEI PLUGIN

sostituisco(il codice appare storto, ma in anteprima viene fico):

		<plugin>
	          <artifactId>maven-compiler-plugin</artifactId>
	          <version>3.7.0</version>
	        </plugin>

con(ovviamente scelgo la versione di java chge voglio):

		<plugin>
	          <artifactId>maven-compiler-plugin</artifactId>
	          <version>3.7.0</version>
			<configuration>
				<source>1.8</source>
				<target>1.8</target>
			</configuration>
	        </plugin>

poi elimino:

        <plugin>
          <artifactId>maven-site-plugin</artifactId>
          <version>3.7</version>
        </plugin>

e aggiungo:

        <plugin>
			<artifactId>maven-javadoc-plugin</artifactId>
			<version>3.0.0</version>
            <configuration>
				<reportOutputDirectory>${basedir}/javadoc</reportOutputDirectory>
				<author>true</author>
				<nosince>false</nosince>
				<show>protected</show>
			</configuration>
		</plugin>  
        <plugin>
			<artifactId>maven-war-plugin</artifactId>
			<version>3.2.0</version>
			<configuration>
				<webXml>${basedir}/src/main/webapp/WEB-INF/web.xml</webXml>
			</configuration>
		</plugin>      

Infine aggiungo RISORSE dentro il blocco "build":

        <!--  process resources before compilation and packaging -->
		<resources>
			
			<!--  copy JSP files to the target directory -->
			<resource>
				<targetPath>${basedir}/target/${project.artifactId}-${project.version}/jsp</targetPath>
				<directory>${basedir}/src/main/webapp/jsp</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
			</resource>
			
			<!--  copy configuration files to the target directory -->
			<resource>
				<targetPath>${basedir}/target/${project.artifactId}-${project.version}/META-INF</targetPath>
				<directory>${basedir}/src/main/webapp/META-INF</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
			</resource>
			
		</resources>        

------------------------------------------------------------------------------------------------------------------------------------

*****************************************************************************************************************************

# NEL MIO CASO ALLA FINE IL FILE POM é:

	<?xml version="1.0" encoding="UTF-8"?>
	<!--
	
	 Copyright 2018 University of Padua, Italy
	
	 Licensed under the Apache License, Version 2.0 (the "License");
	 you may not use this file except in compliance with the License.
	 You may obtain a copy of the License at
	
	     http://www.apache.org/licenses/LICENSE-2.0
	
	 Unless required by applicable law or agreed to in writing, software
	 distributed under the License is distributed on an "AS IS" BASIS,
	 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 See the License for the specific language governing permissions and
	 limitations under the License.
	
	 Author: Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
	 Version: 1.0
	 Since: 1.0
	-->

	<project xmlns="http://maven.apache.org/POM/4.0.0" 	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	  <modelVersion>4.0.0</modelVersion>

	  <groupId>it.unipd.dei.webapp</groupId>
	  <artifactId>20180620-examsim-furlan-1149318</artifactId>
	  <version>1.0-SNAPSHOT</version>
	  <packaging>war</packaging>

	  <name>20180620-examsim-furlan-1149318</name>
	  <description>A simple 20180620-examsim-furlan-1149318.</description>
	  <!-- FIXME change it to the project's website -->
	  <url>http://www.example.com</url>

	  <properties>
	    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

	  </properties>

	  <!-- Dependencies -->
		<dependencies>
			<dependency>
				<groupId>javax.servlet</groupId>
				<artifactId>javax.servlet-api</artifactId>
				<version>4.0.0</version>
				<scope>provided</scope>
			</dependency>

			<dependency>
			    <groupId>org.apache.taglibs</groupId>
			    <artifactId>taglibs-standard-spec</artifactId>
			    <version>1.2.5</version>
			</dependency>
			<dependency>
			    <groupId>org.apache.taglibs</groupId>
			    <artifactId>taglibs-standard-impl</artifactId>
			    <version>1.2.5</version>
			</dependency>
			<dependency>
			    <groupId>org.apache.taglibs</groupId>
			    <artifactId>taglibs-standard-jstlel</artifactId>
			    <version>1.2.5</version>
			</dependency>

			<dependency>
				<groupId>org.postgresql</groupId>
				<artifactId>postgresql</artifactId>
				<version>42.2.2</version>
			</dependency>

			<dependency>
				<groupId>org.apache.tomcat</groupId>
				<artifactId>tomcat-jdbc</artifactId>
				<version>9.0.7</version>
				<scope>provided</scope>
			</dependency>

			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-core</artifactId>
				<version>2.9.5</version>
			</dependency>

		</dependencies>


	  <build>
	    <defaultGoal>compile</defaultGoal>

			<!-- source code folder -->
			<sourceDirectory>${basedir}/src/main/java</sourceDirectory>

			<!-- compiled code folder -->
			<directory>${basedir}/target</directory>

			<!-- name of the generated package -->
			<finalName>${project.artifactId}-${project.version}</finalName>


	    <pluginManagement><!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
	      <plugins>
	        <plugin>
	          <artifactId>maven-clean-plugin</artifactId>
	          <version>3.0.0</version>
	        </plugin>

	        <plugin>
	          <artifactId>maven-project-info-reports-plugin</artifactId>
	          <version>2.9</version>
	        </plugin>
	        <!-- see http://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_jar_packaging -->
	        <plugin>
	          <artifactId>maven-resources-plugin</artifactId>
	          <version>3.0.2</version>
	        </plugin>
	        <plugin>
	          <artifactId>maven-compiler-plugin</artifactId>
	          <version>3.7.0</version>
						<configuration>
							<source>1.8</source>
							<target>1.8</target>
						</configuration>
	        </plugin>
	        <plugin>
	          <artifactId>maven-surefire-plugin</artifactId>
	          <version>2.20.1</version>
	        </plugin>
	        <plugin>
	          <artifactId>maven-jar-plugin</artifactId>
	          <version>3.0.2</version>
	        </plugin>
	        <plugin>
	          <artifactId>maven-install-plugin</artifactId>
	          <version>2.5.2</version>
	        </plugin>
	        <plugin>
	          <artifactId>maven-deploy-plugin</artifactId>
	          <version>2.8.2</version>
	        </plugin>
	        
					<plugin>
				  	<artifactId>maven-javadoc-plugin</artifactId>
			  	  	<version>3.0.0</version>
	            	<configuration>
					      	<reportOutputDirectory>${basedir}/javadoc</reportOutputDirectory>
				      		<author>true</author>
					      	<nosince>false</nosince>
				      		<show>protected</show>
			      		</configuration>
		    	</plugin>
					  
	        <plugin>
			    	<artifactId>maven-war-plugin</artifactId>
			    	<version>3.2.0</version>
			    	<configuration>
					    <webXml>${basedir}/src/main/webapp/WEB-INF/web.xml</webXml>
				    </configuration>
			    </plugin>
	      </plugins>
	    </pluginManagement>

			<!--  process resources before compilation and packaging -->
			<resources>
				<!--  copy JSP files to the target directory -->
				<resource>
					<targetPath>${basedir}/target/${project.artifactId}-${project.version}	/jsp</targetPath>
					<directory>${basedir}/src/main/webapp/jsp</directory>
					<includes>
						<include>**/*.*</include>
					</includes>
				</resource>

				<!--  copy configuration files to the target directory -->
				<resource>
					<targetPath>${basedir}/target/${project.artifactId}-${project.version}	/META-INF</targetPath>
					<directory>${basedir}/src/main/webapp/META-INF</directory>
					<includes>
						<include>**/*.*</include>
					</includes>
				</resource>
			</resources>  

	  </build>

	  <reporting>
	    <plugins>
	      <plugin>
	        <artifactId>maven-project-info-reports-plugin</artifactId>
	      </plugin>
	    </plugins>
	  </reporting>

	</project>        

*************************************************************************************************

# Ora se voglio fare un lato client seguo una strada, altrimenti l'altra. Vedi file:

latoclient.md

latoserver.md