package it.unipd.dei.webapp.database;

/* Importo la tabella del database che mi interessa */
import it.unipd.dei.webapp.resource.<nomeTabellaInResource>;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class <nomeMetodo>
{
	/* Statement SQL per SELECT */
	/* Le tabelle da cui si attinge devono essere tutte importate */
	private static final String STATEMENT = "SELECT <parametri cercati>" +
											"FROM <nomeDatabase>.<nomeTabella da cui prendere>" +
											" WHERE <nomeDatabase>.<nomeTabella>.<attributo> = ?";

	/* Statement SQL per INSERT */
	private static final String STATEMENT = "INSERT INTO <nomeDatabase>.<tabella> (<attr1>, <attr2>, <attr3>, <attr4>) " +
											"VALUES (?, ?, ?, ?)" +
											" RETURNING *"; 

	/* Statement SQL per DELETE */
	private static final String STATEMENT = "DELETE "
                                            + " FROM <nomeDatabase>.<tabella>"
                                            + " WHERE <nomeDatabase>.<tabella>.<attributo> = ?"
											+ " RETURNING *";					

	/* Connessione al databsase*/
    private final Connection con;

    /*  Se per la query serve un parametro (es. email da cercare, commento da rimuovere...) si devono 
    	inserire le variabili */
    private final <tipoVariabile> <nomeVariabile>;
    private final Comment comment; String email; int commentId;   	//esempi

    /* Costruttore, inserisco tutte le variabili*/
    public <nomeClasseJava>(final <tipoVariabile> <nomeVariabile>)
    {
    	this.<nomeVariabile> = <nomeVariabile>;
    }

    /***************************************************************/
    /* Inserisco i metodi della classe, questo per INSERT e DELETE */
    /***************************************************************/
    public <nometabella> <nomeMetodo>() throws SQLException
    {
    	PreparedStatement pstmt = null;
		ResultSet rs = null;

		<nomeTabella> e = null;

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

            pstmt.setInt(1, <nomeTabella>.<metodo1>());  // Uno per ogni metodo della tabella
            pstmt.setString(2, <nomeTabella>.<metodo2>());

			rs = pstmt.executeQuery();

			if (rs.next()) {
				e = new <nomeTabella(rs.getInt("<nomeVariabile1>"), // Uno per ogni variabile della tabella
									 rs.getString("<nomeVariabile2>"));
			}

		}

		finally
		{
			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}
		return e;
		/*********************/
		/* Questo per SELECT */
		/*********************/
		public List<<nomeTabella>> <nomeMetodo() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<<nomeTabella>> competences = new ArrayList<<nomeTabella>>(); //competences è una variabile nuova

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				competences.add(new <nomeTabella>(rs.getString("name")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return competences;
	}






    }

}
