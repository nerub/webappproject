package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SelectQuestionDatabase;
import it.unipd.dei.webapp.resource.Question;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.ResourceList;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

public final class Servlet extends AbstractDatabaseServlet //Servlet è il nome del file
{
	public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
            	Tag e  = null;			// Per il create
            	List<Tag> el = null;  	// Per il select, solo uno dei 2 in base alla classe
        		Message m = null;

        		/* Solo uno tra i punti seguenti in base al nome della classe */
        		// 1.SelectQuestion
        		res.setContentType("application/json");
        		res.setCharacterEncoding("UTF-8");
        		
        		// 2.SelectTagByQuestionId
        		int questionid = Integer.parseInt(req.getParameter("questionId"));
        		
        		// 3.Per il create non va scritto nulla
        		//
        		try {
        			// 1.SelectQuestion
        			el = new SelectQuestionDatabase(getDataSource().getConnection()).SelectQuestion();
        			m = new Message("questions successfully searched.");
        			res.setStatus(HttpServletResponse.SC_OK);
           			// 2.SelectTagByQuestionId
           			el = new SelectTagDatabaseByQuestionId(getDataSource().getConnection(), questionid)
           			        .selectTags();
           			res.setStatus(HttpServletResponse.SC_OK);
					// 3.CreateTag
					name = req.getParameter("name");
            		e = new Tag(name);
            		new InsertTagDatabase(getDataSource().getConnection(), e).InsertTag();
            		m = new Message(String.format("Tag %s successfully created.", name));
            		//
      			}catch (NumberFormatException ex) {
           				m = new Message("Cannot search for tags.",
                   		"E100", ex.getMessage());
                   		res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
          				m.toJSON(res.getOutputStream());
      			} catch (SQLException ex) {
           				m = new Message("Cannot search for tags: unexpected error while accessing the database.",
                   		"E200", ex.getMessage());
                   		res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
           				m.toJSON(res.getOutputStream());
      			}

      			// 1.SelectQuestion
        		new ResourceList(el).toJSON(res.getOutputStream());
        		
        		// 2.SelectTagByQuestionId
        		req.setAttribute("taglist", el);
      	 		req.setAttribute("message", m);
        		
        		// 3.CreateTag
      	 		System.out.println(name);
        		req.setAttribute("Tag", e);
        		req.setAttribute("message", m);
        		req.getRequestDispatcher("/jsp/create-tag-result.jsp").forward(req, res);
        		//
            }
}