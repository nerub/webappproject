package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.*;
import java.io.*;

/* <nomeClasse sarebbe il nome del file.java */
public class <NomeClasse> extends Resource //extend Resource non sempre necessario
{
	private final <tipoVariabile1> <nomeVariabile1>;
	final <tipoVariabile2> <nomeVariabile2>;
	/* Questo lo faccio per ogni variabile presente nella tabella del database */

	/* Costruttore */
	public <NomeClasse>(final <tipoVariabile1> <nomeVariabile1>, final <tipoVariabile2> <nomeVariabile2>) //Dentro aggiungo tutte le variabili 
	{
		this.<nomeVariabile1> = <nomeVariabile1>
		this.<nomeVariabile2> = <nomeVariabile2>
	}

	/* Aggiungo i metodi che mi restituiscono le variabili */
	public final String get<nomeVariabile1>() 
	{
		return <nomeVariabile1>;
	}

	public final String get<nomeVariabile2>() 
	{
		return <nomeVariabile2>;
	}

	/**************/
	/* Parte JSON */
	/**************/

	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("<nomeClasse>");

		jg.writeStartObject();

		jg.writeNumberField("<nomeVariabile1", <nomeVariabile1>); // Per gli int

		jg.writeStringField("<nomeVariabile2>", <nomeVariabile2>); // Per le stringhe

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	public static <NomeClasse> fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		int j<nomeVariabile1> = -1;
		String j<nomeVariabile2> = null;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "nomeClasse".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no comment object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "<nomeVariabile1>":
						jp.nextToken();
						j<nomeVariabile1> = jp.getIntValue();
						break;
					case "<nomeVariabile2>":
						jp.nextToken();
						j<nomeVariabile2> = jp.getText();
						break;
				}
			}
		}

		return new Comment(j<nomeVariabile1>, j<nomeVariabile2>);
	}
}