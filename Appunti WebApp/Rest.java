package it.unipd.dei.webapp.rest;
import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.*;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class <NomeClasse> extends RestResource
{
	/* Costruttore */
	public <NomeClasse>(final HttpServletRequest req, final HttpServletResponse res, Connection con)
	{
		super(req, res, con);
	}

	/* Di seguito si inseriscono tutti i metodi */
	/* Esempi: InsertComment(), DeleteComment(), SelectComment */

	/**********/
	/* INSERT */
	/**********/
	public void <nomeMetodo>() throws IOException
	{
		<NomeTabella> e = null;
        Message m = null;

        try
		{
            final <NomeTabella> <nomeTabella> = <NomeTabella>.fromJSON(req.getInputStream());

            // creates a new object for accessing the database and stores the competence
            e = new <nomeMetodo>Database(con, <nomeTabella>).<nomeMetodo>();

            if (e != null)
			{
                res.setStatus(HttpServletResponse.SC_CREATED);
                e.toJSON(res.getOutputStream());
            }
			
			else
			{
                // it should not happen
                m = new Message("Cannot create the competence: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
		
		catch (Throwable t)
		{
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505"))
			{
                m = new Message("Cannot create the competence: it already exists.", "E5A2", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            }
			
			else
			{
                m = new Message("Cannot create the competence: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
	}

	/**********/
	/* DELETE */
	/**********/
	public void <nomeMetodo>() throws IOException
	{
        <NomeTabella> e  = null;
        Message m = null;

        try
		{
            // parse the URI path to extract the badge
            String path = req.getRequestURI();
			String attributeName = "<nomeTabella>";
            path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());

            final int badge = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and deletes the comment
            e = new <NomeMetodo>Database(con, badge).<NomeMetodo>();

            if(e != null)
			{
                res.setStatus(HttpServletResponse.SC_OK);
                e.toJSON(res.getOutputStream());
            }
			
			else
			{
                m = new Message(String.format("Comment %d not found.", badge), "E5A3", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        }
		
		catch (Throwable t)
		{
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503"))
			{
                m = new Message("Cannot delete the comment: other resources depend on it.", "E5A4", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            }
			
			else
			{
                m = new Message("Cannot delete the comment: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**********/
	/* DELETE */
	/**********/
	public void selectCommentJoinHasByQuestionId()  throws IOException
	{
        List<Comment> el  = null;
        Message m = null;

        try
		{
            // parse the URI path to extract the questionId
            String path = req.getRequestURI();
			String attributeName = "questionId"; //Cosa vogliamo cercare
            path = path.substring(path.lastIndexOf(attributeName) + attributeName.length());

            final int questionId = Integer.parseInt(path.substring(1));


            // creates a new object for accessing the database and create the comments
            el = new SelectCommentJoinHasByQuestionIdDatabase(con, questionId).SelectComment();

            if(el != null)
			{
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(el).toJSON(res.getOutputStream());
            }
			
			else
			{
                // it should not happen
                m = new Message("Cannot create comment: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
		
		catch (Throwable t)
		{
            m = new Message("Cannot create comment: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}