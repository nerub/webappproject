package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.*;

import java.io.*;
/**
 * Represents a {@link Question} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Question extends Resource {
	/**
	 * the id
	 */
	private final int questionId;
	/**
	 *  the difficulty
	 */
	private int difficulty;
	/**
	 * the private state
	 */
	private boolean isPrivate;
	/**
	 * the number of upvotes
	 */
	private int upvote;
	/**
	 * the number of downvotes
	 */
	private int downvote;
	/**
	 * the noumber of views
	 */
	private int views;
	/**
	 * th body
	 */
	private final String body;
	/**
	 * the title
	 */
	private final String title;

	/**
	 * Creates a new question
	 *
	 * the id
	 * @param questionId
	 * the difficulty
	 * @param difficulty
	 * the private state
	 * @param isPrivate
	 * the number of upvotes
	 * @param upvote
	 * the number of downvotes
	 * @param downvote
	 * the number of views
	 * @param views
	 * the body
	 * @param body
	 * the title
	 * @param title
	 */
	public Question(final int questionId, int difficulty, boolean isPrivate, int upvote, int downvote,
					int views, final String body, final String title) {
		this.questionId = questionId;
		this.difficulty = difficulty;
		this.isPrivate = isPrivate;
		this.upvote = upvote;
		this.downvote = downvote;
		this.views = views;
		this.body = body;
		this.title = title;
	}

	// Get methods

	/**
	 * the id
	 * @return id
	 */
	public final int getQuestionId() {
		return questionId;
	}

	/**
	 * the difficulty
	 * @return difficulty
	 */
	public int getDifficulty() {
		return difficulty;
	}

	/**
	 * the private state
	 * @return private state
	 */
	public boolean getIsPrivate() {
		return isPrivate;
	}

	/**
	 * the upvotes
	 * @return upvotes
	 */
	public int getUpvote() {
		return upvote;
	}

	/**
	 * th downvotes
	 * @return downvotes
	 */
	public int getDownvote() {
		return downvote;
	}

	/**
	 * the views
	 * @return views
	 */
	public int getViews() {
		return views;
	}

	/**
	 * the body
	 * @returnbody
	 */
	public final String getBody() {
		return body;
	}

	/**
	 * the title
	 * @return
	 */
	public final String getTitle() {
		return title;
	}

	///////////////

	// Set methods

	/**
	 * the difficulty
	 * @param difficulty
	 */
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	/**
	 * the private state
	 * @param isPrivate
	 */
	public void setIsPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	/**
	 * the number of upvotes
	 * @param upvote
	 */
	public void setUpvote(int upvote) {
		this.upvote = upvote;
	}

	/**
	 * the number of downvotes
	 * @param downvote
	 */
	public void setDownvote(int downvote) {
		this.downvote = downvote;
	}

	/**
	 *  the number of views
	 * @param views
	 */
	public void setViews(int views) {
		this.views = views;
	}


	///////////////
	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("question");

		jg.writeStartObject();

		jg.writeNumberField("questionId", questionId);

		jg.writeNumberField("difficulty", difficulty);

		jg.writeBooleanField("isPrivate", isPrivate);

		jg.writeNumberField("upvote", upvote);

		jg.writeNumberField("downvote", downvote);

		jg.writeNumberField("views", views);

		jg.writeStringField("body", body);

		jg.writeStringField("title", title);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	/**
	 * Creates a {@code Question} from its JSON representation.
	 *
	 * @param in the input stream containing the JSON document.
	 * @return the {@code Question} created from the JSON representation.
	 * @throws IOException if something goes wrong while parsing.
	 */
	public static Question fromJSON(final InputStream in) throws IOException {


		// the fields read from JSON
		int jQuestionId = -1;
		int jDifficulty = -1;
		boolean jIsPrivate = false;
		int jUpvote = -1;
		int jDownvote = -1;
		int jViews = -1;
		String jBody = null;
		String jTitle = null;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "question".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no question object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "questionId":
						jp.nextToken();
						jQuestionId = jp.getIntValue();
						break;
					case "difficulty":
						jp.nextToken();
						jDifficulty = jp.getIntValue();
						break;
					case "isPrivate":
						jp.nextToken();
						jIsPrivate = jp.getBooleanValue();
						break;
					case "upvote":
						jp.nextToken();
						jUpvote = jp.getIntValue();
						break;
					case "downvote":
						jp.nextToken();
						jDownvote = jp.getIntValue();
						break;
					case "views":
						jp.nextToken();
						jViews = jp.getIntValue();
						break;
					case "body":
						jp.nextToken();
						jBody = jp.getText();
						break;
					case "title":
						jp.nextToken();
						jTitle = jp.getText();
						break;
				}
			}
		}
		return new Question(jQuestionId, jDifficulty, jIsPrivate, jUpvote, jDownvote, jViews, jBody, jTitle);
	}
}
