package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
* Searches for all the tags in the database.
*
* @version 1.00
* @since 1.00
*/

public final class SelectTagDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "SELECT name FROM IronJobs.Tag";

  /**
  * Connection to the database.
  */
	private final Connection con;

  /**
  * Creates a new object for searching the tags.
  * @param con The connection to the database.
  */
	public SelectTagDatabase(final Connection con)
	{
		this.con = con;
	}

  /**
  * Searches for the tags.
  * @return A list of the tags in the database.
  * @throws SQLException if the search goes wrong.
  */
	public List<Tag> SelectTag() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Tag> tags = new ArrayList<Tag>();

		try
		{
			pstmt = con.prepareStatement(STATEMENT);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				tags.add(new Tag(rs.getString("name")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return tags;
	}
}
