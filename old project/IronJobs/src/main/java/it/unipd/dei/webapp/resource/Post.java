package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Post} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Post
{
	/**
	 * the email
	 */
	private final String email;
	/**
	 * the questionId
	 */
	private final int questionId;
	/**
	 * the postTime
	 */
	private final String postTime;

	/**
	 * Creates a post
	 * the email
	 * @param email
	 * the questionId
	 * @param questionId
	 * the postTime
	 * @param postTime
	 */
	public Post(final String email, final int questionId, String postTime)
	{
		this.email = email;
		this.questionId = questionId;
		this.postTime = postTime;
	}
	
	// Get methods

	/**
	 * the mail
	 * @return email
	 */
	public final String getEmail()
	{
		return email;
	}

	/**
	 *  the questionId
	 * @return
	 */
	public final int getQuestionId()
	{
		return questionId;
	}

	/**
	 * the postTime
	 * @return postTime
	 */
	public final String getPostTime()
	{
		return postTime;
	}

	///////////////
	
	// Set methods
	
	///////////////
}