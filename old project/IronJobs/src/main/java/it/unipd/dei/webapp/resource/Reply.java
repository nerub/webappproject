package it.unipd.dei.webapp.resource;
/**
 * Represents a {@link Reply} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Reply
{
	/**
	 * the reply time
	 */
	private final String replyTime;
	/**
	 * the first comment
	 */
	private final int commentIdFirst;
	/**
	 * the second comment
	 */
	private final int commentIdSecond;

	/**
	 * Creates a new Reply
	 * the replytime
	 * @param replyTime
	 * the first comment
	 * @param commentIdFirst
	 * the second comment
	 * @param commentIdSecond
	 */
	public Reply(final String replyTime, final int commentIdFirst, final int commentIdSecond)
	{
		this.replyTime = replyTime;
		this.commentIdFirst = commentIdFirst;
		this.commentIdSecond = commentIdSecond;
	}
	
	// Get methods

	/**
	 * the replytime
	 * @return replytime
	 */
	public String getReplyTime()
	{
		return replyTime;
	}

	/**
	 * the first comment
	 * @return first comment
	 */
	public int getCommentIdFirst()
	{
		return commentIdFirst;
	}

	/**
	 * the second comment
	 * @return second comment
	 */
	public int getCommentIdSecond()
	{
		return commentIdSecond;
	}
	
	///////////////

	// Set methods
	
	///////////////
}