package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Related;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
* Searches for all the questions which have a specific tag.
*
* @version 1.00
* @since 1.00
*/

public final class SelectRelatedJoinQuestionIdByTagDatabase
{
  /**
  * SQL statement that does the insertion.
  */
	private static final String STATEMENT = "SELECT questionId, name FROM IronJobs.Related WHERE name = ?";

  /**
  * Connection to the database.
  */
  private final Connection con;

  /**
  * Name of the tag.
  */
  private final String name;

  /**
  * Creates a new object for searching the questions.
  * @param con The connection to the database.
  * @param name The tag name to search for in the questions.
  */
	public SelectRelatedJoinQuestionIdByTagDatabase(final Connection con, final String name)
	{
        this.con = con;
        this.name = name;
	}

  /**
  * Searches the questions.
  * @return A list of the questions with all of the attributes realted to the tag.
  * @throws SQLException if the search goes wrong.
  */
	public List<Related> SelectRelated() throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Related> relatedList = new ArrayList<Related>();

		try
		{
			pstmt = con.prepareStatement(STATEMENT);
            pstmt.setString(1,name);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				relatedList.add(new Related(rs.getInt("questionId"), rs.getString("name")));
			}
		}

		finally
		{
			if (rs != null)
			{
				rs.close();
			}

			if (pstmt != null)
			{
				pstmt.close();
			}

			con.close();
		}

		return relatedList;
	}
}
