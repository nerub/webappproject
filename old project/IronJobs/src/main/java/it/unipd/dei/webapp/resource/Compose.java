package it.unipd.dei.webapp.resource;

/**
 * Represents a {@link Compose} resource.
 *
 * @author Edoardo Furlan (edoardo.furlan.1@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class Compose
{
	/**
	 * the interview id
	 */
	private final int interviewId;
	/**
	 * the id
	 */
	private final int questionId;

	/**
	 * Creates a compose
	 * the id
	 * @param interviewId
	 * the id
	 * @param questionId
	 */
	public Compose(final int interviewId, final int questionId)
	{
		this.interviewId = interviewId;
		this.questionId = questionId;
	}
	
	// Get methods

	/**
	 * return the interviewId
	 * @return interviewId
	 */
	public final int getInterviewId()
	{
		return interviewId;
	}

	/**
	 * return the question Id
	 * @return questioonId
	 */
	public final int getquestionId()
	{
		return questionId;
	}
	
	///////////////
	
	// Set methods
	
	///////////////
}