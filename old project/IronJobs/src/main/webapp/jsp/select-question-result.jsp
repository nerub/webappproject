<%--
  Created by IntelliJ IDEA.
  User: nerub
  Date: 20/05/18
  Time: 0.47
  To change this template use File | Settings | File Templates.

--%>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Search Question Result</title>
</head>

<body>
<h1>Search Question Result</h1>
<hr/>

<!-- display the message -->
<c:import url="/jsp/include/show-message.jsp"/>

<!-- display the list of found solutions, if any -->
<c:if test='${not empty questionlist}'>
    <table>
        <thead>
        <tr>
            <th>questionid</th><th>difficulty</th><th>isprivate</th><th>upvote</th><th>downvote</th><th>views</th><th>body</th><th>title</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="question" items="${questionlist}">
            <tr>
                <td><c:out value="${question.questionId}"/></td>
                <td><c:out value="${question.difficulty}"/></td>
                <td><c:out value="${question.isPrivate}"/></td>
                <td><c:out value="${question.upvote}"/></td>
                <td><c:out value="${question.downvote}"/></td>
                <td><c:out value="${question.views}"/></td>
                <td><c:out value="${question.body}"/></td>
                <td><c:out value="${question.title}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>
</body>
</html>
