<%--
  Created by IntelliJ IDEA.
  User: nerub
  Date: 20/05/18
  Time: 0.47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Search tag</title>
</head>

<body>
<h1>Search tag</h1>
<hr/>

<!-- display the message -->
<c:import url="/jsp/include/show-message.jsp"/>

<!-- display the list of found tags, if any -->
<c:if test='${not empty taglist}'>
    <table>
        <thead>
        <tr>
            <th>name</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="tag" items="${taglist}">
            <tr>
                <td><c:out value="${tag.name}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>
</body>
</html>
