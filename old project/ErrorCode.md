| Error Code | HTTP Status Code              | Cause                                                         |
| ---------- | ----------------------------- | ------------------------------------------------------------- |
| E4A1       | 400 Bad Request               | Output media type not specified                               |
| E4A2       | 406 Not Acceptble             | Unsupported output media type                                 |
| E4A3       | 400 Bad Request               | Input media type not specified                                |
| E4A4       | 415 Unsupported Media Type    | Unsupported input media type                                  |
| E4A5       | 405 Method Not Allowed        | Unsupported operation                                         |
| E4A6       | 405 Not Found                 | Unknown resource requested                                    |
| E4A7       | 400 Bad Request               | Wrong URI format                                              |
| E5A1       | 500 500 Internal Server Error | Unexpected error while processing a resource                  |
| E5A2       | 409 Conflict                  | Resource already exists                                       |
| E5A3       | 404 Not Found                 | Resource not found                                            |
| E5A4       | 409 Conflict                  | Cannot modify a resource because other resources depend on it |
