
| URI                                    | METHOD | DESCRIPTION                                             |
| -------------------------------------- | ------ | ------------------------------------------------------- |
| \rest\competence                       | POST   | Create a new Competence                                 |
| \rest\possess                          | POST   | Create a new Possess                                    |
| \rest\solution                         | POST   | Createa new Solution                                    |
| \rest\solution\questionId\{questionId} | GET    | Search for all solution with given questionId           |
| \rest\user                             | POST   | Create a new User                                       |
| \rest\question{questionId}             | GET    | Reads the data about the question with given questionId |
| \rest\comment                          | POST   | Create new Comment                                      |
| \rest\comment\questionId\{questionId}  | GET    | Search for all comments with given questionId           |
| \rest\comment\{commentId}              | DELETE | Delete the comments with the given commentId            |
| \rest\respondTo                        | POST   | Create a new RespondTo                                  |
| \rest\RespondTo\{commentId}            | DELETE | Delete the RespondTo with the given commentId           |